package libPhil.huffman;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

public class HuffmanTests {
	
	@Test
	public void testHuffmanNode() {
		HuffmanNode node;
		try {
			node = new HuffmanNode(new Integer[]{1},1.0/9);
		} catch (Exception e) {
			e.printStackTrace();
			fail("HuffmanNode constructor failed");
		}
		
		node = new HuffmanNode(new Integer[]{1},1.0/9);
		assertArrayEquals(new Integer[]{1},node.symbol());
		assertNull(node.left());
		assertNull(node.right());
		assertEquals(1.0/9,node.prob(),Math.pow(2, -30));
		
		HuffmanNode node2 = new HuffmanNode(new Integer[]{2},1.0/9);
		HuffmanNode node3 = new HuffmanNode(node,node2);
		assertArrayEquals(new Integer[]{1,2},node3.symbol());
		assertNotNull(node3.left());
		assertNotNull(node3.right());
		assertEquals(node,node3.left());
		assertEquals(node2,node3.right());
		assertEquals(2.0/9,node3.prob(),Math.pow(2, -30));
	}

	@Test
	public void testHuffmanTreeConstructor() {
		Integer[] sym = new Integer[]{1,2,3,4,5,6,7,8,9};
		double[] prob = new double[9];
		for (int i = 0 ; i < 9; i++)
			prob[i] = 1.0/9;
		HuffmanTree tree;
		try {
			tree = new HuffmanTree(sym,prob);
		} catch (Exception e) {
			e.printStackTrace();
			fail("HuffmanTree(sym,prob) constructor failed");
		}
		try {
			tree = new HuffmanTree(sym);
		} catch (Exception e) {
			e.printStackTrace();
			fail("HuffmanTree(sym) constructor failed");
		}
	}
	
	@Test
	public void testHuffmanTreeCodebook() {
		Integer[] sym = new Integer[]{1,2,3,4,5,6,7,8,9};
		try {
			HuffmanTree tree = new HuffmanTree(sym);
			//String codeBook = tree.codeBookToString();
			for (int i = 1; i <= 9; i++)
				assertEquals(1,tree.decode(tree.encode(1)));
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testHuffmanDecodeNice() {
		Integer[] sym = new Integer[]{1,2,3,4,5,6,7};
		try {
			HuffmanTree tree = new HuffmanTree(sym);
			String bits;
			Random random = new Random();
			for (int i = 0; i < 10; i++) {
				bits = "";
				while (bits.length() < 4)
					bits += random.nextInt(2) == 0 ? "0" : "1";
				Object[] result = tree.decode_nice(bits);
				Object symbol = result[0];
				String ubits = (String)result[1];
				// check that the correct symbol was decoded
				assertEquals(tree.decode(bits.substring(0, bits.length()-ubits.length())),symbol);
				// check that the correct ubits were returned
				assertEquals(bits.substring(tree.encode(symbol).length()),ubits);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
