package libPhil.huffman;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class HuffmanTree {
	private HuffmanNode root;
	private HashMap<Object,String> codeBook;
	
	/**
	 * Build a HuffmanTree over the given symbols
	 * @param sym - alphabet of symbols
	 * @param prob - probability of each symbol
	 */
	public HuffmanTree(Object[] sym, double[] prob) {
		LinkedList<HuffmanNode> nodes = new LinkedList<HuffmanNode>();
		for (int i = 0 ; i < sym.length ; i++)
			nodes.add(new HuffmanNode(sym[i], prob[i]));
		codeBook = null;
		root = buildTree(nodes);
	}
	
	/**
	 * Build a HuffmanTree over the given symbols, using uniform distribution
	 * @param sym - alphabet of symbols, all with equal probability
	 */
	public HuffmanTree(Object[] sym) {
		LinkedList<HuffmanNode> nodes = new LinkedList<HuffmanNode>();
		double p = 1.0 / sym.length;
		for (int i = 0 ; i < sym.length ; i++)
			nodes.add(new HuffmanNode(sym[i], p));
		codeBook = null;
		root = buildTree(nodes);
	}
	
	/**
	 * build Huffman tree
	 * @param nodes - list of HuffmanNodes
	 * @return root of tree
	 */
	private HuffmanNode buildTree(LinkedList<HuffmanNode> nodes) {
		// if only one node, return it
		if (nodes.size() == 1)
			return nodes.getFirst();
		
		// sort nodes by prob
		HuffmanNode[] node_arr = nodes.toArray(new HuffmanNode[0]);
		Arrays.sort(node_arr);
		
		// remove two lightest nodes
		HuffmanNode n1 = node_arr[0];
		HuffmanNode n2 = node_arr[1];
		nodes.remove(n1);
		nodes.remove(n2);
		
		// merge them
		HuffmanNode n3 = new HuffmanNode(n1,n2);
		
		// add the new node back
		nodes.add(n3);
		
		// recurse and return result
		return buildTree(nodes);
	}
	
	public HuffmanNode root() {
		return root;
	}
	
	/**
	 * make the codebook
	 */
	private void makeCodeBook() {
		codeBook = new HashMap<Object,String>();
		makeCodeBook(root);
	}
	
	/**
	 * make the codebook from the given node's perpsective
	 * @param root
	 */
	private void makeCodeBook(HuffmanNode root) {
		if (root.left() == null || root.right() == null)
			return;
		Object[] sym_left = root.left().symbol();
		Object[] sym_right = root.right().symbol();
		for (Object o : sym_left)
			codeBook.put(o, codeBook.get(o) == null ? "0" : codeBook.get(o)+"0");
		for (Object o : sym_right)
			codeBook.put(o, codeBook.get(o) == null ? "1" : codeBook.get(o)+"1");
		makeCodeBook(root.left());
		makeCodeBook(root.right());
	}
	
	/**
	 * 
	 * @return codebook printed to String
	 */
	public String codeBookToString() {
		if (codeBook == null)
			makeCodeBook();
		String str = "";
		for (Object o : codeBook.keySet())
			str += o + "\t" + codeBook.get(o) + "\n";
		return str;
	}
	
	/**
	 * encode a symbol
	 * @param symbol
	 * @return code for given symbol
	 */
	public String encode(Object symbol) {
		if (codeBook == null)
			makeCodeBook();
		return codeBook.get(symbol);
	}
	
	/**
	 * decode a given code
	 * @param code
	 * @return decoded symbol
	 */
	public Object decode(String code) {
		return decode(code,root);
	}
	
	/**
	 * decode a given code from the given node
	 * @param code
	 * @param root
	 * @return decoded symbol, null if code too short or too long
	 */
	private Object decode(String code, HuffmanNode root) {
		if (root.left() == null || root.right() == null)
			if (code.isEmpty())
				return root.symbol()[0];
			else // too long, not a valid code
				return null;
		if (code.isEmpty()) // too short, not a valid code
			return null;
		if (code.charAt(0) == '0')
			return decode(code.substring(1),root.left());
		else
			return decode(code.substring(1),root.right());
	}
	
	/**
	 * decode the given code, preserving extra bits that were not used
	 * @param bits - should contain at least a valid code
	 * @return 
	 * [0] - decoded symbol<br>
	 * [1] - unused bits<br>
	 * null - if bits are not a valid code
	 */
	public Object[] decode_nice(String bits) {
		return decode_nice(bits,root);
	}
	
	/**
	 * decode given code, from given node
	 * @param code
	 * @param root
	 * @return decoded symbol and unused bits, return null if code was too short
	 */
	private Object[] decode_nice(String code, HuffmanNode root) {
		if (root.left() == null || root.right() == null) {
			Object[] ret = new Object[2];
			ret[0] = root.symbol()[0];
			ret[1] = code;
			return ret;
		}
		if (code.isEmpty()) // too short, not a valid code
			return null;
		if (code.charAt(0) == '0')
			return decode_nice(code.substring(1),root.left());
		else
			return decode_nice(code.substring(1),root.right());
	}
}
