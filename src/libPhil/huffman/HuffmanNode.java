package libPhil.huffman;

public class HuffmanNode implements Comparable<HuffmanNode> {
	private Object[] symbol;
	private double prob;
	private HuffmanNode left;
	private HuffmanNode right;
	
	/**
	 * create a new HuffmanNode object with given supersymbol
	 * @param sym - supersymbol
	 * @param p - probability of supersymbol
	 */
	public HuffmanNode(Object[] sym, double p) {
		symbol = new Object[sym.length];
		System.arraycopy(sym, 0, symbol, 0, sym.length);
		prob = p;
		left = null;
		right = null;
	}
	
	/**
	 * create a new HuffmanNode object with given symbol
	 * @param sym - symbol
	 * @param p - probability of symbol
	 */
	public HuffmanNode(Object sym, double p) {
		this(new Object[]{sym},p);
	}
	
	/**
	 * merge constructor.  merge two nodes into one. lighter node goes on left.
	 * @param n1 - node 1
	 * @param n2 - node 2
	 */
	public HuffmanNode(HuffmanNode n1, HuffmanNode n2) {
		//System.out.println("merge " + n1.symbolToString() + " with " + n2.symbolToString());
		double p1 = n1.prob();
		double p2 = n2.prob();
		prob = p1 + p2;
		Object[] sym1,sym2;
		if (p1 <= p2) {
			sym1 = n1.symbol();
			sym2 = n2.symbol();
			left = n1;
			right = n2;
		} else {
			sym1 = n2.symbol();
			sym2 = n1.symbol();
			left = n2;
			right = n1;
		}
		symbol = new Object[sym1.length + sym2.length];
		System.arraycopy(sym1, 0, symbol, 0, sym1.length);
		System.arraycopy(sym2, 0, symbol, sym1.length, sym2.length);
	}
	
	public HuffmanNode left() {
		return left;
	}
	
	public HuffmanNode right() {
		return right;
	}
	
	public Object[] symbol() {
		return symbol;
	}
	
	public double prob() {
		return prob;
	}
	
	public String symbolToString() {
		String str = "{";
		for (Object o : symbol)
			str += o + ",";
		str +="}";
		return str;
	}

	@Override
	public int compareTo(HuffmanNode n) {
		if (this.prob() < n.prob())
			return -1;
		if (this.prob() > n.prob())
			return 1;
		if (this.symbol().length < n.symbol().length)
			return -1;
		if (this.symbol().length > n.symbol().length)
			return 1;
		// don't do this, it negates the use of permutations
		//return ((Comparable)(this.symbol[0])).compareTo(((Comparable)(n.symbol()[0])));
		return 0; // preserve original order of symbols
	}
}
