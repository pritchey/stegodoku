package libPhil.sudoku;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.Random;

public class SudokuPuzzleMaker {
	private int[][] board;
	private int[][] puzzle;
	private int numBlanks;

	private static BufferedWriter outputStream;

	public static int TIME_LIMIT = 200; // milliseconds

	class BFSPMslave implements Runnable {
		private long t;
		private int[][] board;
		private long lb;
		private long ub;
		private long cnt;

		/**
		 * brute force search a range of 17- and 18-clue puzzle masks
		 * @param board - board to make into a puzzle
		 * @param lb - lower bound of range to search
		 * @param ub - upper bound of range to search
		 */
		public BFSPMslave(int[][] board, long lb, long ub) {
			this.board = copyBoard(board);
			this.lb = lb;
			this.ub = ub;
			this.cnt = 0;
		}

		@Override
		public void run() {
			this.t = System.currentTimeMillis();
			makePuzzle();
		}

		/**
		 * try all possible symmetric 17- and 18-clue puzzles configurations
		 * between lb and ub (class instance variables)
		 */
		public void makePuzzle() {
			long m = lb;
			long t1 = System.currentTimeMillis();
			int[][] b = new int[9][9];
			while (m <= ub) {
				while (Long.bitCount(m++) != 9);

				String bits = Long.toBinaryString(m-1);
				int k = bits.length()-1;
				for (int i = 0; i < 9; i++)
					for (int j = 0; j < 9; j++)
						if (k >=0 && bits.charAt(k--)=='1') {
							b[i][j] = board[i][j];
							b[8-i][8-j] = board[8-i][8-j];
						} else {
							b[i][j] = 0;
							b[8-i][8-j] = 0;
						}

				try {
					SudokuSolutionCounter.setLimit(2);

					int nc = SudokuSolutionCounter.countSolutions(b);
					if (nc == 1) {
						SudokuBoard.printOneLineBoard(b);
						System.out.println((System.currentTimeMillis()-t1) + " ms, cnt = " + cnt);
					}
				} catch (Exception e) {
					// more than 1 solution found
				}

				cnt++;
			}
			//System.out.println("cnt = " + cnt);
		}

		public long cnt() {
			return this.cnt;
		}
	}

	class SPMslave implements Runnable {
		private int[][] board;
		private int[][] puzzle;
		private int numBlanks;
		private int recursionCount;
		private long t;
		private StringBuilder results;
		private final BitSet unavoidables;


		/**
		 * construct a new SPMslave to use for finding a sudoku board
		 * @param board - initial board
		 */
		public SPMslave(int[][] board) {
			this(board,null);
		}

		/**
		 * construct a new SPMslave to use for finding a sudoku board
		 * @param board - initial board
		 * @param unavoidables - unavoidable clues (saves time by precomputing once for each board)
		 */
		public SPMslave(int[][] board, final BitSet unavoidables) {
			this.board = copyBoard(board);
			this.puzzle = copyBoard(board);
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++) 
					this.numBlanks += board[i][j] == 0 ? 1 : 0;
			this.recursionCount = 0;
			this.results = new StringBuilder();
			this.unavoidables = unavoidables;
		}

		@Override
		public void run() {
			this.t = System.currentTimeMillis();
			makePuzzle();
		}

		/**
		 * makes a sudoku puzzle
		 */
		public void makePuzzle() {
			try {
				makePuzzle(this.board,this.numBlanks);
				//makePuzzle2();
			} catch (Exception e) {
				//System.out.println(e.getMessage());
			}
		}



		/**
		 * make a sudoku puzzle from nothing
		 */
		private void makePuzzle2() {
			int[][] board = new int[9][9];
			for (int i = 0; i < 81; i++)
				board[i/9][i%9] = 0;

			Random random = new Random(System.currentTimeMillis());

			LinkedList<int[][]> S = new LinkedList<int[][]>();
			S.add(board);

			numBlanks = 0;

			// depth first search
			while (!S.isEmpty()) {
				int[][] b = copyBoard(S.pop());

				int nb = 0;
				for (int i = 0; i < 81; i++)
					if (b[i/9][i%9] == 0)
						nb++;

				if (nb < numBlanks)
					continue;

				if (nb <= 64) {
					// 17 or more clues, could be uniquely solvable
					try {
						SudokuSolutionCounter.setLimit(2);
						int nSol;
						nSol = SudokuSolutionCounter.countSolutions(b);
						if (nSol == 1) {
							if (nb > numBlanks) {
								puzzle = copyBoard(b);
								numBlanks = nb;
								//SudokuBoard.printOneLineBoard(b);
								writeResults(nb + "\t" + (System.currentTimeMillis()-t));
							}
							continue;
						}
					} catch (Exception e) {
						// more than 1 solution found
					}
				} // else less than 17 clues, definitely not uniquely solvable

				recursionCount++;
				if (System.currentTimeMillis() - t > 1000) {
					//throw new Exception("(" + Thread.currentThread().getId() + ") 1 second time limit reached");
					System.out.println("(" + Thread.currentThread().getId() + ") 1 second time limit reached");
					return;
				}

				SudokuSolver ss = new SudokuSolver(b);
				ss.solveCells();
				LinkedList<Integer> c = new LinkedList<Integer>();
				int nc = 0;
				for (int i = 0; i < 81; i++) {
					if (ss.board(i/9, i%9) > 0)
						continue;
					int nc2 = ss.candidates(i/9, i%9).size();
					if (nc2 > nc) {
						nc = nc2;
						c.clear();
						c.add(i);
					}
					else if (nc2 == nc)
						c.add(i);
				}

				// pick a least constrained cell at random
				int r = random.nextInt(c.size());
				int row = c.get(r)/9;
				int col = c.get(r)%9;

				// set it variously
				for (Integer j : ss.candidates(row, col)) {
					int[][] b2 = copyBoard(b);
					b2[row][col] = j;
					S.push(b2);
				}
			}
		}

		/**
		 * find a sudoku puzzle
		 * @param board - starting point
		 * @param nb - number of blanks in puzzle
		 * @throws Exception if recursion or blank limit is reached
		 */
		private void makePuzzle(int[][] board, int nb) throws Exception {
			if (nb > 64)
				return;
			recursionCount++;
			if (System.currentTimeMillis() - t > TIME_LIMIT) {
				//System.out.println("(" + Thread.currentThread().getId() + ") 1 minute time limit reached");
				throw new Exception("(" + Thread.currentThread().getId() + ") "+TIME_LIMIT+" ms time limit reached");
			}
			SudokuSolutionCounter.setLimit(2);
			try {
				int nSol = SudokuSolutionCounter.countSolutions(board);
				if (nSol == 1) {
					//System.out.println("single solution");
					if (nb > numBlanks) {
						puzzle = copyBoard(board);
						numBlanks = nb;
						writeResults(nb + "\t" + (System.currentTimeMillis()-t));
						//System.out.println(nb + "\t" + (System.currentTimeMillis()-t));
						//System.out.println("(" + Thread.currentThread().getId() + ") numBlanks = " + numBlanks + " ("+(81-numBlanks)+" clues)\t" + recursionCount);
					}
					if (nb == 64) {
						SudokuBoard.printOneLineBoard(puzzle);
						System.out.println(" i swear it has 17 clues");
					}
				} else {
					//System.out.println(nSol + " solutions");
					return;
				}
			} catch (Exception e) {
				//System.out.println(e.getMessage());
				//System.out.println(" >= " + ssc.totalCount() + " solutions");
				return;
			}

			int[][] board2;
			int row,col;
			LinkedList<Integer> c = new LinkedList<Integer>();
			Random r = new Random(System.currentTimeMillis());

			/*
			// go by most constrained
			// slow and not effective
			board2 = copyBoard(board);
			int min_nc = 8*9+8;
			for (int i = 0; i < 41; i++) {
				row = i/9;
				col = i%9;
				if (board[row][col] > 0) {
					board2[row][col] = 0;
					board2[8-row][8-col] = 0;

					SudokuBoard b = new SudokuBoard(board2);
					int nc1 = 9*(b.candidates(row, col).size()-1) + b.candidates(8-row, 8-col).size()-1;
					int nc2 = b.candidates(row, col).size()-1 + 9*(b.candidates(8-row, 8-col).size()-1);
					int nc = Math.min(nc1, nc2);
					if (nc == min_nc)
						c.add(i);
					else if (nc < min_nc) {
						c.clear();
						c.add(i);
						min_nc = nc;
					}

					board2[row][col] = board[row][col];
					board2[8-row][8-col] = board[8-row][8-col];
				}
			}
			 */


			// go randomly
			c = new LinkedList<Integer>();
			for (int i = 0; i < 81; i++)
				if (board[i/9][i%9] > 0 && (unavoidables == null || !unavoidables.get(i)))
					c.add(i);

			// go in order of most constrained
			// is it still slow and ineffective, or did the first attempt just suck?
			/*
			c = new LinkedList<Integer>();
			int nc = 10;
			for (int i = 0; i < 81; i++) {
				if (board[i/9][i%9] > 0) {
					board2 = copyBoard(board);
					SudokuBoard b2 = new SudokuBoard(board2);
					b2.unset(i/9, i%9, false);
					if (b2.candidates(i/9, i%9).size() < nc) {
						c.clear();
						nc = b2.candidates(i/9, i%9).size();
						c.add(i);
					} else if (b2.candidates(i/9, i%9).size() == nc) {
						c.add(i);
					}
				}
			}
			 */


			while (!c.isEmpty()) {
				//board2 = copyBoard(board);
				int i = c.remove(r.nextInt(c.size())); // the random part
				row = i/9;
				col = i%9;
				int rc = board[row][col];
				/*
				// go symmetrically
				int rc8 = board[8-row][8-col];

				//System.out.println("delete ("+row + "," +col+") and ("+(8-row) + "," +(8-col) + ")");
				board[row][col] = 0;
				board[8-row][8-col] = 0;
				int d = (i == 40) ? 1 : 2;

				makePuzzle(board,nb+d);

				board[row][col] = rc;//board2[row][col];
				board[8-row][8-col] = rc8;//board2[8-row][8-col];
				//System.out.println("reset ("+row + "," +col+") = "+board[row][col]+" and ("+(8-row) + "," +(8-col) + ") = " + board[8-row][8-col]);
				 */

				// go asymmetrically
				board[row][col] = 0;
				makePuzzle(board,nb+1);
				board[row][col] = rc;

			}

			/*
			// go in order
			c = new LinkedList<Integer>();
			for (int i = 0; i < 41; i++)
				if (board[i/9][i%9] > 0)
					c.add(i);
			while (!c.isEmpty()) {
				board2 = copyBoard(board);
				int i = c.remove();
				row = i/9;
				col = i%9;
				board2[row][col] = 0;
				board2[8-row][8-col] = 0;
				int d = (i == 40) ? 1 : 2;

				makePuzzle(board2,nb+d);

				board2[row][col] = board[row][col];
				board2[8-row][8-col] = board[8-row][8-col];
			}
			 */

		}

		/**
		 * 
		 * @return number of blanks
		 */
		public int numBlanks() {
			return numBlanks;
		}

		/**
		 * 
		 * @return copy of the board
		 */
		public int[][] board() {
			return copyBoard(board);
		}

		/**
		 * 
		 * @return copy of the puzzle
		 */
		public int[][] puzzle() {
			return copyBoard(puzzle);
		}

	}

	public static void writeResults(String results) {
		synchronized(outputStream) {
			try {
				outputStream.write(results);
				outputStream.newLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * create a new puzzle maker
	 * @param board - solved board
	 * @param bLimit - limit on number of blanks
	 * @param rLimit - limit on number of recursions
	 */
	public SudokuPuzzleMaker(int[][] board) {
		this.board = copyBoard(board);
		this.puzzle = copyBoard(board);
	}

	/**
	 * run several threads to find a puzzle
	 * @param numThreads
	 */
	public int[][] makePuzzle(int numThreads) {
		SPMslave[] slaves = new SPMslave[numThreads];
		//BFSPMslave[] slaves = new BFSPMslave[numThreads];
		Thread[] threads = new Thread[numThreads];
		int i;
		//double d = ((((1l<<9l)-1l)<<32l) + ((1l<<9l)-1l))/(double)(numThreads);
		//System.out.println("d = " + d);
		final BitSet unavoidables = UnavoidableSetFinder.unavoidableClues(board);
		for (i= 0; i < numThreads; i++) {
			//long lb = (long)((1<<9)-1 + d*i);
			//long ub = (long)(lb + d*(i+1));
			//System.out.println(lb + " " + ub);
			//slaves[i] = new BFSPMslave(board,lb,ub);
			slaves[i] = new SPMslave(board,unavoidables);
			//slaves[i] = new SPMslave(board);
			threads[i] = new Thread(slaves[i]);
			threads[i].start();
		}
		i = 0;
		while (i < threads.length) {
			i = 0;
			//int cnt = 0;
			//for (int j = 0; j < slaves.length; j++)
			//	cnt += slaves[j].cnt();
			//System.out.println(cnt + " masks checked");
			for (Thread t : threads)
				if (t.getState().equals(Thread.State.TERMINATED))
					i++;
			try {
				//System.out.println((numThreads-i) + " threads remain (sleeping for " + (1000*(numThreads-i)) + " ms).");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		int maxI = 0;
		for (i = 1; i < numThreads; i++)
			if (slaves[i].numBlanks() > slaves[maxI].numBlanks())
				maxI = i;
		this.puzzle = slaves[maxI].puzzle();
		this.numBlanks = slaves[maxI].numBlanks();

		return copyBoard(this.puzzle);
	}

	/**
	 * 
	 * @return maximum number of blanks found for board
	 */
	public int numBlanks() {
		return this.numBlanks;
	}

	public static int numBlanks(int[][] puzzle) {
		int nb = 0;
		for (int[] r : puzzle)
			for (int n : r)
				if (n == 0)
					nb++;
		return nb;
	}

	public static int numClues(int[][] puzzle) {
		return 81 - numBlanks(puzzle);
	}

	/**
	 * copy the given board
	 * @param board
	 * @return
	 */
	public static int[][] copyBoard(int[][] board) {
		int[][] board2 = new int[9][9];
		for (int i = 0; i < 9; i++)
			System.arraycopy(board[i], 0, board2[i], 0, 9);
		return board2;
	}

	/**
	 * return a copy of the puzzle
	 * @return
	 */
	public int[][] puzzle() {
		return copyBoard(puzzle);
	}

	//	/**
	//	 *  build up a puzzle randomly by picking rarest candidates
	//	 *  does not work well.
	//	 */
	//	public static int[][] shittyIdea() {
	//		Random random = new Random(System.currentTimeMillis());
	//		SudokuSolutionCounter ssc = new SudokuSolutionCounter(new SudokuBoard(),500);
	//		try {
	//			ssc.countSolutions();
	//		} catch (Exception e) {}
	//		int[][] puzzle = new int[9][9];
	//		for (int i = 0; i < 9; i++)
	//			for (int j = 0; j < 9; j++)
	//				puzzle[i][j] = 0;
	//		while (ssc.totalCount() > 1) {
	//			//System.out.println("add the rarest next clue");
	//
	//			String[] solvedBoards = ssc.solvedBoards();
	//			int[][] f = new int[81][10];
	//			for (int i = 0; i < 81; i++)
	//				for (int j = 0; j < 10; j++)
	//					f[i][j] = 0;
	//			for (String sb : solvedBoards)
	//				for (int i = 0; i < 81; i++)
	//					if (puzzle[i/9][i%9] == 0)
	//						f[i][sb.charAt(i)-'0']++;
	//			// f[i][j] is the frequency of j at cell i
	//
	//			LinkedList<Integer>[] j_min = new LinkedList[81];
	//			for (int i = 0 ; i < 81; i++) {
	//				j_min[i] = null;
	//				if (puzzle[i/9][i%9] == 0) {		
	//					for (int j = 1; j <= 9; j++)
	//						if (f[i][j] > 0 && f[i][j] < solvedBoards.length) {
	//							if (j_min[i] == null) {
	//								j_min[i] = new LinkedList<Integer>();
	//								j_min[i].add(j);
	//							}
	//							if (f[i][j] < f[i][j_min[i].peek()]) {
	//								j_min[i].clear();
	//								j_min[i].add(j);
	//							} else if (f[i][j] == f[i][j_min[i].peek()])
	//								j_min[i].add(j);
	//						}
	//
	//				}
	//			}
	//			// j_min[i] is the list of rarest numbers at cell i, or null if cell is already set (or as good as set)
	//
	//			LinkedList<Integer> jj_min = new LinkedList<Integer>();
	//			for (int i = 0 ; i < 81; i++) {
	//				if (j_min[i] != null) {
	//					if (jj_min.isEmpty() && f[i][j_min[i].peek()] > 0)
	//						jj_min.add(i);
	//					if (f[i][j_min[i].peek()] < f[i][j_min[jj_min.peek()].peek()]) {
	//						jj_min.clear();
	//						jj_min.add(i);
	//					} else if (f[i][j_min[i].peek()] == f[i][j_min[jj_min.peek()].peek()])
	//						jj_min.add(i);
	//				}
	//			}
	//			// jjmin is the list of cells which have the rarest candidates
	//
	//			// j_min[jj_min] are the rarest candidate for the jj_min-th cells
	//			// set random cell from jj_min to random value from j_min[jj_min]
	//			int rc = jj_min.get(random.nextInt(jj_min.size()));
	//			int rn = j_min[rc].get(random.nextInt(j_min[rc].size()));
	//			puzzle[rc/9][rc%9] = rn;
	//			//System.out.println("set ("+rc/9+","+rc%9+") = " + rn);
	//
	//			// recount!
	//			try {
	//			ssc = new SudokuSolutionCounter(new SudokuBoard(puzzle),500);
	//				//System.out.println(ssc.countSolutions() + " solutions found");
	//			} catch (Exception e) {
	//				//System.out.println("more than 500 solutions found");
	//			}
	//		}
	//
	//		//SudokuBoard.print(puzzle);
	//		return puzzle;
	//	}

	//	public static int[][] slightlyLessShittyIdea(int[][] board) {
	//		Random random = new Random(System.currentTimeMillis());
	//		//long t1 = System.nanoTime();
	//		BitSet clues = UnavoidableSetFinder.unavoidableClues(board);
	//		//long t2 = System.nanoTime();
	//		//System.out.printf("%.4f ms\n",(t2-t1)/1000000.0);
	//
	//		//System.out.println(clues.cardinality() + " unavoidable clues: " + clues);
	//
	//		int[][] puzzle = new int[9][9];
	//		for (int i = 0; i < 9; i++)
	//			for (int j = 0; j < 9; j++)
	//				if (clues.get(9*i+j))
	//					puzzle[i][j] = board[i][j];
	//				else
	//					puzzle[i][j] = 0;
	//
	//		//SudokuBoard.print(puzzle);
	//		try {
	//			SudokuSolutionCounter ssc = new SudokuSolutionCounter(new SudokuBoard(puzzle),500);
	//			try {
	//				ssc.countSolutions();
	//				//System.out.println(ssc.countSolutions() + " solutions found");
	//			} catch (Exception e) {
	//				//System.out.println("> 500 solutions found");
	//			}
	//
	//			/*
	//		System.out.println("make puzzle symmetric");
	//		for (int i = 0; i < 9; i++)
	//			for (int j = 0; j < 9; j++)
	//				if (clues.get(9*i+j))
	//					puzzle[8-i][8-j] = b.board(8-i, 8-j);
	//
	//		SudokuBoard.print(puzzle);
	//
	//
	//		ssc = new SudokuSolutionCounter(new SudokuBoard(puzzle),500);
	//		try {
	//			System.out.println(ssc.countSolutions() + " solutions found");
	//		} catch (Exception e) {
	//			System.out.println("more than 500 solutions found");
	//		}
	//			 */
	//
	//			while (ssc.totalCount() > 1) {
	//				//System.out.println("add the rarest next clue");
	//
	//				String[] solvedBoards = ssc.solvedBoards();
	//				int[] f = new int[81];
	//				for (int i = 0; i < 81; i++)
	//					for (int j = 0; j < 10; j++)
	//						f[i] = 0;
	//				for (String sb : solvedBoards)
	//					for (int i = 0; i < 81; i++)
	//						if (sb.charAt(i)-'0' == board[i/9][i%9])
	//							f[i]++;
	//				// f[i] is the frequency of the correct number for cell i
	//
	//				LinkedList<Integer> i_min = new LinkedList<Integer>();
	//				for (int i = 0; i < 81; i++) {
	//					if (puzzle[i/9][i%9] == 0) {
	//						if (i_min.isEmpty())
	//							i_min.add(i);
	//						if (f[i] < f[i_min.peek()]) {
	//							i_min.clear();
	//							i_min.add(i);
	//						} else if (f[i] == f[i_min.peek()])
	//							i_min.add(i);
	//					}
	//				}
	//				// i_min is the list of cells with rarest correct answers
	//
	//				// set random cell from i_min
	//				int rc = i_min.get(random.nextInt(i_min.size()));
	//				puzzle[rc/9][rc%9] = board[rc/9][rc%9];
	//				//System.out.println("set ("+rc/9+","+rc%9+") = " + board[rc/9][rc%9]);
	//
	//				// recount!
	//				ssc = new SudokuSolutionCounter(new SudokuBoard(puzzle),500);
	//				try {
	//					ssc.countSolutions();
	//					//System.out.println(ssc.countSolutions() + " solutions found");
	//				} catch (Exception e) {
	//					//System.out.println("> 500 solutions found");
	//				}
	//			}
	//		} catch (Exception e) {
	//		}
	//
	//		//SudokuBoard.print(puzzle);
	//		return puzzle;
	//	}

	/**
	 * create a puzzle by doing a random DFS on clue configurations
	 * @param board
	 * @return puzzle found
	 */
	public static int[][] randomDFS(int[][] board) {
		Random random = new Random(System.currentTimeMillis());
		int [][] puzzle = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				puzzle[i][j] = board[i][j];
		int[] lc = new int[2];
		LinkedList<int[]> l = new LinkedList<int[]>();
		SudokuSolutionCounter ssc;
		while(true) {
			// find all clues that can be removed
			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9; j++) {
					if (puzzle[i][j] == 0)
						continue;
					// backup clue
					int k1 = puzzle[i][j];
					//int k2 = puzzle[8-i][8-j];
					// remove clue
					puzzle[i][j] = 0;
					//puzzle[8-i][8-j] = 0;
					//count solutions
					SudokuSolutionCounter.setLimit(2);
					try {
						int ns = SudokuSolutionCounter.countSolutions(puzzle);
						if (ns == 1) // add clue to list of removable clues
							l.add(new int[]{i,j});
					} catch (Exception e1) {}
					// restore clue
					puzzle[i][j] = k1;
					//puzzle[8-i][8-j] = k2;
				}
			}
			if (l.isEmpty()) {
				// put the last clue removed back and break
				puzzle[lc[0]][lc[1]] = board[lc[0]][lc[1]];
				break;
			}
			// remove a random removable clue
			int r = random.nextInt(l.size());
			int[] c = l.remove(r);
			lc[0] = c[0];
			lc[1] = c[1];
			puzzle[c[0]][c[1]] = 0;
			//puzzle[8-c[0]][8-c[1]] = 0;
			l.clear();
		}
		return puzzle;
	}

	/**
	 * use a greedy approach to find the smallest symmetric set of clues that make the puzzle solvable
	 * @param board - [solved] board
	 * @param seed - RNG seed
	 * @return the constructed puzzle
	 */
	public static int[][] findSymMinClueSet(int[][] board, long seed) {
		// guarantee that the board is solved
		SudokuSolver ss = new SudokuSolver(board);
		board = ss.solvedBoard();
		//System.out.println("findSymMCS");
		//SudokuBoard.printOneLineBoard(board);

		// must be deterministic for stego to work
		Random random = new Random(seed);

		int[][] puzzle = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				puzzle[i][j] = 0;
		int nc = 0;

		LinkedList<int[]> l = new LinkedList<int[]>();

		boolean s = false;
		while(!s) {
			int min_c = Integer.MAX_VALUE;
			l.clear();

			for (int i = 0; i < 5; i++) {
				for (int j = 0; (i < 4 && j < 9) || j < 5; j++) {
					if (puzzle[i][j] > 0)
						continue;

					puzzle[i][j] = board[i][j];
					puzzle[8-i][8-j] = board[8-i][8-j];

					ss = new SudokuSolver(puzzle);
					ss.solveCells();
					if (ss.isSolved()) {
						s = true;
					}
					int c = 0;
					for (int i2 = 0; i2 < 9; i2++)
						for (int j2 = 0; j2 < 9; j2++)
							c += ss.candidates(i2, j2).size();
					if (c < min_c)
						l.clear();
					if (c <= min_c) {
						min_c = c;
						l.add(new int[]{i,j});
					}

					puzzle[i][j] = 0;
					puzzle[8-i][8-j] = 0;
				}
			}
			int r = random.nextInt(l.size());
			int[] c = l.remove(r);
			puzzle[c[0]][c[1]] = board[c[0]][c[1]];
			puzzle[8-c[0]][8-c[1]] = board[8-c[0]][8-c[1]];
			nc += (c[0] == c[1] && c[0] == 4) ? 1 : 2;
			//System.out.println(nc + " clues, " + min_c + " candidates left on board");
		}
		//SudokuBoard.printOneLineBoard(puzzle);
		
		// clean out clues that maybe we dont actually need
		for (int i = 0; i < 9; i++) {
			for (int j = 0; (i < 4 && j < 9) || j < 5; j++) {
				if (puzzle[i][j] == 0)
					continue;
				int n = puzzle[i][j];
				int m = puzzle[8-i][8-j];
				puzzle[i][j] = 0;
				puzzle[8-i][8-j] = 0;
				ss = new SudokuSolver(puzzle);
				ss.solveCells();
				if (!ss.isSolved()) {
					puzzle[i][j] = n;
					puzzle[8-i][8-j] = m;
				}
			}
		}

		return puzzle;
	}

	/**
	 * use a greedy approach to find the smallest set of clues that make the puzzle solvable
	 * @param board - [solved] board
	 * @param seed - RNG seed
	 * @return the constructed puzzle
	 */
	public static int[][] findMinClueSet(int[][] board, long seed) {
		// guarantee that the board is solved
		SudokuSolver ss = new SudokuSolver(board);
		board = ss.solvedBoard();
		//System.out.println("findMCS");
		//SudokuBoard.printOneLineBoard(board);

		// must be deterministic for stego to work
		Random random = new Random(seed);

		int[][] puzzle = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				puzzle[i][j] = 0;
		int nc = 0;

		LinkedList<int[]> l = new LinkedList<int[]>();

		boolean s = false;
		while(!s) {
			int min_c = Integer.MAX_VALUE;
			l.clear();
			//int min_i = -1;
			//int min_j = -1;

			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9; j++) {
					if (puzzle[i][j] > 0)
						continue;

					//if (min_i == -1) {
					//	min_i = i;
					//	min_j = j;
					//}

					puzzle[i][j] = board[i][j];

					ss = new SudokuSolver(puzzle);
					ss.solveCells();
					if (ss.isSolved()) {
						// note: there may be several of these
						// TODO exploit capacity!
						//SudokuBoard.printOneLineBoard(puzzle);

						// sanity check
						//						SudokuSolutionCounter ssc = new SudokuSolutionCounter(puzzle,2);
						//						try {
						//							int ns = ssc.countSolutions();
						//							if (ns == 1)
						//								s = true;
						//						} catch (Exception e) {
						//							// not actually uniquely solvable
						//							System.out.println("SudokuSolver is broken.");
						//						}
						s = true;
					}
					int c = 0;
					for (int i2 = 0; i2 < 9; i2++)
						for (int j2 = 0; j2 < 9; j2++)
							c += ss.candidates(i2, j2).size();
					if (c < min_c)
						l.clear();
					if (c <= min_c) {
						min_c = c;
						//	min_i = i;
						//	min_j = j;
						l.add(new int[]{i,j});
					}

					puzzle[i][j] = 0;
				}
			}
			int r = random.nextInt(l.size());
			int[] c = l.remove(r);
			puzzle[c[0]][c[1]] = board[c[0]][c[1]];
			nc++;
			//System.out.println(nc + " clues, " + min_c + " candidates left on board");
		}
		//SudokuBoard.printOneLineBoard(puzzle);

		// clean out clues that maybe we dont actually need
		// TODO this in deterministic random order
		// TODO this in increasing combination sizes (when none work, go back and pick one that did)
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (puzzle[i][j] == 0)
					continue;
				int n = puzzle[i][j];
				puzzle[i][j] = 0;
				ss = new SudokuSolver(puzzle);
				ss.solveCells();
				if (!ss.isSolved()) {
					puzzle[i][j] = n;
				}
			}
		}

		return puzzle;
	}

	/**
	 * run many random DFS calculations and pick top clues until valid.
	 * cannot be used for stego.
	 * terrible. horrendous. awful.
	 * @param board
	 * @return puzzle
	 */
	public static int[][] iteratedDFS(int[][] board) {
		// guarantee that the board is solved
		SudokuSolver ss = new SudokuSolver(board);
		board = ss.solvedBoard();

		// init frequencies
		int[][] f = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				f[i][j] = 0;

		// compute frequencies
		for (int k = 0; k < 10; k++) {
			int[][] p = randomDFS(board);
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					if (p[i][j] > 0)
						f[i][j]++;
		}

		// print frequencies
		//		System.out.println();
		//		for (int i = 0; i < 9; i++) {
		//			for (int j = 0; j < 9; j++)
		//				System.out.printf("%2d ", f[i][j]);
		//			System.out.println();
		//		}

		// init puzzle
		int[][] puzzle = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				puzzle[i][j] = 0;

		boolean s = false;
		LinkedList<int[]> l = new LinkedList<int[]>();
		Random random = new Random(System.currentTimeMillis());
		int nc = 0;
		while (!s) {
			// find most common clues
			int max = 0;
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++) {
					if (f[i][j] > max)
						l.clear();
					if (f[i][j] >= max) {
						l.add(new int[]{i,j});
						max = f[i][j];
					}
				}
			//System.out.println("max = " + max);
			//System.out.println(l.size() + " cells have max freq");

			// add them in random order
			while (!l.isEmpty()) {
				int r = random.nextInt(l.size());
				int[] c = l.remove(r);
				puzzle[c[0]][c[1]] = board[c[0]][c[1]];
				//System.out.println("set ("+c[0]+","+c[1]+") = " + board[c[0]][c[1]]);
				f[c[0]][c[1]] = -1; // remove clue from future consideration
				nc++;

				// once puzzle has 17 clues, start checking for solutions
				if (nc >= 17) {
					SudokuSolutionCounter.setLimit(2);
					try {
						int ns = SudokuSolutionCounter.countSolutions(puzzle);
						if (ns == 1) {
							// unique solution found, puzzle is made.
							s = true;
							break;
						}
					} catch (Exception e) {}
				}
			}	
		}

		return puzzle;
	}

	/**
	 * determine if puzzle has symmetric clue configuration
	 * @param b - puzzle
	 * @return true if clues are symmetric
	 */
	public static boolean isSymmetric(int[][] b) {
		for (int i = 0; i < 5; i++)
			for (int j = 0; (i < 4 && j < 9) || j < 5; j++)
				if ((b[i][j] == 0 && b[8-i][8-j] != 0) ||
						(b[i][j] != 0 && b[8-i][8-j] == 0))
					return false;
		return true;
	}
	
	public static void main(String[] args) {

		System.out.println("nc_sym,nc_asym");
		for (int i = 0; i < 100; i++) {
			RandomSudoku rs = new RandomSudoku();
			int nc_sym = numClues(findSymMinClueSet(rs.board(),0));
			int nc_asym = numClues(findMinClueSet(rs.board(),0));
			System.out.println(nc_sym + "," + nc_asym);
		}


		//		Scanner s = new Scanner(System.in);
		//		System.out.println("Enter a [solved] puzzle: ");
		//		String str = s.nextLine();
		//		s.close();
		//		int[][] board = new int[9][9];
		//		for (int i = 0; i < 9; i++) {
		//			for (int j = 0; j < 9; j++) {
		//				board[i][j] = Integer.parseInt(str.substring(9*i+j, 9*i+j+1));
		//			}
		//		}
		//		SudokuSolver ss = new SudokuSolver(board);
		//		if (!ss.isSolved())
		//			if (!ss.solve()) {
		//				System.out.println("Board is not solvable");
		//				System.exit(0);
		//			}
		//		try {
		//			SudokuSolutionCounter ssc = new SudokuSolutionCounter(2);
		//			ssc.countSolutions(board);
		//		} catch (Exception e) {
		//			System.out.println("Warning: Board is not uniquely solvable");
		//		}
		//		int[][] board2 = ss.solvedBoard();

		/*
		int numClues = 18;
		SudokuPuzzleMaker spm;
		long t1,t2;
		RandomSudoku rs;
		int R = 100;
		int T = 8;
		try {
			outputStream = new BufferedWriter(new FileWriter("PuzzleMaker_results.txt"));


			// get the bad one out of the system
			rs = new RandomSudoku();

			t1 = System.currentTimeMillis();
			for (int i = 0; i < R; i++) {
				rs = new RandomSudoku();
				spm = new SudokuPuzzleMaker(rs.board());
				spm.makePuzzle(T);
				if (i % 10 == 0) {
					//System.out.println(i);
					//SudokuBoard.printOneLineBoard(spm.puzzle());
					outputStream.flush();
					System.out.print("collecting garbage...");
					System.gc();
					System.out.println("done");
				}
				System.out.println((81-spm.numBlanks()) + " clues");
				SudokuBoard.printOneLineBoard(spm.puzzle());
			}
			t2 = System.currentTimeMillis();
			outputStream.close();
			System.out.println("\ndone!");
			System.out.println((t2-t1) + " ms");
		} catch (IOException e) {
			e.printStackTrace();
		}
		 */

		//shittyIdea();

		//		for (int i = 0; i < 100; i++) {
		//			RandomSudoku rs = new RandomSudoku();
		//			//int[][] puzzle1 = slightlyLessShittyIdea(rs.board());
		//			//SudokuBoard.print(puzzle1);
		//			//System.out.println(numClues(puzzle1) + " clues");
		//			int[][] puzzle2 = new SudokuPuzzleMaker(rs.board()).makePuzzle(8);
		//			//SudokuBoard.print(puzzle2);
		//			//SudokuBoard.printOneLineBoard(puzzle2);
		//			System.out.print(numClues(puzzle2) + "\t");
		//			SudokuSolver ss = new SudokuSolver(puzzle2);
		//			ss.solveCells();
		//			if (ss.isSolved())
		//				System.out.print("Easy\t");
		//			else
		//				System.out.print("Hard\t");
		//			SudokuBoard.printOneLineBoard(puzzle2);
		//
		//		}

		//		int N = 100;
		//		long t1,t2;
		//		int nc;
		//		int [][] p;
		//		for (int j = 0; j < 30; j++) {
		//			//double m1 = 0;
		//			//double m2 = 0;
		//			for (int i = 0; i < N; i++) {
		//				RandomSudoku rs = new RandomSudoku();
		//
		//				t1 = System.currentTimeMillis();
		//				p = randomDFS(rs.board());
		//				t2 = System.currentTimeMillis();
		//				nc = numClues(p);
		//				System.out.print(nc + "," + (t2-t1));
		//
		//				t1 = System.currentTimeMillis();
		//				p = findMinClueSet(rs.board(),0);
		//				t2 = System.currentTimeMillis();
		//				nc = numClues(p);
		//				System.out.print("," + nc + "," + (t2-t1));
		//
		//				//				t1 = System.currentTimeMillis();
		//				//				p = iteratedDFS(rs.board());
		//				//				t2 = System.currentTimeMillis();
		//				//				nc = numClues(p);
		//				//				System.out.print("," + nc + "," + (t2-t1));
		//
		//				//m1 += nc1;
		//				//m2 += nc2;
		//				System.out.println();
		//			}
		//			//System.out.println(m1/N + "," + m2/N);
		//		}

		//		t1 = System.currentTimeMillis();
		//		spm.makePuzzle(1);
		//		t2 = System.currentTimeMillis();
		//		System.out.println(spm.numBlanks() + " blanks");
		//		System.out.println((t2-t1) + " milliseconds");
		//		SudokuBoard.print(spm.puzzle());
		//		String c = new SudokuBoard(spm.puzzle()).compressedBoard();
		//		System.out.println(c + " ("+c.length()+" bits)");

		//		for (int i = 0; i < 100; i++) {
		//			spm = new SudokuPuzzleMaker(board2,81-numClues,100);
		//			t1 = System.currentTimeMillis();
		//			spm.makePuzzle(1);
		//			t2 = System.currentTimeMillis();
		//			System.out.println(spm.numBlanks() + "\t" + (t2-t1));
		//		}
	}
}
