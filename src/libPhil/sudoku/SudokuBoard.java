package libPhil.sudoku;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

public class SudokuBoard {
	private int[][] board;
	private LinkedList<Integer>[][] candidates;

	public SudokuBoard() {
		board = new int[9][9];
		candidates = new LinkedList[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				board[i][j] = 0;
				candidates[i][j] = new LinkedList<Integer>();
				for (int k = 1; k <= 9; k++)
					candidates[i][j].add(k);
			}
		}
	}

	/**
	 * copy constructor
	 * @param b - board matrix to copy
	 * @throws Exception 
	 */
	public SudokuBoard(int[][] b) throws Exception {
		this();
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (!set(i,j,b[i][j]))
					throw new Exception("cell ("+i+","+j+") not set [to "+b[i][j]+"]");
			}
		}
	}
	
	/**
	 * copy constructor
	 * @param b - one-line board string to copy
	 * @throws Exception 
	 */
	public SudokuBoard(String b) throws Exception {
		this();
		b = b.replaceAll("[0.*?_ -]", "0");
		for (int k = 0; k < b.length(); k++)
			set(k/9,k%9,b.charAt(k)-'0');
			//if (!set(k/9,k%9,b.charAt(k)-'0'))
			//	throw new Exception("cell not set");
	}

	/**
	 * copy constructor
	 * @param b - SudokuBoard to copy
	 * @throws Exception 
	 */
	public SudokuBoard(SudokuBoard b) throws Exception {
		this(b.board());
	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @return value at specified position on board
	 */
	public int board(int row, int col) {
		return board[row][col];
	}

	public LinkedList<Integer> candidates(int row, int col) {
		return candidates[row][col];
	}

	/**
	 * set the given cell to the given number
	 * @param row
	 * @param col
	 * @param num
	 * @return true if cell was set
	 */
	public boolean set(int row, int col, int num) {
		//if (num == -1)
		//	try {
		//		throw new Exception("attempt to set to -1");
		//	} catch (Exception e) {
		//		// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
		return set(row,col,num,false);
	}

	/**
	 * set the given cell to the given number
	 * @param row
	 * @param col
	 * @param num
	 * @param debug - if true, will print debug messages
	 * @return true if cell was set
	 */
	public boolean set(int row, int col, int num, boolean debug) {
		if (row < 0 || row >= 9 || col < 0 || col >= 9) // ignore out of bounds
			return false;
		if (board[row][col] > 0) {
			if (debug) System.out.println("("+(char)('A'+row)+","+(col+1)+") already set to " + board[row][col]);
			return false; 
		}
		if (num == 0 || board[row][col] == num) // already set
			return true;
		for (int i = 0; i < 9; i++)
			if (board[row][i] == num || board[i][col] == num) {
				if (debug) System.out.println("already exists in row or column");
				return false;
			}
		for (int i = (row/3)*3; i < 3*((row/3)+1); i++)
			for (int j = (col/3)*3; j < 3*((col/3)+1); j++)
				if (board[i][j] == num) { 
					if (debug) System.out.println("already exists in box");
					return false;
				}

		for (int i = 0; i < 9; i++) {
			if (debug) {
				System.out.print("("+row +","+i+"): ");
				for (int n : candidates[row][i])
					System.out.print(n + " ");
				System.out.print(" --> ");
			}
			candidates[row][i].remove(new Integer(num));
			if (debug) {
				for (int n : candidates[row][i])
					System.out.print(n + " ");
				System.out.println();
			}

			if (debug) {
				System.out.print("("+i+","+col+"): ");
				for (int n : candidates[i][col])
					System.out.print(n + " ");
				System.out.print(" --> ");
			}
			candidates[i][col].remove(new Integer(num));
			if (debug) {
				for (int n : candidates[i][col])
					System.out.print(n + " ");
				System.out.println();
			}
		}
		for (int i = (row/3)*3; i < 3*((row/3)+1); i++)
			for (int j = (col/3)*3; j < 3*((col/3)+1); j++) {
				if (debug) {
					System.out.print("("+i +","+j+"): ");
					for (int n : candidates[i][j])
						System.out.print(n + " ");
					System.out.print(" --> ");
				}
				candidates[i][j].remove(new Integer(num));
				if (debug) {
					for (int n : candidates[i][j])
						System.out.print(n + " ");
					System.out.println();
				}
			}
		candidates[row][col].clear();
		//candidates[row][col].add(num);
		board[row][col] = num;
		if (debug) System.out.println("set cell ("+row+","+col+") => "+num+".");
		return true;
	}
	
	/**
	 * unset cell at row,col
	 * @param row
	 * @param col
	 */
	public void unset(int row, int col, boolean debug) {
		if (row < 0 || row >= 9 || col < 0 || col >= 9) // ignore out of bounds
			return;
		if (board[row][col] == 0 ) {
			if (debug) System.out.println("("+(char)('A'+row)+","+(col+1)+") already not set");
			return; 
		}
		board[row][col] = 0;
		// recompute candidates;
		SudokuBoard b;
		try {
			b = new SudokuBoard(this);
			SudokuSolver ss = new SudokuSolver(b);
			while (ss.solveCells());
			if (ss.board(row, col) != 0) {
				candidates[row][col].clear();
				candidates[row][col].add(ss.board(row, col));
			}	
		} catch (Exception e) {
		}
		
	}
	
	/**
	* determine if specified cell is empty
	* @param row
	* @param col
	* @return true iff cell is empty
	*/
	public boolean isEmpty(int row, int col) {
		if (row < 0 || row >= 9 || col < 0 || col >= 9) // ignore out of bounds
			return false;
		return board[row][col] == 0;		
	}
	
	/**
	* determine if specified cell is set
	* @param row
	* @param col
	* @return true iff cell is set
	*/
	public boolean isSet(int row, int col) {
		return !isEmpty(row,col);
	}

	/**
	 * 
	 * @return true if rows are all valid
	 */
	private boolean validRows() {
		for (int r = 0; r < 9; r++) {
			int[] a = new int[]{0,0,0,0,0,0,0,0,0};
			for (int c = 0; c < 9; c++) {
				if (board[r][c] == 0) continue;
				if (board[r][c] >= 1 && board[r][c] <= 9 && a[board[r][c]-1] == 0)
					a[board[r][c]-1]++;
				else {
					System.out.println("invalid at ("+r+","+c+")");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 
	 * @return true if columns are all valid
	 */
	private boolean validCols() {
		for (int c = 0; c < 9; c++) {
			int[] a = new int[]{0,0,0,0,0,0,0,0,0};
			for (int r = 0; r < 9; r++) {
				if (board[r][c] == 0) continue;
				if (board[r][c] >= 1 && board[r][c] <= 9 && a[board[r][c]-1] == 0)
					a[board[r][c]-1]++;
				else{
					System.out.println("invalid at ("+r+","+c+")");
					return false;
				}
					
			}
		}
		return true;
	}

	/**
	 * 
	 * @return true if boxes are all valid
	 */
	private boolean validBoxes() {
		for (int r = 0; r < 9; r+=3) {
			for (int c = 0; c < 9; c+=3) {
				int[] a = new int[]{0,0,0,0,0,0,0,0,0};
				for (int rr = r; rr < r+3; rr++) {
					for (int cc = c; cc < c+3; cc++) {
						if (board[rr][cc] == 0) continue;
						if (board[rr][cc] >= 1 && board[rr][cc] <= 9 && a[board[rr][cc]-1] == 0)
							a[board[rr][cc]-1]++;
						else{
							System.out.println("invalid at ("+rr+","+cc+")");
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * 
	 * @return copy of the board
	 */
	public int[][] board() {
		int[][] bc = new int[9][9];
		for (int r = 0; r < 9; r++)
			for (int c = 0; c < 9; c++)
				bc[r][c] = board[r][c];
		return bc;
	}

	/**
	 * 
	 * @return true if board is solved
	 */
	public boolean isSolved() {
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				if (board[i][j] == 0)
					return false;
		return isValid();
	}
	
	/**
	 * 
	 * @return true if all subunits are valid, even if not complete
	 */
	public boolean isValid() {
		return validRows() && validCols() && validBoxes();
	}

	/**
	 * print the sudoku board
	 */
	public void print() {
		print(board);
	}

	/**
	 * print the given sudoku board
	 * @param board
	 */
	public static void print(int[][] board) {
		for (int i = 0; i < 9; i++) {
            if (i % 3 == 0)
                System.out.println("+-------+-------+-------+");
            for (int j = 0; j < 9; j++) {
                if (j % 3 == 0)
                    System.out.print("| ");
                if (board[i][j] > 0) {
                    System.out.print(board[i][j] + " ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println("|");
        }
        System.out.println("+-------+-------+-------+");
        System.out.println();
		printOneLineBoard(board);
	}

	/**
	 * print board to file
	 * @param board - board to print
	 * @param path - file to write
	 */
	public static void print(int[][] board, String path) {
		if (path.equals(""))
			print(board);
		else {
			try {
				BufferedWriter outputStream = new BufferedWriter(new FileWriter(path));
				for (int[] r : board) {
					for (int n : r)
						outputStream.write(n + " ");
					outputStream.newLine();
				}
				outputStream.newLine();
				for (int[] r : board) {
					for (int n : r)
						outputStream.write(Integer.toString(n));
				}
				outputStream.newLine();
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * print board all on one line
	 * @param board - board to print
	 */
	public static void printOneLineBoard(int[][] board) {
		System.out.println(getOneLineBoard(board));
	}
	
	public static String getOneLineBoard(int[][] board) {
		StringBuilder sb = new StringBuilder();
		for (int[] r : board) {
			for (int n : r)
				if (n < 0)
					sb.append("0");
				else
					sb.append(n);
		}
		return sb.toString();
	}

	/**
	 * compress board to bitstring
	 * bits 0..40 encode board pattern (0 means empty, board is symmetric)
	 * bits 41..end encode numbers as reached in top-left to bottom-right order
	 * this is not the best that can be done.
	 * TODO: improve compression
	 * @return compressed board
	 */
	public static String compress(int[][] board) {
		String str = "";

		for (int i = 0; i < 41; i++)
			str += board[i/9][i%9] == 0 ? "0" : "1";

		for (int[] r : board) {
			for (int n : r) {
				switch(n) {
				case 0 :
					break;
				case 1 :
					str += "000";
					break;
				case 2 :
					str += "001";
					break;
				case 3 :
					str += "010";
					break;
				case 4 :
					str += "011";
					break;
				case 5 :
					str += "100";
					break;
				case 6 :
					str += "101";
					break;
				case 7 :
					str += "110";
					break;
				case 8 :
					str += "1110";
					break;
				case 9 :
					str += "1111";
					break;
				}
			}
		}

		return str;
	}

	public String compressedBoard() {
		return compress(board);
	}

	/**
	 * decompress the given board
	 * @param c - compressed board string
	 * @return int[][] board
	 */
	public static int[][] decompress(String c) {
		int[][] board = new int[9][9];
		for (int i = 0; i < 41; i++) {
			int row = i/9;
			int col = i%9;
			if (c.charAt(i) == '1') {
				board[row][col] = -1;
				board[8-row][8-col] = -1;
			}
			else {
				board[row][col] = 0;
				board[8-row][8-col] = 0;
			}
		}
		int j = 0;
		for (int i = 41; i < c.length();) {
			String b = c.substring(i, i+3);
			i += 3;
			if (b.compareTo("111") == 0) {
				b += c.charAt(i);
				i++;
			}
			int n = Integer.parseInt(b, 2) + 1;
			if (n > 7)
				n -= 7;
			for (int jj = j; jj < 81; jj++) {
				int row = jj/9;
				int col = jj%9;
				if (board[row][col] == 0)
					continue;
				board[row][col] = n;
				j = jj+1;
				break;
			}
		}
		return board;
	}
	
	/**
	 * 
	 * @return true if the board string represents a valid sudoku board or puzzle
	 */
	public static boolean isSudoku(String board) {
		try {
			SudokuBoard b = new SudokuBoard(board);
			return b.isValid();
		} catch (Exception e) {
			//e.printStackTrace();
			return false;
		}
	}
}
