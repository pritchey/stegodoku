package libPhil.sudoku;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.math.BigInteger;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Scanner;

import stegodoku.StegoDokuV3;
import stegodoku.StegoPuzzleMaker;
import libPhil.combinadic.Combinadic;
import libPhil.factoradic.Factoradic;
import libPhil.huffman.HuffmanTree;
import libPhil.permutation.PermutableArray;
import static libPhil.sudoku.SudokuPuzzleMaker.isSymmetric;

public class SudokuCompressor {

	/**
	 * adaptive huffman coded run lengths<br>
	 * huffman code locations as offsets from previous clue<br>
	 * and huffman code clue values.
	 * @param b - puzzle
	 * @return compressed puzzle
	 */
	public static BitSet ahcrlCompress(int[][] b) {
		BitSet bs = new BitSet();
		int idx = 0;

		// check for symmetry
		boolean sym = true;
		if (!isSymmetric(b))
			sym = false;
		bs.set(idx++,sym);

		int k = 0;
		int k2 = 0;
		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});
		for (int i = 0; i < 9; i++) {
			if (sym && i >= 5) // symmetric and past the middle, done
				break;
			for (int j = 0; j < 9; j++) {
				if (b[i][j] == 0)
					k++;
				else {
					Integer[] loc = new Integer[81-k2];
					double[] prob = new double[81-k2];
					double sum = 0;
					for (int i2 = 0; i2 < loc.length; i2++) {
						loc[i2] = i2;
						prob[i2] = 0.4*Math.exp(-0.4*i2);
						sum += prob[i2];
					}
					for (int i2 = 0; i2 < prob.length; i2++)
						prob[i2] /= sum;
					HuffmanTree rlTree = new HuffmanTree(loc,prob);
					String c = rlTree.encode(k);
					for (int i2 = 0; i2 < c.length(); i2++)
						bs.set(idx++,c.charAt(i2)=='1');
					c = numTree.encode(b[i][j]);
					for (int i2 = 0; i2 < c.length(); i2++)
						bs.set(idx++,c.charAt(i2)=='1');
					if (sym) {
						// do sym clue as well
						c = numTree.encode(b[8-i][8-j]);
						for (int i2 = 0; i2 < c.length(); i2++)
							bs.set(idx++,c.charAt(i2)=='1');
						if (9*i + j >= 40) // at middle of symmetric board, done
							break;
					}
					k2 += k;
					k = 0;
				}
			}
		}

		return bs;
	}

	/**
	 * decompress ahcrl compressed puzzle
	 * @param bs - compressed puzzle
	 * @return - decompressed puzzle
	 */
	public static String ahcrlDecompress(BitSet bs) {

		boolean sym = bs.get(0);
		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});

		String bits = "";
		for (int i = 1; i < bs.length(); i++)
			bits += bs.get(i) ? "1" : "0";
		int k2 = 0;
		int[] board = new int[81];
		for (int i = 0; i < 81; i++)
			board[i] = 0;
		Object[] ret;
		while(!bits.isEmpty()) {
			Integer[] loc = new Integer[81-k2];
			double[] prob = new double[81-k2];
			double sum = 0;
			for (int i2 = 0; i2 < loc.length; i2++) {
				loc[i2] = i2;
				prob[i2] = 0.4*Math.exp(-0.4*i2);
				sum += prob[i2];
			}
			for (int i2 = 0; i2 < prob.length; i2++)
				prob[i2] /= sum;
			HuffmanTree rlTree = new HuffmanTree(loc,prob);
			ret = rlTree.decode_nice(bits);
			int k = (int)ret[0];
			bits = (String)ret[1];
			ret = numTree.decode_nice(bits);
			while (ret == null) {
				bits += "0";
				ret = numTree.decode_nice(bits);
			}
			int n = (int)ret[0];
			bits = (String)ret[1];
			board[k2+k] = n;
			if (sym) {
				ret = numTree.decode_nice(bits);
				while (ret == null) {
					bits += "0";
					ret = numTree.decode_nice(bits);
				}
				n = (int)ret[0];
				bits = (String)ret[1];
				board[80-(k2+k)] = n;
			}
			k2 += k+1;
		}

		String b = "";
		for (int i = 0; i < 81; i++)
			b += board[i];
		return b;
	}

	/**
	 * use the stego puzzle decoder to compress a puzzle
	 * @param p - puzzle
	 * @return compressed (decoded) puzzle
	 */
	public static BitSet puzzleStegoCompress(int[][] p) {
		BitSet bs = new BitSet();
		int idx = 0;
		String bits = StegoPuzzleMaker.decode(p);
		for (char c : bits.toCharArray())
			bs.set(idx++,c=='1');

		return bs;
	}
	/**
	 * use the stego puzzle encoder to decompress a puzzle
	 * @param bs - compressed puzzle as BitSet
	 * @return decompressed puzzle as String
	 */
	public static String puzzleStegoDecompress(BitSet bs) {
		String bits = "";
		for (int i = 0; i < bs.length(); i++)
			bits += bs.get(i) ? "1" : "0";
		Object[] ret = StegoPuzzleMaker.encode(bits);
		int[][] p = (int[][])ret[0];
		String b = "";
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				b += p[i][j];
		return b;
	}

	/**
	 * use the StegoDoku-H decoder as a compressor<br>
	 * [0]: sym<br>
	 * [1..41/81]: clue pattern
	 * [42/82..end]: compressed board
	 * @param b - puzzle
	 * @return compressed (decoded) board
	 */
	public static BitSet stegoCompress(int[][] b) {
		BitSet bs = new BitSet();
		int idx = 0;

		// check for symmetry
		boolean sym = true;
		if (!isSymmetric(b))
			sym = false;
		bs.set(idx++,sym);

		if (!sym)
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					bs.set(idx++,b[i][j]!=0);
		else
			for (int i = 0; i < 41; i++)
				bs.set(idx++,b[i/9][i%9]!=0);

		String bits = StegoDokuV3.decode(b);
		for (char c : bits.toCharArray())
			bs.set(idx++,c=='1');
		//if (bs.length() < bits.length())
		//	bs.set(i++);

		return bs;
	}

	/**
	 * use the StegoDoku-H encoder as decompressor
	 * @param bs - BitSet compressed board
	 * @return decompressed board as String
	 */
	public static String stegoDecompress(BitSet bs) {
		boolean sym = bs.get(0);
		int pl = 81;
		if (sym)
			pl = 41;

		BitSet pattern = bs.get(1, 1+pl);
		BitSet clues = bs.get(1+pl, bs.length());
		String bits = "";
		for (int i = 0; i < clues.length(); i++)
			bits += clues.get(i) ? "1" : "0";

		StegoDokuV3 sd = StegoDokuV3.encode(bits);

		int[] board = new int[81];
		if (!sym)
			for (int i = 0; i < 81; i++)
				if (pattern.get(i))
					board[i] = sd.board(i/9, i%9);
				else
					board[i] = 0;
		else
			for (int i = 0; i < 41; i++)
				if (pattern.get(i)) {
					board[i] = sd.board(i/9, i%9);
					board[80-i] = sd.board(8-(i/9), 8-(i%9));
				}
				else {
					board[i] = 0;
					board[80-i] = 0;
				}

		String b = "";
		for (int i = 0; i < 81; i++)
			b += board[i];
		return b;
	}

	/**
	 * huffman coded run length compressor<br>
	 * blocks of zeros are huffman coded with a 0-prefix<br>
	 * clues are huffman coded with a 1-prefix
	 * @param b - puzzle
	 * @return compressed puzzle
	 */
	public static BitSet hcrlCompress(int[][] b) {
		BitSet bs = new BitSet();
		int idx = 0;

		Integer[] rl = new Integer[65];
		double[] rl_prob = new double[65];
		double sum = 0;
		for (int i = 0; i <= 64; i++) {
			rl[i] = i;
			rl_prob[i] = 0.4*Math.exp(-0.4*i);
			sum += rl_prob[i];
		}
		for (int i = 0; i <= 64; i++)
			rl_prob[i] /= sum;
		HuffmanTree rlTree = new HuffmanTree(rl,rl_prob);
		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});

		int z = 0;
		for (int i = 0; i < 81; i++) {
			if (b[i/9][i%9] == 0)
				z++;
			else {
				String zstr = '0' + rlTree.encode(z);
				for (int j = 0; j < zstr.length(); j++)
					bs.set(idx++,zstr.charAt(j)=='1');
				String nstr = '1' + numTree.encode(b[i/9][i%9]);
				for (int j = 0; j < nstr.length(); j++)
					bs.set(idx++,nstr.charAt(j)=='1');
				z = 0;
			}
		}
		// if it ends with a bunch of zeros, don't both coding them

		return bs;
	}

	/**
	 * pretty good sudoku compressor<br>
	 * bit 0 = 0 for asymmetric, 1 for symmetric clue pattern
	 * bits 1..6 are the number of clues, n
	 * next block is the clue locations
	 * then the clue values
	 * @param b - puzzle
	 * @return compressed puzzle
	 * @throws Exception 
	 */
	public static BitSet pgscCompress(int[][] b) {
		BitSet bs = new BitSet();
		int idx = 0;

		// check for symmetry
		boolean s = true;
		if (!isSymmetric(b))
			s = false;
		bs.set(idx++,s);

		// count clues
		int n = 0;
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				n += (b[i][j] == 0 ? 0 : 1);
		boolean o = false;
		if (s) {
			if (n == 81) {
				o = true;
				n--;
			}
			n = (n+1)/2 - 9;
			//System.out.println("size = " + (n+9));
		} else {
			n -= 17;
			//System.out.println("size = " + (n+17));
		}

		// set size field
		String bits = Integer.toBinaryString(64+n);
		for (int i = 1; i < bits.length(); i++)
			bs.set(idx++,bits.charAt(i)=='1');
		//System.out.println("size bits = " + bits.substring(1));

		// set loc data
		int pl = 81;
		if (s) // symmetric clues
			pl = 41;
		//else asymmetric clues
		int d = 0;
		for (int i = 0; i < pl; i++) {
			if (b[i/9][i%9] == 0) {
				d++;
				continue;
			}
			//System.out.println("d = " + d);
			bits = Integer.toBinaryString(32+d);
			for (int j = 1; j < bits.length(); j++)
				bs.set(idx++,bits.charAt(j)=='1');
			d = 0;
		}

		// set clue value data
		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});
		for (int i = 0; i < 81; i++) {
			if (b[i/9][i%9] == 0)
				continue;
			String nstr = numTree.encode(b[i/9][i%9]);

			for (int j = 0; j < nstr.length(); j++)
				bs.set(idx++,nstr.charAt(j)=='1');
		}

		return bs;
	}

	public static String pgscDecompress(BitSet bs) {
		boolean sym = bs.get(0);

		String bits = "";
		//for (int i = 0; i < bs.length(); i++)
		//	bits += bs.get(i) ? "1" : "0";
		//System.out.println(bits);

		//if (s)
		//	System.out.println("sym");
		//else
		//	System.out.println("asym");

		// size
		BitSet size = bs.get(1, 7);
		int n = 0;
		for (int i = 0; i < 6; i++) {
			n *= 2;
			n += size.get(i) ? 1 : 0;
		}
		if (sym) 
			n += 9;
		else
			n += 17;
		//System.out.println("size = " + n);

		String b = "";
		int[] board = new int[81];
		for (int i = 0; i < 81; i++)
			board[i] = 0;

		// clue locations
		int idx = 7;
		int k = 0;
		for (int j = 0; j < n; j++) {
			BitSet t = bs.get(idx, idx+5);
			int n2 = 0;
			for (int i = 0; i < 5; i++) {
				n2 *= 2;
				n2 += t.get(i) ? 1 : 0;
			}

			//System.out.println("n2 = " + n2);

			k += n2;
			board[k] = -1;
			if (sym)
				board[80-k] = -1;
			k++;

			idx += 5;

			//bits = "";
			//for (int i = 0; i < t.length(); i++)
			//	bits += t.get(i) ? "1" : "0";
			//System.out.println(bits);

			//System.out.println(b1+ " " +b2);
		}
		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});
		BitSet values = bs.get(idx, bs.length());
		bits = "";
		for (int i = 0; i < values.length(); i++)
			bits += values.get(i) ? "1" : "0";
		k = 0;
		for (int i = 0; i < 81; i++) {
			if (board[i] == 0)
				continue;
			Object[] ret = numTree.decode_nice(bits);
			//System.out.println((k++) + " " + ret[0]);
			while (ret == null) {
				bits += "0";
				ret = numTree.decode_nice(bits);
			}
			board[i] = (int)ret[0];
			bits = (String)ret[1];
		}

		String nb = "";
		for (int i = 0; i < 81; i++)
			b += board[i];
		return b;
	}

	/**
	 * compress board using clue pattern<br>
	 * if symmetric clue pattern:<br>
	 * bits 1..41 encode board pattern<br>
	 * bits 42..end encode numbers as reached in top-left to bottom-right order<br>
	 * else: clue pattern takes up 81 bits.<br>
	 * 0 means empty in clue pattern<br>
	 * this is not the best that can be done.
	 * @param b - puzzle
	 * @return compressed puzzle
	 */
	public static BitSet patternCompress(int[][] b) {
		BitSet bs = new BitSet();
		int idx = 0;
		//System.out.println("idx = " + idx);

		// check for symmetry
		//int pl = 41;
		bs.set(0);
		boolean sym = isSymmetric(b);
		if (!sym) {
			//	pl = 81;
			bs.flip(0);
		}
		//System.out.println("SYM = " + sym);
		idx = 1;
		//System.out.println("idx = " + idx);

		LinkedList<Integer> list = new LinkedList<Integer>();
		for (int i = 8; i >= 0; i--)
			for (int j = 8; j >= 0; j--)
				if (b[i][j] > 0)
					list.add(9*i+j);
		int s = list.size();
		//System.out.println(s);
		if (sym)
			s = (s-17)/2;
		else
			s = s-17;
		//System.out.println(s);

		Integer[] Iarr;
		if (sym)
			Iarr = list.subList(list.size()/2, list.size()).toArray(new Integer[0]);
		else
			Iarr = list.toArray(new Integer[0]);

		for (int i = 0; i < (sym ? 3 : 4); i++) {
			bs.set(idx++,s % 2 == 1);
			s /= 2;
		}
		//System.out.println("idx = " + idx);


		int[] iarr = new int[Iarr.length];
		for (int i = 0; i < iarr.length; i++)
			iarr[i] = Iarr[i];
		//System.out.println(Combinadic.toString(iarr));
		BigInteger bi = Combinadic.toNumber(iarr);
		//System.out.println(bi);
		//System.out.println(Combinadic.toString(Combinadic.toCombination(bi, iarr.length)));
		int len = Combinadic.combin((sym ? 41 : 81), iarr.length).bitLength();
		//System.out.println(len);
		for (int i = len-1; i >= 0; i--) {
			bs.set(idx++,bi.testBit(i));
			//System.out.print(bi.testBit(i) ? "1" : "0");
		}
		//System.out.println();
		//System.out.println("idx = " + idx);

		// set clue pattern
		//for (int i = 0; i < pl; i++)
		//	bs.set(idx++,b[i/9][i%9] != 0);

		// remaining bits are the clues
		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});
		for (int i = 0; i < 81; i++) {
			if (b[i/9][i%9] == 0)
				continue;
			String nstr = numTree.encode(b[i/9][i%9]);

			for (int j = 0; j < nstr.length(); j++)
				bs.set(idx++,nstr.charAt(j)=='1');
		}
		//System.out.println("idx = " + idx);

		return bs;
	}

	public static String patternDecompress(BitSet bs) {
		boolean sym = bs.get(0);
		//int pl = 81;
		//if (sym)
		//	pl = 41;
		//System.out.println("SYM = " + sym);

		BitSet num_clues = bs.get(1,(sym ? 4 : 5));
		int nc = 0;
		for (int i = 0; i < (sym ? 3 : 4); i++)
			nc += (num_clues.get(i) ? Math.pow(2,i) : 0);
		//System.out.println(nc);
		nc += (sym ? 9 : 17);
		//System.out.println(nc);
		int len = Combinadic.combin((sym ? 41 : 81), nc).bitLength();
		//System.out.println(len);
		BitSet pattern = bs.get((sym ? 4 : 5), (sym ? 4 : 5)+len);
		BigInteger bi = BigInteger.ZERO;
		for (int i = 0; i < len; i++) {
			bi = bi.shiftLeft(1);
			if (pattern.get(i))
				bi = bi.add(BigInteger.ONE);
			//System.out.print(pattern.get(i) ? "1" : "0");
		}
		//System.out.println();
		//System.out.println(bi);
		int[] comb = Combinadic.toCombination(bi, nc);
		//System.out.println(Combinadic.toString(comb));
		BitSet clues = bs.get((sym ? 4 : 5)+len, bs.length());
		String bits = "";
		for (int i = 0; i < clues.length(); i++)
			bits += clues.get(i) ? "1" : "0";

		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});
		int[] board = new int[81];
		for (int i = 0; i < 81; i++)
			board[i] = 0;
		for (int i = comb.length-1; i >= 0; i--) {
			int j = comb[i];
			//System.out.println(j);
			Object[] ret = numTree.decode_nice(bits);
			while (ret == null) {
				bits += "0";
				ret = numTree.decode_nice(bits);
			}
			bits = (String)ret[1];
			board[j] = (int)ret[0];
		}

		if (sym) {
			// populate bottom half
			for (int i = 0; i < comb.length; i++)  {
				int j = comb[i];
				if (j == 40)
					continue;
				Object[] ret = numTree.decode_nice(bits);
				while (ret == null) {
					bits += "0";
					ret = numTree.decode_nice(bits);
				}
				bits = (String)ret[1];
				board[80-j] = (int)ret[0];
			}
		}

		String b = "";
		for (int i = 0; i < 81; i++)
			b += board[i];

		return b;
	}

	public static BitSet symAwareHuffmanCompress(int[][] b) {
		BitSet bs = new BitSet();
		int idx = 0;

		// check for symmetry
		boolean sym = true;
		if (!isSymmetric(b))
			sym = false;
		bs.set(idx++,sym);

		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});

		if (!sym) {
			for (int i = 0; i < 81; i++) {
				String nstr;
				if (b[i/9][i%9] == 0)
					nstr = "0";
				else
					nstr = "1" + numTree.encode(b[i/9][i%9]);

				for (int j = 0; j < nstr.length(); j++)
					bs.set(idx++,nstr.charAt(j)=='1');
			}
		} else {
			for (int i = 0; i < 41; i++) {
				String nstr;
				String nstr2 = "";
				if (b[i/9][i%9] == 0)
					nstr = "0";
				else {
					nstr = "1" + numTree.encode(b[i/9][i%9]);
					nstr2 = "1" + numTree.encode(b[8-(i/9)][8-(i%9)]);
				}

				for (int j = 0; j < nstr.length(); j++)
					bs.set(idx++,nstr.charAt(j)=='1');
				for (int j = 0; j < nstr2.length(); j++)
					bs.set(idx++,nstr2.charAt(j)=='1');
			}
		}

		return bs;
	}

	public static String symAwareHuffmanDecompress(BitSet bs) {
		boolean sym = bs.get(0);

		if (!sym)
			return huffmanDecompress(bs.get(1, bs.length()));

		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});
		String bits = "";
		for (int i = 1; i < bs.length(); i++)
			bits += bs.get(i) ? "1" : "0";

		int[] board = new int[81];
		for (int i = 0; i < 81; i++)
			board[i] = 0;

		for (int i = 0; i < 41 && !bits.isEmpty(); i++) {
			if (bits.charAt(0) == '0') {
				bits = bits.substring(1);
				continue;
			}
			bits = bits.substring(1);
			Object[] ret = numTree.decode_nice(bits);
			while (ret == null) {
				bits += "0";
				ret = numTree.decode_nice(bits);
			}
			board[i] = (int)ret[0];
			bits = (String)ret[1];

			bits = bits.substring(1);
			ret = numTree.decode_nice(bits);
			while (ret == null) {
				bits += "0";
				ret = numTree.decode_nice(bits);
			}
			board[80-i] = (int)ret[0];
			bits = (String)ret[1];
		}

		String b = "";
		for (int i = 0; i < 81; i++)
			b += board[i];
		return b;
	}

	/**
	 * huffman compress board<br>
	 * @param b - puzzle
	 * @return compressed board
	 */
	public static BitSet huffmanCompress(int[][] b) {
		BitSet bs = new BitSet();
		int idx = 0;

		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});

		for (int i = 0; i < 81; i++) {
			String nstr;
			if (b[i/9][i%9] == 0)
				nstr = "0";
			else
				nstr = "1" + numTree.encode(b[i/9][i%9]);

			//System.out.println("nstr = " + nstr);

			for (int j = 0; j < nstr.length(); j++)
				bs.set(idx++,nstr.charAt(j)=='1');
		}

		return bs;
	}

	public static String huffmanDecompress(BitSet bs) {
		HuffmanTree numTree = new HuffmanTree(new Integer[]{1,2,3,4,5,6,7,8,9});
		String bits = "";
		for (int i = 0; i < bs.length(); i++)
			bits += bs.get(i) ? "1" : "0";

		int[] board = new int[81];
		for (int i = 0; i < 81; i++)
			board[i] = 0;

		for (int i = 0; i < 81 && !bits.isEmpty(); i++) {
			if (bits.charAt(0) == '0') {
				bits = bits.substring(1);
				continue;
			}
			bits = bits.substring(1);
			Object[] ret = numTree.decode_nice(bits);
			while (ret == null) {
				bits += "0";
				ret = numTree.decode_nice(bits);
			}
			board[i] = (int)ret[0];
			bits = (String)ret[1];
		}

		String b = "";
		for (int i = 0; i < 81; i++)
			b += board[i];
		return b;
	}

	/**
	 * convert board to byte array (8 bits per cell, all 81 cells in a row)
	 * @param b - puzzle to not compress
	 * @return not compressed puzzle
	 */
	public static BitSet noCompress(int[][] b) {
		byte[] c = new byte[81];

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				c[9*i+j] = (byte)(b[i][j]);
			}
		}

		return BitSet.valueOf(c);
	}

	private static void printRunLengths() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			int[] f = new int[65];
			for (int i = 0 ; i < f.length; i++)
				f[i] = 0;
			while (s.hasNext()) {
				String p = s.nextLine();
				int z = 0;
				for (char c : p.toCharArray()) {
					if (c == '0') {
						z++;
					} else {
						f[z]++;
						z = 0;
					}
				}
			}
			s.close();

			int sum = 0;
			for (int i = 0; i < f.length; i++) {
				sum += f[i];
			}
			double e = 0.0;
			for (int i = 0; i < f.length; i++) {
				System.out.printf("%d\t%d\t%.4f\n",i,f[i],(double)f[i]/sum);
				if (f[i] > 0)
					e -= (double)f[i]/sum * Math.log((double)f[i]/sum) / Math.log(2);
			}
			System.out.printf("entropy = %.4f\n", e);


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void printCompressedSizes() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			//Scanner s = new Scanner(new File("test_puzzles.txt"));
			System.out.println("clues,noCompress,huffmanCompress,symAwareHuffmanCompress,patternCompress,pgscCompress,hcrlCompress,stegoCompress,ahcrlCompress");
			while (s.hasNext()) {
				String p = s.nextLine();
				// count clues
				int clues = 0;
				for (char c : p.toCharArray())
					if (c != '0')
						clues++;
				System.out.print(clues);
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = noCompress(b);
				System.out.print("," + bs.length());
				bs = huffmanCompress(b);
				System.out.print("," + bs.length());
				bs = symAwareHuffmanCompress(b);
				System.out.print("," + bs.length());
				bs = patternCompress(b);
				System.out.print(","+bs.length());
				bs = pgscCompress(b);
				System.out.print(","+bs.length());
				bs = hcrlCompress(b);
				System.out.print(","+bs.length());
				bs = stegoCompress(b);
				System.out.print(","+bs.length());
				bs = ahcrlCompress(b);
				System.out.print("," + bs.length());
				System.out.println();
			}
			s.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void printExampleCompressed() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			String p = s.nextLine();
			int[][] b = new SudokuBoard(p).board();
			BitSet bs;
			bs = noCompress(b);
			System.out.println(bs);
			bs = huffmanCompress(b);
			System.out.println(bs);
			bs = patternCompress(b);
			System.out.println(bs);
			bs = pgscCompress(b);
			System.out.println(bs);
			bs = hcrlCompress(b);
			System.out.println(bs);
			bs = ahcrlCompress(b);
			System.out.println(bs);
			s.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * rotate board right
	 * @param b
	 * @return board b rotated right
	 */
	public static int[][] rotate(int[][] b) {
		int[][] b2 = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				b2[j][8-i]=b[i][j];
		return b2;
	}

	/**
	 * flip board over vertical axis (mirror left <-> right)
	 * @param b
	 * @return
	 */
	public static int[][] flip(int[][] b) {
		int[][] b2 = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				b2[i][8-j] = b[i][j];
		return b2;
	}

	/**
	 * flip board over diagonal axis
	 * @param b
	 * @return transposed board
	 */
	public static int[][] transpose(int[][] b) {
		int[][] b2 = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				b2[j][i] = b[i][j];
		return b2;
	}

	/**
	 * swap rows (0-indexed)
	 * @param b
	 * @param r1
	 * @param r2
	 */
	public static void swapRows(int[][] b, int r1, int r2) {
		if (r1 == r2) return;
		int[] tmp = b[r1];
		b[r1] = b[r2];
		b[r2] = tmp;
	}

	/**
	 * swap columns (0-indexed)
	 * @param b
	 * @param c1
	 * @param c2
	 */
	public static void swapColumns(int[][] b, int c1, int c2) {
		if (c1 == c2) return;
		for (int i = 0; i < 9; i++) {
			int tmp = b[i][c1];
			b[i][c1] = b[i][c2];
			b[i][c2] = tmp;
		}
	}

	/**
	 * swap stacks (indexed from 1)
	 * @param b
	 * @param s1
	 * @param s2
	 */
	public static void swapStacks(int[][] b, int s1, int s2) {
		if (s1 == s2)
			return;
		for (int i = 0; i < 3; i++)
			swapColumns(b,3*(s1-1)+i,3*(s2-1)+i);
	}

	/**
	 * swap bands (indexed from 1)
	 * @param b
	 * @param b1
	 * @param b2
	 */
	public static void swapBands(int[][] b, int b1, int b2) {
		if (b1 == b2)
			return;
		for (int i = 0; i < 3; i++)
			swapRows(b,3*(b1-1)+i,3*(b2-1)+i);
	}

	/**
	 * move the box to the top left corner
	 * @param b
	 * @param bi - box number (indexed from 1)
	 */
	public static void moveBoxTo1(int[][] b, int bi) {
		switch(bi) {
		case 1: break;
		case 2: 
			swapStacks(b,1,2);
			break;
		case 3: 
			swapStacks(b,1,3);
			break;
		case 4: 
			swapBands(b,1,2);
			break;
		case 5: 
			swapStacks(b,1,2);
			swapBands(b,1,2);
			break;
		case 6: 
			swapStacks(b,1,3);
			swapBands(b,1,2);
			break;
		case 7: 
			swapBands(b,1,3);
			break;
		case 8: 
			swapStacks(b,1,2);
			swapBands(b,1,3);
			break;
		case 9: 
			swapStacks(b,1,3);
			swapBands(b,1,3);
			break;
		}
	}

	/**
	 * renumber grid using p-th permutation
	 * @param b
	 * @param p
	 */
	public static void renumber(int[][] b, int p) {
		PermutableArray<Integer> pa = new PermutableArray<Integer>(new Integer[]{1,2,3,4,5,6,7,8,9});
		Integer[] row0_new = pa.permute(Factoradic.valueOf(p));
		int[] row0_old = new int[9];
		for (int i = 0; i < 9; i++) {
			row0_old[i] = b[0][i];
			for (int j = 0; j < 81; j++)
				if (b[j/9][j%9] == row0_old[i])
					b[j/9][j%9] = (row0_new[i]+10);
		}
		for (int i = 0; i < 81; i++)
			b[i/9][i%9] -= 10;
	}

	/**
	 * find minlex board
	 * @param b_copy
	 * @return [0]: minlex'd board, [1]: log
	 */
	public static Object[] minlex(int[][] b) {
		int[][] minlexd_b = null;
		String minlexd_log = "";
		int[][] b_copy = new int[9][9];
		int[][] b2 = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++) {
				b2[i][j] = b[i][j];
				b_copy[i][j] = b[i][j];
			}
		for (int bi = 1; bi <= 9; bi++) {
			String log = "";
			for (int i = 0; i < 9; i++)
				for (int j = 0; j < 9; j++)
					b_copy[i][j] = b2[i][j];

			// move box bi to box 1
			moveBoxTo1(b_copy,bi);
			switch(bi) {
			case 1: log += "000"; break;
			case 2: log += "001"; break;
			case 3: log += "010"; break;
			case 4: log += "011"; break;
			case 5: log += "100"; break;
			case 6: log += "101"; break;
			case 7: log += "110"; break;
			case 8: log += "1110"; break;
			case 9: log += "1111"; break;
			}
			log += "|";

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after movebox");
			//			}

			// in box 1, put the one in the top left corner
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					if (b_copy[i][j] == 1) {
						// swap columns
						swapColumns(b_copy,0,j);
						// swap rows
						swapRows(b_copy,0,i);
						switch(3*i+j+1) {
						case 1: log += "000"; break;
						case 2: log += "001"; break;
						case 3: log += "010"; break;
						case 4: log += "011"; break;
						case 5: log += "100"; break;
						case 6: log += "101"; break;
						case 7: log += "110"; break;
						case 8: log += "1110"; break;
						case 9: log += "1111"; break;
						}
						log += "|";
						j = 3;
						i = 3;
					}

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after box1");
			//			}

			// swap cols 2 and 3?
			if (b_copy[0][1] > b_copy[0][2]) {
				swapColumns(b_copy,1,2);
				log += "1";
			} else {
				// do nothing
				log += "0";
			}
			log += "|";

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after sc23");
			//			}

			// swap rows 2 and 3?
			if (b_copy[1][0] > b_copy[2][0]) {
				swapRows(b_copy,1,2);
				log += "1";
			} else {
				// do nothing
				log += "0";
			}
			log += "|";

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after sr23");
			//			}

			// in boxes 2 and 3, put columns in order
			for (int i = 3; i < 9; i+=3) {
				if (b_copy[0][i] < b_copy[0][i+1]) {
					if (b_copy[0][i] < b_copy[0][i+2]) {
						if (b_copy[0][i+1] < b_copy[0][i+2]) {
							// 345
							// do nothing
							log += "000";
						} else {
							// 354
							swapColumns(b_copy,i+1,i+2);
							log += "001";
						}
					} else {
						// 534
						swapColumns(b_copy,i,i+1);
						swapColumns(b_copy,i,i+2);
						log += "10";
					}
				} else {
					if (b_copy[0][i] < b_copy[0][i+2]) {
						// 435
						swapColumns(b_copy,i,i+1);
						log += "010";
					} else {
						if (b_copy[0][i+1] < b_copy[0][i+2]) {
							// 453
							swapColumns(b_copy,i,i+2);
							swapColumns(b_copy,i,i+1);
							log += "011";
						} else {
							// 543
							swapColumns(b_copy,i,i+2);
							log += "11";
						}
					}
				}
				log += "|";
			}

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after boxes 2,3");
			//			}

			// swap stacks 2 and 3?
			if (b_copy[0][3] > b_copy[0][6]) {
				swapStacks(b_copy,2,3);
				log += "1";
			} else {
				// do nothing
				log += "0";
			}
			log += "|";

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after ss23");
			//			}

			// in boxes 4 and 7, put rows in order
			for (int i = 3; i < 9; i+=3) {
				if (b_copy[i][0] < b_copy[i+1][0]) {
					if (b_copy[i][0] < b_copy[i+2][0]) {
						if (b_copy[i+1][0] < b_copy[i+2][0]) {
							// 345
							// do nothing
							log += "000";
						} else {
							// 354
							swapRows(b_copy,i+1,i+2);
							log += "001";
						}
					} else {
						// 534
						swapRows(b_copy,i,i+1);
						swapRows(b_copy,i,i+2);
						log += "10";
					}
				} else {
					if (b_copy[i][0] < b_copy[i+2][0]) {
						// 435
						swapRows(b_copy,i,i+1);
						log += "010";
					} else {
						if (b_copy[i+1][0] < b_copy[i+2][0]) {
							// 453
							swapRows(b_copy,i,i+2);
							swapRows(b_copy,i,i+1);
							log += "011";
						} else {
							// 543
							swapRows(b_copy,i,i+2);
							log += "11";
						}
					}
				}
				log += "|";
			}

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after boxes 4,7");
			//			}

			// swap bands 2 and 3?
			if (b_copy[3][0] > b_copy[6][0]) {
				swapBands(b_copy,2,3);
				log += "1";
			} else {
				// do nothing
				log += "0";
			}
			log += "|";

			//			if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//				System.out.println("after sb23");
			//			}

			// transpose?
			if (b_copy[0][1] > b_copy[1][0]) {
				b_copy = transpose(b_copy);
				log += "1";
			} else {
				// do nothing
				log += "0";
			}

			//if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b))) {
			//	System.out.println("after transpose");
			//}

			//System.out.print("this: ");
			//SudokuBoard.printOneLineBoard(b);

			if (minlexd_b == null) {
				minlexd_b = new int[9][9];
				for (int i = 0; i < 9; i++)
					for (int j = 0; j < 9; j++)
						minlexd_b[i][j] = b_copy[i][j];
				minlexd_log = log;
				//if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(minlexd_b))) {
				//	System.out.println("here");
				//}
			} else {
				for (int i = 0; i < 9; i++)
					for (int j = 0; j < 9; j++)
						if (b_copy[i][j] == minlexd_b[i][j])
							continue;
						else if (b_copy[i][j] < minlexd_b[i][j]) {
							//System.out.println("new min because b["+i+"]["+j+"] = " + b[i][j] + " <  minlexd_b["+i+"]["+j+"] = " + minlexd_b[i][j]);
							for (int ii = 0; ii < 9; ii++)
								for (int jj = 0; jj < 9; jj++)
									minlexd_b[ii][jj] = b_copy[ii][jj];
							minlexd_log = log;
							j = 9;
							i = 9;
							//if (!SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(minlexd_b))) {
							//	System.out.println("here2");
							//}
						} else {
							// not a new min
							j = 9;
							i = 9;
						}
			}

			//System.out.print("min : ");
			//SudokuBoard.printOneLineBoard(minlexd_b);
		}

		minlexd_log = minlexd_log.replace("|", "");
		return new Object[]{minlexd_b,minlexd_log};
	}

	/**
	 * convert minlex'd board and code back to original grid
	 * @param b
	 * @param str
	 * @return original grid
	 */
	public static int[][] unminlex(int[][] b, String str) {
		int box1;
		// which box got moved to 1
		if (str.charAt(0) == '0') {
			if (str.charAt(1) == '0') {
				if (str.charAt(2) == '0') {
					// box 1: no change
					box1 = 1;
					str = str.substring(3);
				} else {
					// box 2: swap stacks 1 and 2
					box1 = 2;
					str = str.substring(3);
				}
			} else {
				if (str.charAt(2) == '0') {
					// box 3: swap stacks 1 and 3
					box1 = 3;
					str = str.substring(3);
				} else {
					// box 4: swap bands 1 and 2
					box1 = 4;
					str = str.substring(3);
				}
			}
		} else {
			if (str.charAt(1) == '0') {
				if (str.charAt(2) == '0') {
					// box 5: swap stacks 1 and 2, swap bands 1 and 2
					box1 = 5;
					str = str.substring(3);
				} else {
					// box 6: swap stacks 1 and 3, swap bands 1 and 2
					box1 = 6;
					str = str.substring(3);
				}
			} else {
				if (str.charAt(2) == '0') {
					// box 7: swap bands 1 and 3
					box1 = 7;
					str = str.substring(3);
				} else {
					if (str.charAt(3) == '0') {
						// box 8: swap stacks 1 and 2, swap bands 1 and 3
						box1 = 8;
						str = str.substring(4);
					} else {
						// box 9: swap stacks 1 and 3, swap bands 1 and 3
						box1 = 9;
						str = str.substring(4);
					}
				}
			}
		}

		// where was the 1 in box1
		int box11;
		if (str.charAt(0) == '0') {
			if (str.charAt(1) == '0') {
				if (str.charAt(2) == '0') {
					// cell 1: no change
					box11 = 1;
					str = str.substring(3);
				} else {
					// cell 2: swap columns 1 and 2
					box11 = 2;
					str = str.substring(3);
				}
			} else {
				if (str.charAt(2) == '0') {
					// cell 3: swap columns 1 and 3
					box11 = 3;
					str = str.substring(3);
				} else {
					// cell 4: swap rows 1 and 2
					box11 = 4;
					str = str.substring(3);
				}
			}
		} else {
			if (str.charAt(1) == '0') {
				if (str.charAt(2) == '0') {
					// cell 5: swap columns 1 and 2, swap rows 1 and 2
					box11 = 5;
					str = str.substring(3);
				} else {
					// cell 6: swap columns 1 and 3, swap rows 1 and 2
					box11 = 6;
					str = str.substring(3);
				}
			} else {
				if (str.charAt(2) == '0') {
					// cell 7: swap bands 1 and 3
					box11 = 7;
					str = str.substring(3);
				} else {
					if (str.charAt(3) == '0') {
						// cell 8: swap columns 1 and 2, swap rows 1 and 3
						box11 = 8;
						str = str.substring(4);
					} else {
						// cell 9: swap columns 1 and 3, swap rows 1 and 3
						box11 = 9;
						str = str.substring(4);
					}
				}
			}
		}

		// did columns 2 and 3 get swapped
		boolean sc23;
		if (str.charAt(0) == '0') {
			// no
			sc23 = false;
		} else {
			// yes
			sc23 = true;
		}
		str = str.substring(1);

		// did rows 2 and 3 get swapped
		boolean sr23;
		if (str.charAt(0) == '0') {
			// no
			sr23 = false;
		} else {
			// yes
			sr23 = true;
		}
		str = str.substring(1);

		// what was the order of the box 2 and 3 columns
		int[] b23co = new int[2];
		for (int i = 0; i < 2; i++) {
			if (str.charAt(0) == '0') {
				if (str.charAt(1) == '0') {
					if (str.charAt(2) == '0') {
						// abc: no change
						b23co[i] = 0;
						str = str.substring(3);
					} else {
						// acb: swap b and c
						b23co[i] = 1;
						str = str.substring(3);
					}
				} else {
					if (str.charAt(2) == '0') {
						// bac: swap a and b
						b23co[i] = 2;
						str = str.substring(3);
					} else {
						// bca: rotate right
						b23co[i] = 3;
						str = str.substring(3);
					}
				}
			} else {
				if (str.charAt(1) == '0') {
					// cab: rotate left
					b23co[i] = 4;
					str = str.substring(2);
				} else {
					// cba: swap a and c
					b23co[i] = 5;
					str = str.substring(2);
				}
			}
		}

		// did stacks 2 and 3 get swapped
		boolean ss23;
		if (str.charAt(0) == '0') {
			// no
			ss23 = false;
		} else {
			// yes
			ss23 = true;
		}
		str = str.substring(1);

		// what was the order of the box 4 and 7 rows
		int[] b47ro = new int[2];
		for (int i = 0; i < 2; i++) {
			if (str.charAt(0) == '0') {
				if (str.charAt(1) == '0') {
					if (str.charAt(2) == '0') {
						// abc: no change
						b47ro[i] = 0;
						str = str.substring(3);
					} else {
						// acb: swap b and c
						b47ro[i] = 1;
						str = str.substring(3);
					}
				} else {
					if (str.charAt(2) == '0') {
						// bac: swap a and b
						b47ro[i] = 2;
						str = str.substring(3);
					} else {
						// bca: rotate down
						b47ro[i] = 3;
						str = str.substring(3);
					}
				}
			} else {
				if (str.charAt(1) == '0') {
					// cab: rotate up
					b47ro[i] = 4;
					str = str.substring(2);
				} else {
					// cba: swap a and c
					b47ro[i] = 5;
					str = str.substring(2);
				}
			}
		}

		// did bands 2 and 3 get swapped
		boolean sb23;
		if (str.charAt(0) == '0') {
			// no
			sb23 = false;
		} else {
			// yes
			sb23 = true;
		}
		str = str.substring(1);

		// was the board transposed
		boolean t;
		if (str.charAt(0) == '0') {
			// no
			t = false;
		} else {
			// yes
			t = true;
		}
		str = str.substring(1);

		//		System.out.println("box1 = " + box1);
		//		System.out.println("box11 = " + box11);
		//		System.out.println("sc23 = " + sc23);
		//		System.out.println("sr23 = " + sr23);
		//		System.out.println("b2co = " + b23co[0]);
		//		System.out.println("b3co = " + b23co[1]);
		//		System.out.println("ss23 = " + ss23);
		//		System.out.println("b4ro = " + b47ro[0]);
		//		System.out.println("b7ro = " + b47ro[1]);
		//		System.out.println("sb23 = " + ss23);
		//		System.out.println("t = " + t);

		//		SudokuBoard.printOneLineBoard(b);

		if (t)
			b = transpose(b);

		//		SudokuBoard.printOneLineBoard(b);

		if (sb23)
			swapBands(b,2,3);

		//		SudokuBoard.printOneLineBoard(b);

		for (int i = 0; i < 2; i++)
			switch (b47ro[i]) {
			case 0: break;
			case 1: 
				swapRows(b,3*i+4,3*i+5);
				break;
			case 2: 
				swapRows(b,3*i+3,3*i+4);
				break;
			case 3: 
				swapRows(b,3*i+3,3*i+5);
				swapRows(b,3*i+4,3*i+5);
				break;
			case 4: 
				swapRows(b,3*i+3,3*i+4);
				swapRows(b,3*i+4,3*i+5);
				break;
			case 5: 
				swapRows(b,3*i+3,3*i+5);
				break;
			}

		//		SudokuBoard.printOneLineBoard(b);

		if (ss23)
			swapStacks(b,2,3);

		//		SudokuBoard.printOneLineBoard(b);

		for (int i = 0; i < 2; i++)
			switch (b23co[i]) {
			case 0: break;
			case 1: 
				swapColumns(b,3*i+4,3*i+5);
				break;
			case 2: 
				swapColumns(b,3*i+3,3*i+4);
				break;
			case 3: 
				swapColumns(b,3*i+3,3*i+5);
				swapColumns(b,3*i+4,3*i+5);
				break;
			case 4: 
				swapColumns(b,3*i+3,3*i+4);
				swapColumns(b,3*i+4,3*i+5);
				break;
			case 5: 
				swapColumns(b,3*i+3,3*i+5);
				break;
			}

		//		SudokuBoard.printOneLineBoard(b);

		if (sr23)
			swapRows(b,1,2);

		//		SudokuBoard.printOneLineBoard(b);

		if (sc23)
			swapColumns(b,1,2);

		//		SudokuBoard.printOneLineBoard(b);

		switch(box11) {
		case 1: break;
		case 2: 
			swapColumns(b,0,1);
			break;
		case 3: 
			swapColumns(b,0,2);
			break;
		case 4: 
			swapRows(b,0,1);
			break;
		case 5: 
			swapRows(b,0,1);
			swapColumns(b,0,1);
			break;
		case 6: 
			swapRows(b,0,1);
			swapColumns(b,0,2);
			break;
		case 7: 
			swapRows(b,0,2);
			break;
		case 8: 
			swapRows(b,0,2);
			swapColumns(b,0,1);
			break;
		case 9: 
			swapRows(b,0,2);
			swapColumns(b,0,2);
			break;
		}

		//		SudokuBoard.printOneLineBoard(b);

		switch(box1) {
		case 1: break;
		case 2: 
			swapStacks(b,1,2);
			break;
		case 3: 
			swapStacks(b,1,3);
			break;
		case 4: 
			swapBands(b,1,2);
			break;
		case 5: 
			swapBands(b,1,2);
			swapStacks(b,1,2);
			break;
		case 6: 
			swapBands(b,1,2);
			swapStacks(b,1,3);
			break;
		case 7: 
			swapBands(b,1,3);
			break;
		case 8: 
			swapBands(b,1,3);
			swapStacks(b,1,2);
			break;
		case 9: 
			swapBands(b,1,3);
			swapStacks(b,1,3);
			break;
		}

		//		SudokuBoard.printOneLineBoard(b);

		return b;
	}

	/**
	 * generate a random permutation of the board
	 * @param b board
	 * @return random permutation of the board
	 */
	public static int[][] randomBoardPermutation(int[][] b) {
		Random random = new Random(System.currentTimeMillis());
		int r;
		for (int i = 0; i < 100; i++) {
			switch(random.nextInt(6)) {
			case 0 : 
				b = transpose(b);
				break;
			case 1 : 
				r = 3*random.nextInt(3);
				swapRows(b,r+random.nextInt(3),r+random.nextInt(3));
				break;
			case 2 : 
				r = 3*random.nextInt(3);
				swapColumns(b,r+random.nextInt(3),r+random.nextInt(3));
				break;
			case 3 :
				swapStacks(b,1+random.nextInt(3),1+random.nextInt(3));
				break;
			case 4 : 
				swapBands(b,1+random.nextInt(3),1+random.nextInt(3));
				break;
			case 5 : 
				moveBoxTo1(b,random.nextInt(9)+1);
				break;
			}
		}
		return b;
	}

	/**
	 * map the board back to an essentially different board
	 * @param b
	 * @return [0]: equivalent essentially different board, [1]: geometric permutation, [2]: renumbering
	 */
	public static Object[] essentiallyDifferentGrid(int[][] b) {
		int[][] min_b = null;
		int rn = -1;
		String str = "";
		// original numbering
		int[] row0 = new int[9];
		for (int i = 0; i < 9; i++)
			row0[i] = b[0][i];
		int perm = Factoradic.permutationToFactoradic(row0).toNumber().intValue();
		// for each renumbering
		for (int i = 0; i < 9*8*7*6*5*4*3*2; i++) {
			renumber(b,i);
			
			// minlex the board
			Object[] ret = minlex(b);
			
			// keep track of which renumbering results in the minlex minlex
			if (min_b == null) {
				min_b = (int[][])ret[0];
				str = (String)ret[1];
				rn = i;
				//System.out.println("rn = " + rn);
				//SudokuBoard.printOneLineBoard(min_b);
			} else {
				for (int r = 0; r < 9; r++)
					for (int c = 0; c < 9; c++) {
						if (min_b[r][c] == ((int[][])ret[0])[r][c])
							continue;
						else if (min_b[r][c] > ((int[][])ret[0])[r][c]) {
							// update
							min_b = (int[][])ret[0];
							str = (String)ret[1];
							rn = i;
							//System.out.println("rn = " + rn);
							//SudokuBoard.printOneLineBoard(min_b);
						}
						// break out
						c = 9;
						r = 9;
					}
			}
		}

		// return board, original numbering and minlexing
		return new Object[]{min_b,str,perm};
	}

	/**
	 * find wrong minlex grid
	 * @param b
	 * @return [0]: minlex'd board, [1]: code
	 */
	public static Object[] improperMinlex(int[][] b) {
		int[][] b2 = b;
		int[][] b3 = null;
		String str = "";
		for (int f = 0; f < 2; f++) {
			for (int r = 0; r < 4; r++) {
				//SudokuBoard.print(b);
				//System.out.print("before: ");
				//SudokuBoard.printOneLineBoard(b2);
				Object[] ret = mapBoardToOrigin(b2);
				b2 = (int[][])(ret[0]);
				//System.out.print("after : ");
				//SudokuBoard.printOneLineBoard(b2);
				if (b3 == null) {
					b3 = b2;
					str = (String)(ret[1]);
					if (f == 1)
						str += "1";
					else
						str += "0";
					switch(r) {
					case 0 : str += "00"; break;
					case 1 : str += "01"; break;
					case 2 : str += "10"; break;
					case 3 : str += "11"; break;
					}
				}
				else {
					boolean l = true;
					for (int i = 0; i < 9 && l; i++)
						for (int j = 0; j < 9 && l; j++)
							if (b2[i][j] == b3[i][j])
								continue;
							else if (b2[i][j] > b3[i][j])
								l = false;
							else {
								b3=b2;
								str = (String)(ret[1]);
								l = false;
								//str += "|";
								if (f == 1)
									str += "1";
								else
									str += "0";
								switch(r) {
								case 0 : str += "00"; break;
								case 1 : str += "01"; break;
								case 2 : str += "10"; break;
								case 3 : str += "11"; break;
								}
							}
				}
				b = rotate(b);
				b2 = b;
				//System.out.print("minlex: ");
				//SudokuBoard.printOneLineBoard(b3);
			}
			b = flip(b);
			b2 = b;
		}
		return new Object[]{b3,str};
	}

	/**
	 * convert wrong minlex'd board and code back to original grid
	 * @param b
	 * @param str
	 * @return original grid
	 */
	public static int[][] unImproperMinlex(int[][] b, String str) {
		//split str
		String r0p = str.substring(0, 19);
		str = str.substring(19);
		boolean sr23 = str.charAt(0) == '1';
		str = str.substring(1);
		String r456p = (str.charAt(0) == '0' ? str.substring(0,3) : str.substring(0,2));
		str = str.substring(r456p.length());
		String r789p = (str.charAt(0) == '0' ? str.substring(0,3) : str.substring(0,2));
		str = str.substring(r789p.length());
		boolean sb23 = str.charAt(0) == '1';
		str = str.substring(1);
		boolean f = str.charAt(0) == '1';
		int r = Integer.parseInt(str.substring(1),2);

		// undo band 2,3 swap
		//System.out.println("swap band 2 and 3 = " + sb23);
		if (sb23)
			for (int i = 3; i < 6; i++) {
				int[] tmp = b[i];
				b[i] = b[i+3];
				b[i+3] = tmp;
			}
		//SudokuBoard.printOneLineBoard(b);

		// undo r456 and r678 swaps
		if (r789p.charAt(0) == '0') {
			if (r789p.charAt(1) == '0') {
				if (r789p.charAt(2) == '1') {
					// swap rows 8 and 9
					//System.out.println("swap rows 8 and 9");
					int[] tmp = b[7];
					b[7] = b[8];
					b[8] = tmp;
				} else {
					// do nothing
					//System.out.println("do nothing");
				}
			} else {
				if (r789p.charAt(2) == '0') {
					// swap rows 7 and 8
					//System.out.println("swap rows 7 and 8");
					int[] tmp = b[6];
					b[6] = b[7];
					b[7] = tmp;
				} else {
					// swap 7 with 9 with 8
					//System.out.println("rotate rows 7,8,9 down");
					int[] tmp = b[6];
					b[6] = b[8];
					b[8] = b[7];
					b[7] = tmp;
				}
			}
		} else {
			if (r789p.charAt(1) == '0') {
				// swap 8 with 9 with 7
				//System.out.println("rotate rows 7,8,9 up");
				int[] tmp = b[6];
				b[6] = b[7];
				b[7] = b[8];
				b[8] = tmp;
			} else {
				// swap 7 with 9
				//System.out.println("swap rows 7 and 9");
				int[] tmp = b[6];
				b[6] = b[8];
				b[8] = tmp;
			}
		}

		//SudokuBoard.printOneLineBoard(b);

		if (r456p.charAt(0) == '0') {
			if (r456p.charAt(1) == '0') {
				if (r456p.charAt(2) == '1') {
					// swap rows 5 and 6
					//System.out.println("swap rows 5 and 6");
					int[] tmp = b[4];
					b[4] = b[5];
					b[5] = tmp;
				} else {
					// do nothing
					//System.out.println("do nothing");
				}
			} else {
				if (r456p.charAt(2) == '0') {
					// swap rows 4 and 5
					//System.out.println("swap rows 4 and 5");
					int[] tmp = b[3];
					b[3] = b[4];
					b[4] = tmp;
				} else {
					// swap 4 with 6 with 5
					//System.out.println("rotate rows 4,5,6 down");
					int[] tmp = b[3];
					b[3] = b[5];
					b[5] = b[4];
					b[4] = tmp;
				}
			}
		} else {
			if (r456p.charAt(1) == '0') {
				// swap 5 with 6 with 4
				//System.out.println("rotate rows 4,5,6 up");
				int[] tmp = b[3];
				b[3] = b[4];
				b[4] = b[5];
				b[5] = tmp;
			} else {
				// swap 4 with 6
				//System.out.println("swap rows 4 and 6");
				int[] tmp = b[3];
				b[3] = b[5];
				b[5] = tmp;
			}
		}

		//SudokuBoard.printOneLineBoard(b);

		// undo row 2,3 swap
		//System.out.println("swap rows 2 and 3 = " + sr23);
		if (sr23) {
			int[] tmp = b[1];
			b[1] = b[2];
			b[2] = tmp;
		}

		// undo row0 permutation
		PermutableArray<Integer> pa = new PermutableArray<Integer>(new Integer[]{1,2,3,4,5,6,7,8,9});
		//System.out.println(Factoradic.value(Long.parseLong(r0p, 2)).toNumber());
		Integer[] row0 = pa.permute(Factoradic.valueOf(Long.parseLong(r0p, 2)));
		//for (int i = 0; i < 9; i++)
		//	System.out.print(row0[i]);
		//System.out.println();
		for (int i = 0; i < 9; i++) {
			//System.out.println((i+1) + " -> " + row0[i]);
			for (int j = 0; j < 81; j++)
				if (b[j/9][j%9] == i+1)
					b[j/9][j%9] = (row0[i]+10);
		}
		for (int i = 0; i < 81; i++)
			b[i/9][i%9] -= 10;

		// undo flip and rotate
		//System.out.println("rotate " + (4-r) + " times");
		//System.out.println("flip = " + f);
		for (int i = 0; i < 4-r; i++)
			b = rotate(b);
		//SudokuBoard.printOneLineBoard(b);
		if (f) 
			b = flip(b);
		//SudokuBoard.printOneLineBoard(b);

		return b;
	}

	/**
	 * transform board so that 1st row is 123456789<br>
	 * and 3rd row is less than 4th less than 5th<br>
	 * and 6th rows is less than 7th less than 8th<br>
	 * and 2nd band is less than 3rd
	 * @param b - sudoku board
	 * @return [0]: transformed sudoku board, [1]: code
	 */
	public static Object[] mapBoardToOrigin(int[][] b) {
		// make sure board is solved
		SudokuSolver ss = new SudokuSolver(b);
		if (!ss.solve()) {
			System.out.println("ERROR: not solvable");
			SudokuBoard.print(b);
			return null; // not solvable
		}

		// make first row be 123456789
		int [][] board = ss.solvedBoard();
		//SudokuBoard.printOneLineBoard(board);
		int[] row0 = new int[9];
		for (int i = 0; i < 9; i++)
			row0[i] = board[0][i];
		//for (int i = 0; i < 9; i++)
		//	System.out.print(row0[i]);
		//System.out.println();
		BigInteger bi = Factoradic.permutationToFactoradic(row0).toNumber();
		String str = "";
		for (int i = 0; i < 19; i++)
			str += (bi.testBit(18-i) ? "1" : "0");
		//str += "|";
		//System.out.println(str + " = " + bi);

		for (int i = 0; i < 9; i++) {
			//System.out.println(row0[i] + " -> " + (i+1));
			for (int j = 0; j < 81; j++)
				if (board[j/9][j%9] == row0[i])
					board[j/9][j%9] = (i+1 + 10);
		}
		for (int i = 0; i < 81; i++)
			board[i/9][i%9] -= 10;

		//SudokuBoard.printOneLineBoard(board);

		//make 2nd row be less than 3rd row
		if (board[1][0] > board[2][0]) {
			int[] tmp = board[1];
			board[1] = board[2];
			board[2] = tmp;
			str += "1";
		} else
			str += "0";
		//str += "|";
		//SudokuBoard.printOneLineBoard(board);

		// make 4th row be less than 5th and 6th
		// and 5th row be less than 6th
		// make 7th row be less than 8th and 9th
		// and 8th row be less than 9th
		for (int i = 3; i < 9; i+=3) {
			if (board[i][0] > board[i+1][0]) {
				// ?4?3? 
				// ?7?6?
				if (board[i+1][0] > board[i+2][0]) {
					// 543
					// 876
					int[] tmp = board[i];
					board[i] = board[i+2];
					board[i+2] = tmp;
					str += "11";
				} else if (board[i][0] > board[i+2][0]) {
					// 453
					// 786
					int[] tmp = board[i];
					board[i] = board[i+1];
					board[i+1] = board[i+2];
					board[i+2] = tmp;
					str += "011";
				} else {
					// 435
					// 768
					int[] tmp = board[i];
					board[i] = board[i+1];
					board[i+1] = tmp;
					str += "010";
				}
			} else {
				// ?3?4?
				// ?6?7?
				if (board[i][0] > board[i+2][0]) {
					// 534
					// 867
					int[] tmp = board[i];
					board[i] = board[i+2];
					board[i+2] = board[i+1];
					board[i+1] = tmp;
					str += "10";
				} else if (board[i+1][0] > board[i+2][0]) {
					// 354
					// 687
					int[] tmp = board[i+1];
					board[i+1] = board[i+2];
					board[i+2] = tmp;
					str += "001";
				} else {
					// 345
					// 678
					// do nothing
					str += "000";
				}
			}
			//SudokuBoard.printOneLineBoard(board);
		}
		//str += "|";

		// make 2nd band be less than 3rd band
		if (board[3][0] > board[6][0]) {
			for (int i = 3; i < 6; i++) {
				int[] tmp = board[i];
				board[i] = board[i+3];
				board[i+3] = tmp;
			}
			str += "1";
		} else {
			// do nothing
			str += "0";
		}
		//SudokuBoard.printOneLineBoard(board);

		//System.out.println(str);

		return new Object[]{board,str};
	}

	/**
	 * transform board so that 1st row is 123456789<br>
	 * and 3rd row is less than 4th less than 5th<br>
	 * and 6th rows is less than 7th less than 8th<br>
	 * and 2nd band is less than 3rd
	 * @param b - sudoku board
	 * @return transoformed sudoku board
	 */
	public static SudokuBoard mapBoardToOrigin(SudokuBoard b) {
		try {
			return new SudokuBoard((int[][])(mapBoardToOrigin(b.board())[0]));
		} catch (Exception e) {
		}
		return null;
	}

	private static void testMapBoardToOrigin() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			String p = s.nextLine();
			SudokuBoard b = new SudokuBoard(p);
			SudokuSolver ss = new SudokuSolver(b);
			ss.solveCells();
			ss.printBoard();
			SudokuBoard b0 = mapBoardToOrigin(b);

			b0.print();
			s.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void testPuzzleStegoDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine();
				//System.out.println(p);
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = puzzleStegoCompress(b);
				//System.out.println(bs.length() + " bits");
				String decompressed = puzzleStegoDecompress(bs);
				System.out.println(p);
				System.out.println(decompressed);
				if (!p.equals(decompressed)) {
					System.out.println("fail.");
					break;
				}
			}
			s.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void test17ClueUniqueness() {
		try {
			Scanner s = new Scanner(new File("17_clue_puzzles.txt"));
			PriorityQueue<String> b0Q = new PriorityQueue<String>();
			while (s.hasNext()) {
				String p = s.nextLine();
				boolean[] mask = new boolean[81];
				for (int i = 0; i < 81; i++)
					mask[i] = p.charAt(i) >= '1' && p.charAt(i) <= '9';
					SudokuBoard b = new SudokuBoard(p);
					//SudokuSolver ss = new SudokuSolver(b);
					//ss.solveCells();
					//String bStr = SudokuBoard.getOneLineBoard(ss.board());
					SudokuBoard b0 = mapBoardToOrigin(b);
					if (b0 == null)
						System.out.println("not solvable: " + p);
					String b0Str = SudokuBoard.getOneLineBoard(b0.board());
					String b0StrMasked = "";
					for (int i = 0 ; i < 81; i++)
						if (mask[i])
							b0StrMasked += b0Str.charAt(i);
						else
							b0StrMasked += "0";
					b0Q.add(b0StrMasked);
			}
			s.close();
			BufferedWriter out = new BufferedWriter(new FileWriter("17_clue_puzzles_origin_unique.txt"));
			String last = b0Q.remove();
			out.write(last);
			out.newLine();
			String cur;
			while (!b0Q.isEmpty()) {
				cur = b0Q.remove();
				if (cur.equals(last)) {
					System.out.println("duplicate detected: " + cur);
					continue;
				}
				out.write(cur);
				out.newLine();
				last = cur;
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static int countGrids() {
		int cnt = 0;
		int a = 1;
		int[][] board = new int[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++)
				board[i][j] = 0;
			board[0][i] = i+1;
		}
		SudokuSolutionCounter.setLimit(Integer.MAX_VALUE);
		for (int b = 4; b <= 9; b++) {
			board[1][0] = b;
			for (int c = b+1; c <= 9; c++) {
				board[2][0] = c;
				for (int d = a+1; d <= 9; d++) {
					if (d == b || d == c)
						continue;
					board[3][0] = d;
					for (int e = d+1; e <= 9; e++) {
						if (e == b || e == c)
							continue;
						board[4][0] = e;
						for (int f = e+1; f <= 9; f++) {
							if (f == b || f == c)
								continue;
							board[5][0] = f;
							for (int g = d+1; g <= 9; g++) {
								if (g == b || g == c || g == e || g == f)
									continue;
								board[6][0] = g;
								for (int h = g+1; h <= 9; h++) {
									if (h == b || h == c || h == e || h == f)
										continue;
									board[7][0] = h;
									int i = 45-a-b-c-d-e-f-g-h;
									if (i <= h)
										continue;
									board[8][0] = i;
									System.out.println(""+a+b+c+d+e+f+g+h+i);
									try {
										cnt += SudokuSolutionCounter.countSolutions(board);
									} catch (Exception e1) {
										e1.printStackTrace();
									}	
								}
							}
						}
					}
				}
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		//printCompressedSizes();

		//test17ClueUniqueness();

		//printRunLengths();

		//testPgscDecompress();

		//testHuffmanDecompress();

		//testPatternDecompress();

		//testAhcrlDecompress();

		//testStegoDecompress();

		// same board, different puzzle
		//testPuzzleStegoDecompress();

		System.out.println(countGrids());

		/*
		RandomSudoku rs = new RandomSudoku();
		SudokuBoard.print(rs.board());
		int[][] b0 = mapBoardToOrigin(rs.board());
		SudokuBoard.print(b0);
		 */

	}
}
