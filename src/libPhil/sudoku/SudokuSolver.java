package libPhil.sudoku;

import java.util.LinkedList;
import java.util.HashSet;

public class SudokuSolver {
	private SudokuBoard board;
	private int[][] solvedBoard;

	/**
	 * construct a new SudokuSolver object, starting with blank board
	 */
	public SudokuSolver() {
		board = new SudokuBoard();
		solvedBoard = null;
	}

	/**
	 * construct a new SudokuSolver object, starting with given board
	 * @param b - SudokuBoard to solve
	 */
	public SudokuSolver(SudokuBoard b) {
		try {
			board = new SudokuBoard(b);
		} catch (Exception e) {
		}
		solvedBoard = null;
	}
	
	public SudokuSolver(String b) {
		try {
			board = new SudokuBoard(b);
		} catch (Exception e) {
		}
		solvedBoard = null;
	}

	/**
	 * construct a new SudokuSolver object, starting with given board
	 * @param b - initial board state
	 */
	public SudokuSolver(int[][] b) {
		this();
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				board.set(i,j,b[i][j]);
			}
		}
	}


	/**
	 * use various techniques to solve cells
	 * @return true if board was updated
	 */
	public boolean solveCells() {
		return solveCells(false);
	}

	/**
	 * use various techniques to solve cells
	 * @param debug - if true, will print debug messages
	 * @return true if board was updated
	 */
	public boolean solveCells(boolean debug) {
		boolean updated = false;
		boolean f_recently_updated = true;
		// loop until no updates occur or board is solved
		while (f_recently_updated && !isSolved()) {
			f_recently_updated = false;
			boolean b = nakedSingles(debug)
					|| hiddenSingles(debug)
					|| nakedPairs(debug)
					|| nakedTriples(debug)
					|| hiddenPairs(debug)
					|| nakedQuads(debug)
					|| pointingPairs(debug)
					|| boxLineReduction(debug)
					//|| bruteForceCandidates(debug)
					;
			updated = b || updated; 
			f_recently_updated = b || f_recently_updated;
		}
		if (debug && updated)
			System.out.println(System.currentTimeMillis() + ": solved some cells");
		return updated;
	}
	
	/*
	Rules/Strategies (http://www.sudokuwiki.org/sudoku.htm)
	0: Naked Singles
	1: Hidden Singles
	2: Naked Pairs/Triplets
	3: Hidden Pairs/Triplets
	4: Naked/Hidden Quads
	5: Pointing Pairs
	6: Box/Line Reduction
	...
	n: Brute Force Candidates
	*/

	/**
	 * naked singles technique
	 * @return true if board updated
	 */
	private boolean nakedSingles() {
		return nakedSingles(false);
	}

	/**
	 * hidden singles technique
	 * @return true if board was updated
	 */
	private boolean hiddenSingles() {
		return hiddenSingles(false);
	}

	/**
	 * naked pairs technique
	 * @return true if board was updated
	 */
	private boolean nakedPairs() {
		return nakedPairs(false);
	}

	/**
	 * find cells with only one candidate
	 * @param debug - if true, will print debug messages
	 * @return true if an update was made
	 */
	private boolean nakedSingles(boolean debug) {
		// this could be done with nakedKtuples,
		// but it is simpler on its own
		if (debug)
			System.out.println("naked singles");
		boolean updated = false;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board.board(i, j) > 0) continue;
				if (board.candidates(i,j).size() == 1) {
					if (debug) 
						System.out.println("0. set ("+(char)('A'+i)+","+(j+1)+","+board.candidates(i,j).getFirst()+")");
					//boolean b = board.set(i,j,board.candidates(i,j).getFirst(),debug);
					board.set(i,j,board.candidates(i,j).getFirst());
					updated = true;
				}
			}
		}
		//if (updated)
		//	System.out.println("solved a naked single");
		return updated;
	}

	/**
	 * find pairs of cells with only 2 candidates
	 * @param debug - if true, will print debug messages
	 * @return
	 */
	private boolean nakedPairs(boolean debug) {
		if (debug)
			System.out.println("naked pairs");
		return nakedKtuples(2,debug);
		/*
		boolean updated = false;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board.board(i, j) > 0) continue;
				// check in row
				for (int j2 = j+1; j2 < 9; j2++) {
					if (board.board(i, j2) > 0) continue;
					LinkedList<Integer> c = new LinkedList<Integer>();
					c.addAll(board.candidates(i, j));
					for (int n : board.candidates(i, j2))
						if (!c.contains(n))
							c.add(n);
					if (c.size() == 2) {
						for (int j3 = 0; j3 < 9; j3++) {
							if (board.board(i, j3) > 0) continue;
							if (j3 != j && j3 != j2) {
								boolean b = board.candidates(i, j3).removeAll(c);
								updated = b || updated;
								if (updated && debug) {
									for (Integer k : c) {
										System.out.println("2(p)r. ("+(char)('A'+i)+","+(j3+1)+","+k+") is false");
									}
								}
							}
						}
					}
				}
				if (updated)
					return updated;

				// check in column
				for (int i2 = i+1; i2 < 9; i2++) {
					if (board.board(i2, j) > 0) continue;
					LinkedList<Integer> c = new LinkedList<Integer>();
					c.addAll(board.candidates(i, j));
					for (int n : board.candidates(i2, j))
						if (!c.contains(n))
							c.add(n);
					if (c.size() == 2) {
						for (int i3 = 0; i3 < 9; i3++) {
							if (board.board(i3, j) > 0) continue;
							if (i3 != i && i3 != i2) {
								boolean b = board.candidates(i3, j).removeAll(c);
								updated = b || updated;
								if (updated && debug) {
									for (Integer k : c) {
										System.out.println("2(p)c. ("+(char)('A'+i3)+","+(j+1)+","+k+") is false");
									}
								}
							}
						}
					}
				}
				if (updated)
					return updated;

				// check in box
				for (int i2 = i; i2 < 3*((i/3)+1); i2++) {
					for (int j2 = j+1; j2 < 3*((j/3)+1); j2++) {
						if (board.board(i2, j2) > 0) continue;
						LinkedList<Integer> c = new LinkedList<Integer>();
						c.addAll(board.candidates(i, j));
						for (int n : board.candidates(i2, j2))
							if (!c.contains(n))
								c.add(n);
						if (c.size() == 2) {
							for (int i3 = (i/3)*3; i3 < 3*((i/3)+1); i3++) {
								for (int j3 = (j/3)*3; j3 < 3*((j/3)+1); j3++) {
									if (board.board(i3, j3) > 0) continue;
									if (!((i3 == i && j3 == j) || (i3 == i2 && j3 == j2))) {
										boolean b = board.candidates(i3, j3).removeAll(c);
										updated = b || updated;
										if (updated && debug) {
											for (Integer k : c) {
												System.out.println("2(p)b. ("+(char)('A'+i3)+","+(j3+1)+","+k+") is false");
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//if (updated)
		//	System.out.println("solved a naked pair");
		return updated;
		*/
	}

	/**
	 * find cells which have a unique candidate
	 * @param debug - if true, will print debug messages
	 * @return true if board was updated
	 */
	private boolean hiddenSingles(boolean debug) {
		if (debug)
			System.out.println("hidden singles");
		
		boolean updated = false;
		boolean f = true;
		// loop until no updates occur

		while (f) {
			f = false;
			for (int r = 0; r < 9; r++) {
				for (int c = 0; c < 9; c++) {
					if (board.board(r, c) > 0) continue;
					for (int i = 0; i < board.candidates(r, c).size(); i++) {
						int num = board.candidates(r, c).get(i);
						boolean u = true;
						// for row
						for (int cc = 0; cc < 9; cc++) {
							if (cc == c)
								continue;
							if (board.candidates(r, cc).contains(num)) {
								u = false;
								break;
							}
						}
						if (u) {
							if (debug) {
								//board.print();
								//for (int cc2 = 0; cc2 < 9; cc2++) {
								//	for (int n : board.candidates(r, cc2))
								//		System.out.print(n + " ");
								//	System.out.println();
								//}
								System.out.println("1r. set("+(char)('A'+r)+","+(1+c)+","+num+")");
							}
							boolean b = board.set(r, c, num);//board.set(r,c,num,debug);
							updated = b || updated;
							f = b || f;
							break;
						}

						// for col
						u = true;
						for (int rr = 0; rr < 9; rr++) {
							if (rr == r)
								continue;
							if (board.candidates(rr, c).contains(new Integer(num))) {
								u = false;
								break;
							}
						}
						if (u) {
							if (debug) {
								System.out.println("1c. set("+(char)('A'+r)+","+(1+c)+","+num+")");
							}
							boolean b = board.set(r, c, num);//board.set(r,c,num,debug);
							updated = b || updated;
							f = b || f;
							break;
						}

						// for box
						u = true;
						for (int rr = (r/3)*3; rr < 3*((r/3)+1); rr++) {
							for (int cc = (c/3)*3; cc < 3*((c/3)+1); cc++) {
								if (rr == r && cc == c)
									continue;
								if (board.candidates(rr, cc).contains(new Integer(num))) {
									u = false;
									break;
								}
							}
						}
						if (u) {
							if (debug) {
								System.out.println("1b. set("+(char)('A'+r)+","+(1+c)+","+num+")");
							}
							boolean b = board.set(r, c, num);board.set(r,c,num,debug);
							updated = b || updated;
							f = b || f;
							break;
						}
					}
				}
			}
		}
		//if (updated)
		//	System.out.println("solved a hidden single");
		return updated;
	}
	
	/**
	 * look for k cells in same subunit that have k candidates total
	 * @param k - tuple size (2=pairs, 3=triples, 4=quads, 5=quints)
	 * @param debug - if true, will print debug messages
	 * @return true if found and candidates updated
	 */
	private boolean nakedKtuples(int k,boolean debug) {
		if (k < 1 || k > 4) {
			if (debug)
				System.out.println("[SudokuSolver.nakedKtuples] WARNING: ignoring k="+k+", expect k in [1,4]");
				return false;
		}
		//if (debug)
		//	System.out.println("naked "+k+"-tuples");
		
		HashSet<Integer> hs;
		boolean f_updated = false;
		final int ROW = 0;
		final int COL = 1;
		final int BOX = 2;
		int row = -1;
		int col = -1;
		// generate all k-tuples of 9
		int[] A = new int[k];
		for (int i = 0; i < k; i++) {
			A[i] = i;
		}
		while (A[0] < 10-k) {
			// do for each row,col,box
			for (int rcb = 0; rcb < 3; rcb++) {
				// for the j-th row/col/box
				rcbLoop: for (int j = 0; j < 9; j++) {
					hs = new HashSet<Integer>();
					
					// compute union of candidates over selected cells
					for (int i = 0; i < k; i++) {
						// convert to row,col,box coords
						switch (rcb) {
							case ROW:
								row = j;
								col = A[i];
								break;
							case COL:
								row = A[i];
								col = j;
								break;
							case BOX:
								row = 3*((int)(j/3))+(int)(A[i]/3);
								col = 3*((int)(j%3))+(int)(A[i]%3);
								break;
						}
						
						// if any of the selected cells are already set, we cannot use it
						if (board.isSet(row,col)) {
							// go to next row/col/box
							continue rcbLoop;
						}
						hs.addAll(board.candidates(row,col));
					}
					
					if (hs.size() == k) {
						// found naked k-tuple in j-th row/col/box
						// remove candidates from cells NOT in the selected set
						int idx = 0;
						for (int i = 0; i <= k; i++) {					
							// if i < k, then check that idx < A[i]
							while ((i >= k || idx < A[i]) && idx < 9) {
								// convert to row,col,box coords
								switch (rcb) {
									case ROW:
										row = j;
										col = idx;
										break;
									case COL:
										row = idx;
										col = j;
										break;
									case BOX:
										row = 3*((int)(j/3))+(int)(idx/3);
										col = 3*((int)(j%3))+(int)(idx%3);
									break;
								}
								// | is non-short-circuit OR
								int candidates_before = board.candidates(row,col).size();
								f_updated |= board.candidates(row,col).removeAll(hs);
								int candidates_after = board.candidates(row,col).size();
								if (debug && candidates_after < candidates_before) {
									/*
									System.out.print("[");
									for (int c : A) {
										System.out.print(" "+c);
									}
									System.out.println(" ]");
									
									switch (rcb) {
										case ROW:
											System.out.println("\tcheck rows");
											break;
										case COL:
											System.out.println("\tcheck cols");
											break;
										case BOX:
											System.out.println("\tcheck boxes");
											break;
									}
									*/
									for (int c : hs) {
										System.out.println((k==1?"0":k<=3?"2":"4")+(rcb==0?"r":rcb==1?"c":"b")+". ("+(char)('A'+row)+","+(col+1)+","+c+") is false");
									}
								}
								idx += 1;
							}
							// move past current cell
							idx += 1;
						}
					}
					
					// short circuit solving - return as soon as we make progress
					if (f_updated) {
						return true;
					}
				}
			}
			
			// generate next combination
			A[k-1] += 1;
			int j = k-1;
			while (j > 0 && A[j] >= 10-k+j) {
				j -= 1;
				A[j] += 1;
			}
			j += 1;
			for (; j < k; j++) {
				A[j] = A[j-1] + 1;
			}
		}
		return false;
	}

	/**
	 * look for 3 cells in same subunit that have same 3 candidates
	 * @param debug - if true, will print debug messages
	 * @return true if found and candidates updated
	 */
	private boolean nakedTriples(boolean debug) {
		if (debug)
			System.out.println("naked triples");
		return nakedKtuples(3,debug);
		/*
		// i don't think this code even works anyways /pcr
		if (debug)
			System.out.println("naked triples");
		boolean[][][] candidates = new boolean[9][9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 9; k++) {
					candidates[i][j][k] = board.candidates(i, j).contains(k+1);
				}
			}
		}
		boolean f = false;
		// check row
		if (debug)
			System.out.println("\tcheck rows");
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				int nc = 0;
				for (int k = 0; k < 9 && nc < 3; k++) {
					if (candidates[i][j][k])
						nc++;
				}
				if (nc == 3) {
					for (int j2 = 0; j2 < 9; j2++) {
						if (j2 == j)
							continue;
						int k = 0;
						while (k < 9 && candidates[i][j2][k] == candidates[i][j][k]) {
							k++;
						}
						if (k == 9) {
							for (int j3 = 0; j3 < 9; j3++) {
								if (j3 == j2 || j3 == j)
									continue;
								k = 0;
								while (k < 9 && candidates[i][j3][k] == candidates[i][j][k]) {
									k++;
								}
								if (k == 9) {
									for (int j4 = 0; j4 < 9; j4++) {
										if (j4 == j3 || j4 == j2 || j4 == j)
											continue;
										for (k = 0; k < 9; k++) {
											if (candidates[i][j][k]  && candidates[i][j4][k]) {
												if (debug)
													System.out.println("2(t)r. ("+(char)('A'+i)+","+(j4+1)+","+(k+1)+") is false");
												board.candidates(i, j4).remove(new Integer(k+1));
												candidates[i][j4][k] = false;
												f = true;
											}
										}
									}
									if (f) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}

		// check column
		if (debug)
			System.out.println("\tcheck columns");
		for (int j = 0; j < 9; j++) {
			for (int i = 0; i < 9; i++) {
				int nc = 0;
				for (int k = 0; k < 9 && nc < 3; k++) {
					if (candidates[i][j][k])
						nc++;
				}
				if (nc == 3) {
					for (int i2 = 0; i2 < 9; i2++) {
						if (i2 == i)
							continue;
						int k = 0;
						while (k < 9 && candidates[i2][j][k] == candidates[i][j][k]) {
							k++;
						}
						if (k == 9) {
							for (int i3 = 0; i3 < 9; i3++) {
								if (i3 == i2 || i3 == i)
									continue;
								k = 0;
								while (k < 9 && candidates[i3][j][k] == candidates[i][j][k]) {
									k++;
								}
								if (k == 9) {
									for (int i4 = 0; i4 < 9; i4++) {
										if (i4 == i3 || i4 == i2 || i4 == i)
											continue;
										for (k = 0; k < 9; k++) {
											if (candidates[i][j][k]  && candidates[i4][j][k]) {
												if (debug)
													System.out.println("2(t)c. ("+(char)('A'+i4)+","+(j+1)+","+(k+1)+") is false");
												board.candidates(i4, j).remove(new Integer(k+1));
												candidates[i][j][k] = false;
												f = true;
											}
										}
									}
									if (f) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}

		// check box
		if (debug)
			System.out.println("\tcheck boxes");
		for (int i = 0; i < 9; i+=3) {
			for (int j = 0; j < 9; j+=3) {
				if (debug)
					System.out.println("\t\tbox " + ((i/3)*3+j/3));
				for (int i2 = i; i2 < i+3; i2++) {
					for (int j2 = j; j2 < j+3; j2++) {
						int nc = 0;
						for (int k = 0; k < 9 && nc < 3; k++) {
							if (candidates[i2][j2][k])
								nc++;
						}
						if (nc == 3) {
							if (debug)
								System.out.println("\t\t\t("+(char)('A'+i2)+","+(j2+1)+") has 3 candidates");
							for (int i3 = i; i3 < i+3; i3++) {
								for (int j3 = j; j3 < j+3; j3++) {
									if (i3 == i2 && j3 == j2)
										continue;
									int k = 0;
									while (k < 9 && candidates[i3][j3][k] == candidates[i2][j2][k]) {
										k++;
									}
									if (k == 9) {
										if (debug)
											System.out.println("\t\t\t("+(char)('A'+i3)+","+(j3+1)+") has same candidates");
										for (int i4 = i; i4 < i+3; i4++) {
											for (int j4 = j; j4 < j+3; j4++) {
												if ((i4 == i2 && j4 == j2) || (i4 == i3 && j4 == j3))
													continue;
												k = 0;
												while (k < 9 && candidates[i4][j4][k] == candidates[i2][j2][k]) {
													k++;
												}
												if (k == 9) {
													if (debug)
														System.out.println("\t\t\t("+(char)('A'+i4)+","+(j4+1)+") also has same candidates");
													for (int i5 = i; i5 < i+3; i5++) {
														for (int j5 = j; j4 < j+3; j5++) {
															if ((i5 == i2 && j5 == j2) || (i5 == i3 && j5 == j3) || (i5 == i4 && j5 == j4))
																continue;
															for (k = 0; k < 0; k++) {
																if (candidates[i2][j2][k]  && candidates[i5][j5][k]) {
																	if (debug)
																		System.out.println("2(t)b. ("+(char)('A'+i5)+","+(j5+1)+","+(k+1)+") is false");
																	board.candidates(i5, j5).remove(new Integer(k+1));
																	candidates[i5][j5][k] = false;
																	f = true;
																}
															}
														}
													}
													if (f) {
														return true;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return false;
		*/
	}
	
	/**
	 * look for 4 cells in same subunit that have same 4 candidates
	 * @param debug - if true, will print debug messages
	 * @return true if found and candidates updated
	 */
	private boolean nakedQuads(boolean debug) {
		if (debug)
			System.out.println("naked quads");
		return nakedKtuples(4,debug);
	}

	/**
	 * look for a pair of candidates that appear in only two cells
	 * @param debug - if true, will print debug messages
	 * @return true if found and candidates updated
	 */
	private boolean hiddenPairs(boolean debug) {
		if (debug)
			System.out.println("hidden pairs");
		boolean[][][] candidates = new boolean[9][9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 9; k++) {
					candidates[i][j][k] = board.candidates(i, j).contains(k+1);
				}
			}
		}

		int[] n = new int[9];
		boolean f = false;
		// check rows
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++)
				n[j] = 0;
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 9; k++) {
					if (candidates[i][j][k]) {
						n[k]++;
					}
				}
			}
			for (int k = 0; k < 9; k++) {
				if (n[k] == 2) {
					for (int k2 = k+1; k2 < 9; k2++) {
						if (n[k2] == 2) {
							int c = 0;
							for (int j = 0; j < 9; j++) {
								if (candidates[i][j][k] && candidates[i][j][k2]) {
									c++;
								}
							}
							if (c == 2) {
								for (int j = 0; j < 9; j++) {
									if (candidates[i][j][k] && candidates[i][j][k2]) {
										for (int k3 = 0; k3 < 9; k3++) {
											if (k3 == k2 || k3 == k)
												continue;
											if (candidates[i][j][k3]) {
												if (debug)
													System.out.println("3r. ("+(char)('A'+i)+","+(j+1)+","+(k3+1)+") is false");
												board.candidates(i, j).remove(new Integer(k3+1));
												candidates[i][j][k3] = false;
												f = true;
											}
										}
									}
								}
								if (f)
									return true;
							}
						}
					}
				}
			}
		}

		// check columns
		for (int j = 0; j < 9; j++) {
			for (int i = 0; i < 9; i++)
				n[i] = 0;
			for (int i = 0; i < 9; i++) {
				for (int k = 0; k < 9; k++) {
					if (candidates[i][j][k]) {
						n[k]++;
					}
				}
			}
			for (int k = 0; k < 9; k++) {
				if (n[k] == 2) {
					for (int k2 = k+1; k2 < 9; k2++) {
						if (n[k2] == 2) {
							int c = 0;
							for (int i = 0; i < 9; i++) {
								if (candidates[i][j][k] && candidates[i][j][k2]) {
									c++;
								}
							}
							if (c == 2) {
								for (int i = 0; i < 9; i++) {
									if (candidates[i][j][k] && candidates[i][j][k2]) {
										for (int k3 = 0; k3 < 9; k3++) {
											if (k3 == k2 || k3 == k)
												continue;
											if (candidates[i][j][k3]) {
												if (debug)
													System.out.println("3c. ("+(char)('A'+i)+","+(j+1)+","+(k3+1)+") is false");
												board.candidates(i, j).remove(new Integer(k3+1));
												candidates[i][j][k3] = false;
												f = true;
											}
										}
									}
								}
								if (f)
									return true;
							}
						}
					}
				}
			}
		}


		// check boxes
		for (int i = 0; i < 9; i+=3) {
			for (int j = 0; j < 9; j+=3) {
				for (int k = 0; k < 9; k++)
					n[k] = 0;
				for (int i2 = i; i2 < i+3; i2++) {
					for (int j2 = j; j2 < j+3; j2++) {
						for (int k = 0; k < 9; k++) {
							if (candidates[i2][j2][k]) {
								n[k]++;
							}
						}
					}
				}
				for (int k = 0; k < 9; k++) {
					if (n[k] == 2) {
						//if (debug) {
						//	System.out.println(k + " appears twice");
						//}
						for (int k2 = k+1; k2 < 9; k2++) {
							if (n[k2] == 2) {
								//if (debug) {
								//	System.out.println(k2 + " also appears twice");
								//}
								int c = 0;
								for (int i2 = i; i2 < i+3; i2++) {
									for (int j2 = j; j2 < j+3; j2++) {
										if (candidates[i2][j2][k] && candidates[i2][j2][k2]) {
											c++;
										}
									}
								}
								if (c == 2) {
									//if (debug) {
									//	System.out.println("2 cells have both");
									//}
									for (int i2 = i; i2 < i+3; i2++) {
										for (int j2 = j; j2 < j+3; j2++) {
											if (candidates[i2][j2][k] && candidates[i2][j2][k2]) {
												for (int k3 = 0; k3 < 9; k3++) {
													if (k3 == k2 || k3 == k)
														continue;
													if (candidates[i2][j2][k3]) {
														if (debug)
															System.out.println("3b. ("+(char)('A'+i2)+","+(j2+1)+","+(k3+1)+") is false");
														board.candidates(i2, j2).remove(new Integer(k3+1));
														candidates[i2][j2][k3] = false;
														f = true;
													}
												}
											}
										}
									}
									if (f)
										return true;
								}
							}
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * a candidate which can only go in a certain row or column within a box<br>
	 * remove candidate from other cells in the row or column outside the box
	 * @param debug - if true, will print debug messages
	 * @return true if found and candidates updated
	 */
	private boolean pointingPairs(boolean debug) {
		if (debug)
			System.out.println("pointing pairs");
		boolean[][][] candidates = new boolean[9][9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 9; k++) {
					candidates[i][j][k] = board.candidates(i, j).contains(k+1);
				}
			}
		}

		boolean f= false;
		int[] n = new int[9];
		// check boxes
		for (int i = 0; i < 9; i+=3) {
			for (int j = 0; j < 9; j+=3) {
				for (int k = 0; k < 9; k++)
					n[k] = 0;
				for (int i2 = i; i2 < i+3; i2++) {
					for (int j2 = j; j2 < j+3; j2++) {
						for (int k = 0; k < 9; k++) {
							if (candidates[i2][j2][k]) {
								n[k]++;
							}
						}
					}
				}
				for (int k = 0; k < 9; k++) {
					if (n[k] == 2 || n[k] == 3) {
						int row = -1;
						int col = -1;
						for (int i2 = i; i2 < i+3; i2++) {
							for (int j2 = j; j2 < j+3; j2++) {
								if (candidates[i2][j2][k]) {
									if (row == -1) {
										row = i2;
									} else if (i2 != row) {
										row = -2;
									}
									if (col == -1) {
										col = j2;
									} else if (j2 != col) {
										col = -2;
									}
								}
							}
						}
						if (row >= 0) {
							for (int j2 = 0; j2 < 9; j2++) {
								if (j2 >= j && j2 < j+3)
									continue;
								if (candidates[row][j2][k]) {
									if (debug)
										System.out.println("5r. ("+(char)('A'+row)+","+(j2+1)+","+(k+1)+") is false");
									board.candidates(row, j2).remove(new Integer(k+1));
									candidates[row][j2][k] = false;
									f = true;
								}
							}
						} else if (col >= 0) {
							for (int i2 = 0; i2 < 9; i2++) {
								if (i2 >= i && i2 < i+3)
									continue;
								if (candidates[i2][col][k]) {
									if (debug)
										System.out.println("5c. ("+(char)('A'+i2)+","+(col+1)+","+(k+1)+") is false");
									board.candidates(i2, col).remove(new Integer(k+1));
									candidates[i2][col][k] = false;
									f = true;
								}
							}
						}
						if (f)
							return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * a candidate which can only go in a certain row or column<br>
	 * and all are in the same box<br>
	 * remove as candidate from other cells in box 
	 * @param debug - if true, will print debug messages
	 * @return true if found and candidates updated
	 */
	private boolean boxLineReduction(boolean debug) {
		if (debug)
			System.out.println("box line reduction");
		boolean[][][] candidates = new boolean[9][9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 9; k++) {
					candidates[i][j][k] = board.candidates(i, j).contains(k+1);
				}
			}
		}

		// check rows
		int[] n = new int[9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++)
				n[j] = 0;
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 9; k++) {
					if (candidates[i][j][k]) {
						n[k]++;
					}
				}
			}
			for (int k = 0; k < 9; k++) {
				if (n[k] == 2 || n[k] == 3) {
					int box = -1;
					boolean f = true;
					for (int j = 0; j < 9; j++) {
						if (candidates[i][j][k]) {
							if (box == -1) {
								box = 3*(i/3)+j/3;
							} else if (box != 3*(i/3)+j/3) {
								f = false;
								break;
							}
						}
					}
					if (f) {
						f = false;
						for (int i2 = 3*(i/3); i2 < 3*(i/3+1); i2++) {
							if (i2 == i)
								continue;
							for (int j2 = (box%3)*3; j2 < (box%3)*3+3; j2++) {
								if (candidates[i2][j2][k]) {
									if (debug)
										System.out.println("6r. ("+(char)('A'+i2)+","+(j2+1)+","+(k+1)+") is false");
									board.candidates(i2, j2).remove(new Integer(k+1));
									candidates[i2][j2][k] = false;
									f = true;
								}
							}
						}
						if (f)
							return true;
					}
				}
			}
		}

		// check columns
		for (int j = 0; j < 9; j++) {
			for (int i = 0; i < 9; i++)
				n[i] = 0;
			for (int i = 0; i < 9; i++) {
				for (int k = 0; k < 9; k++) {
					if (candidates[i][j][k]) {
						n[k]++;
					}
				}
			}
			for (int k = 0; k < 9; k++) {
				if (n[k] == 2 || n[k] == 3) {
					int box = -1;
					boolean f = true;
					for (int i = 0; i < 9; i++) {
						if (candidates[i][j][k]) {
							if (box == -1) {
								box = 3*(j/3)+i/3; // number differently for convenience
							} else if (box != 3*(j/3)+i/3) {
								f = false;
								break;
							}
						}
					}
					if (f) {
						f = false;
						for (int j2 = 3*(j/3); j2 < 3*(j/3+1); j2++) {
							if (j2 == j)
								continue;
							for (int i2 = (box%3)*3; i2 < (box%3)*3+3; i2++) {
								if (candidates[i2][j2][k]) {
									if (debug)
										System.out.println("6c. ("+(char)('A'+i2)+","+(j2+1)+","+(k+1)+") is false");
									board.candidates(i2, j2).remove(new Integer(k+1));
									candidates[i2][j2][k] = false;
									f = true;
								}
							}
						}
						if (f)
							return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * for each candidate, check if it is possible to solve the board<br>
	 * this should guarantee that the candidate list is minimal (only true candidates)
	 * @param debug - if true, will print debug messages
	 * @return true if candidates updated
	 */
	public boolean bruteForceCandidates(boolean debug) {
		long t1 = System.currentTimeMillis()/1000L;
		boolean updated = false;
		for (int r = 0; r < 9; r++)
			for (int c = 0; c < 9; c++) {
				if (board.board(r,c) > 0) continue;
				LinkedList<Integer> toRemove = new LinkedList<Integer>();
				for (int i : board.candidates(r,c)) {
					SudokuSolver ss = new SudokuSolver(board);
					ss.set(r, c, i);
					if (!ss.solve(debug)) {
						// this candidate can be eliminated
						toRemove.add(new Integer(i));
					} 
				}
				updated = updated || !toRemove.isEmpty();
				board.candidates(r,c).removeAll(toRemove);
			}
		long t2 = System.currentTimeMillis()/1000L;
		if (debug && updated) {
			System.out.print("bruteForceCandidates: " + (t2-t1) + " seconds");
		}
		return updated;
	}

	/**
	 * 
	 * @return copy of the solved board
	 */
	public int[][] solvedBoard() {
		if (solvedBoard == null)
			if (!solve()) {
				System.err.println("(Warning) SudokuSolver.solvedBoard(): board is not solvable");
				return null;
			}
		int[][] bc = new int[9][9];
		for (int r = 0; r < 9; r++)
			for (int c = 0; c < 9; c++)
				bc[r][c] = solvedBoard[r][c];
		return bc;
	}

	/**
	 * solve the board, if possible
	 * @return true if board is solvable (may not be unique)
	 */
	public boolean solve(boolean debug) {
		// solve using logic first
		solveCells(debug);
		if (isSolved()) {
			this.solvedBoard = board.board();
			return true;
		}
		for (int r = 0; r < 9; r++)
			for (int c = 0; c < 9; c++) {
				if (board.board(r,c) > 0) continue;
				for (int i : board.candidates(r,c)) {
					SudokuSolver ss = new SudokuSolver(board);
					ss.set(r, c, i, debug);
					ss.solveCells(debug);
					if (ss.solve(debug)) {
						this.solvedBoard = ss.solvedBoard();
						return true;
					}
				}
				return false;
			}
		// should never be here
		System.err.println("(Warning) SudokuSolver.solve(): managed to escape from for loops");
		return false;
	}
	
	public boolean solve() {
		return solve(false);
	}

	/**
	 * set the given cell to the given number
	 * @param row
	 * @param col
	 * @param num
	 * @return true if cell was set
	 */
	public boolean set(int row, int col, int num) {
		return board.set(row,col,num,false);
	}
	
	public boolean set(int row, int col, int num, boolean debug) {
		return board.set(row,col,num,debug);
	}

	/**
	 * 
	 * @return copy of the board
	 */
	public int[][] board() {
		return board.board();
	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @return value at specified position on board
	 */
	public int board(int row, int col) {
		return board.board(row,col);
	}

	public boolean isSolved() {
		return board.isSolved();
	}

	public void printBoard() {
		board.print();
	}

	public LinkedList<Integer> candidates(int row, int col) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		list.addAll(board.candidates(row, col));
		return list;
	}
}
