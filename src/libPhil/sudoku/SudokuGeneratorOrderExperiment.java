package libPhil.sudoku;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;

public class SudokuGeneratorOrderExperiment {
	private static SudokuSolver solver;
	private static Random random;
	private static int numBacktracks;
	
	private static enum Order {F,L,FMC,LMC,FLC,LLC,I,O,IMC,OMC,ILC,OLC,R};

//	private static final int F = 0;
//	private static final int L = 1;
//	private static final int FMC = 2;
//	private static final int LMC = 3;
//	private static final int FLC = 4;
//	private static final int LLC = 5;
//	private static final int I = 6;
//	private static final int O = 7;
//	private static final int IMC = 8;
//	private static final int OMC = 9;
//	private static final int ILC = 10;
//	private static final int OLC = 11;
//	private static final int R = 12;

	/**
	 * find the first, most constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findFMC(SudokuSolver s) {
		int cs = 10;
		int ii = -1,jj = -1;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (s.board(i, j) > 0) continue;
				if (s.candidates(i, j).size() < cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * find the last, most constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findLMC(SudokuSolver s) {
		int cs = 10;
		int ii = -1,jj = -1;
		for (int i = 8; i >= 0; i--) {
			for (int j = 8; j >= 0; j--) {
				if (s.board(i, j) > 0) continue;
				if (s.candidates(i, j).size() < cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * find the first, least constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findFLC(SudokuSolver s) {
		int cs = 0;
		int ii = -1,jj = -1;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (s.board(i, j) > 0) continue;
				if (s.candidates(i, j).size() > cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * find the last, least constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findLLC(SudokuSolver s) {
		int cs = 0;
		int ii = -1,jj = -1;
		for (int i = 8; i >= 0; i--) {
			for (int j = 8; j >= 0; j--) {
				if (s.board(i, j) > 0) continue;
				if (s.candidates(i, j).size() > cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * find the first open cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findF(SudokuSolver s) {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (s.board(i, j) > 0) continue;
				return new int[]{i,j};
			}
		}
		return new int[]{-1,-1};
	}

	/**
	 * find the last open cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findL(SudokuSolver s) {
		for (int i = 8; i >= 0; i--) {
			for (int j = 8; j >= 0; j--) {
				if (s.board(i, j) > 0) continue;
				return new int[]{i,j};
			}
		}
		return new int[]{-1,-1};
	}
	
	/**
	 * find the first middle cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findI(SudokuSolver s) {
		int i = 4;
		int j = 4;
		int c = 1;
		int nc1 = 1;
		int nc2 = 2;
		boolean d = true;
		int i1 = 0;
		int i2 = 0;
		while (0 <= i && i <= 8 && 0 <= j && j <= 8) {
			if (s.board(i, j) == 0)
				return new int[]{i,j};
			if (i1 == 0)
				if (i2 == 0) j++;
				else j--;
			else
				if (i2 == 0) i++;
				else i--;
			c--;
			if (c == 0) {
				if (d) {i1 = 1-i1; i2 = 1-i2; d = false;}
				else {i1 = 1-i1; d = true;}
				c = nc1;
				if (nc1 == nc2)
					nc2++;
				else
					nc1 = nc2;
			}
		}
		return new int[]{-1,-1};
	}
	
	
	/**
	 * find the last middle cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findO(SudokuSolver s) {
		int i = 4;
		int j = 4;
		int c = 1;
		int nc1 = 1;
		int nc2 = 2;
		boolean d = true;
		int i1 = 0;
		int i2 = 0;
		int[] ret = new int[]{-1,-1};
		while (0 <= i && i <= 8 && 0 <= j && j <= 8) {
			if (s.board(i, j) == 0)
				ret = new int[]{i,j};
			if (i1 == 0)
				if (i2 == 0) j++;
				else j--;
			else
				if (i2 == 0) i++;
				else i--;
			c--;
			if (c == 0) {
				if (d) {i1 = 1-i1; i2 = 1-i2; d = false;}
				else {i1 = 1-i1; d = true;}
				c = nc1;
				if (nc1 == nc2)
					nc2++;
				else
					nc1 = nc2;
			}
		}
		return ret;
	}

	/**
	 * find the first middle most constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findIMC(SudokuSolver s) {
		int cs = 10;
		int ii = -1,jj = -1;
		int i = 4;
		int j = 4;
		int c = 1;
		int nc1 = 1;
		int nc2 = 2;
		boolean d = true;
		int i1 = 0;
		int i2 = 0;
		while (0 <= i && i <= 8 && 0 <= j && j <= 8) {
			if (s.board(i, j) == 0) {
				if (s.candidates(i, j).size() < cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
			if (i1 == 0)
				if (i2 == 0) j++;
				else j--;
			else
				if (i2 == 0) i++;
				else i--;
			c--;
			if (c == 0) {
				if (d) {i1 = 1-i1; i2 = 1-i2; d = false;}
				else {i1 = 1-i1; d = true;}
				c = nc1;
				if (nc1 == nc2)
					nc2++;
				else
					nc1 = nc2;
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * find the last middle most constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findOMC(SudokuSolver s) {
		int cs = 10;
		int ii = -1,jj = -1;
		int i = 4;
		int j = 4;
		int c = 1;
		int nc1 = 1;
		int nc2 = 2;
		boolean d = true;
		int i1 = 0;
		int i2 = 0;
		while (0 <= i && i <= 8 && 0 <= j && j <= 8) {
			if (s.board(i, j) == 0) {
				if (s.candidates(i, j).size() <= cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
			if (i1 == 0)
				if (i2 == 0) j++;
				else j--;
			else
				if (i2 == 0) i++;
				else i--;
			c--;
			if (c == 0) {
				if (d) {i1 = 1-i1; i2 = 1-i2; d = false;}
				else {i1 = 1-i1; d = true;}
				c = nc1;
				if (nc1 == nc2)
					nc2++;
				else
					nc1 = nc2;
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * find the first middle least constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findILC(SudokuSolver s) {
		int cs = 0;
		int ii = -1,jj = -1;
		int i = 4;
		int j = 4;
		int c = 1;
		int nc1 = 1;
		int nc2 = 2;
		boolean d = true;
		int i1 = 0;
		int i2 = 0;
		while (0 <= i && i <= 8 && 0 <= j && j <= 8) {
			if (s.board(i, j) == 0) {
				if (s.candidates(i, j).size() > cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
			if (i1 == 0)
				if (i2 == 0) j++;
				else j--;
			else
				if (i2 == 0) i++;
				else i--;
			c--;
			if (c == 0) {
				if (d) {i1 = 1-i1; i2 = 1-i2; d = false;}
				else {i1 = 1-i1; d = true;}
				c = nc1;
				if (nc1 == nc2)
					nc2++;
				else
					nc1 = nc2;
			}
		}
		return new int[]{ii,jj};
	}
	/**
	 * find the last middle least constrained cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findOLC(SudokuSolver s) {
		int cs = 0;
		int ii = -1,jj = -1;
		int i = 4;
		int j = 4;
		int c = 1;
		int nc1 = 1;
		int nc2 = 2; 
		boolean d = true;
		int i1 = 0;
		int i2 = 0;
		while (0 <= i && i <= 8 && 0 <= j && j <= 8) {
			if (s.board(i, j) == 0) {
				if (s.candidates(i, j).size() >= cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
			if (i1 == 0)
				if (i2 == 0) j++;
				else j--;
			else
				if (i2 == 0) i++;
				else i--;
			c--;
			if (c == 0) {
				if (d) {i1 = 1-i1; i2 = 1-i2; d = false;}
				else {i1 = 1-i1; d = true;}
				c = nc1;
				if (nc1 == nc2)
					nc2++;
				else
					nc1 = nc2;
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * find a random open cell
	 * @param s
	 * @return {row,col} of found cell
	 */
	public static int[] findR(SudokuSolver s) {
		int ii = -1, jj = -1;
		LinkedList<Integer> c = new LinkedList<Integer>();
		for (int i = 0; i < 81; i++)
			c.add(i);
		for (int i = c.size(); i > 0; i--)
			c.add(c.remove(random.nextInt(i)));
		do {
			int n = c.removeFirst();
			ii = n/9;
			jj = n%9;
		} while (s.board(ii, jj) > 0 && !c.isEmpty());
		return new int[]{ii,jj};
	}

	/**
	 * recursively create a random Sudoku
	 * @param s - SudokuSolver object to keep track of board state
	 * @param order - determines how to find next cell
	 * @return true if a solved Sudoku is found
	 * @throws Exception 
	 */
	public static boolean f(SudokuSolver s,Order order) throws Exception {
		if (s.isSolved()) return true;

		int[] cell;
		switch (order) {
		case F : cell = findF(s); break;
		case L : cell = findL(s); break;
		case FMC : cell = findFMC(s); break;
		case FLC : cell = findFLC(s); break;
		case LMC : cell = findLMC(s); break;
		case LLC : cell = findLLC(s); break;
		case I : cell = findI(s); break;
		case O : cell = findO(s); break;
		case IMC : cell = findIMC(s); break;
		case OMC : cell = findOMC(s); break;
		case ILC : cell = findILC(s); break;
		case OLC : cell = findOLC(s); break;
		case R : cell = findR(s); break;
		default : cell = null; break;
		}
		if (cell == null || cell[0] == -1) return false;
		int ii = cell[0];
		int jj = cell[1];

		LinkedList<Integer> c = s.candidates(ii, jj);
		SudokuSolver s2;
		boolean b;
		do {
			if (c.isEmpty()) // no candidates, dead end
				return false;
			// pick a random candidate
			int num = c.remove(random.nextInt(c.size()));
			// on a copy of the board: set it and solve for consequences
			s2 = new SudokuSolver(s.board());
			s2.set(ii, jj, num);
			s2.solveCells();
			// recursively continue with copy
			b = f(s2,order);
			if (b) // found a solution, set candidate on actual board
				solver.set(ii, jj, num);
			else { // backtrack 
				numBacktracks++;
				if (numBacktracks > 100)
					throw new Exception("backtrack limit exceed, GTFO-ing.");
				//System.out.println("backtrack");
			}
		} while (!b);
		return true;
	}

	public static void main(String[] args) {
		random = new Random();
		int N = 1; // number of boards generated per trial
		int N2 = 10000; // number of trials
		boolean[] f = new boolean[Order.values().length];
		long t1,t2;

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Ordering_results.txt"));
			for (Order o : Order.values()) {
				outputStream.write("order =  " + o + "\t");
				f[o.ordinal()] = false;
			}
			outputStream.newLine();
			t1 = System.currentTimeMillis();
			for (int i = 0; i < N2; i++) {
				for (Order o : Order.values()) {
					numBacktracks = 0;
					for (int j = 0; j < N; j++) {
						solver = new SudokuSolver();
						try {
							f(solver,o);
						} catch (Exception e) {
							f[o.ordinal()] = true;
						}
					}
					outputStream.write(numBacktracks + "\t");
				}
				outputStream.newLine();
				if (i % N2/10 == 0) {
					System.out.print(i/10);
					outputStream.flush();
				}
			}
			t2 = System.currentTimeMillis();
			outputStream.close();
			for (Order o : Order.values())
				System.out.println("\norder = " + o + "\t" + (f[o.ordinal()]?"*":""));
			System.out.println("\ndone!");
			System.out.println((t2-t1) + " ms");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
