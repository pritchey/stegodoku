package libPhil.sudoku;
import java.util.LinkedList;
import java.util.Scanner;


public class SudokuSolutionCounter {
	private static int totalCount;
	private static int limit;
	//private LinkedList<String> solvedBoards;
	
	//private static int status_ij;
	
	static {
		limit = 500;
		totalCount = 0;
		//status_ij = 81;
	}
	
	public static void setLimit(int newLimit) {
		limit = newLimit;
	}
	
	public static int countSolutions(int[][] board) throws Exception {
		totalCount = 0;
		//status_ij = 81;
		int[][] b = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				b[i][j] = board[i][j];
		return countSolutions(b,0,0);
	}

	private static int countSolutions(int[][] b, int i, int j) throws Exception {
		SudokuSolver ss = new SudokuSolver(b);
		ss.solveCells();
		if (ss.isSolved()) {
			//String bstr = SudokuBoard.getOneLineBoard(ss.solvedBoard());
			//solvedBoards.add(bstr);
			//System.out.println(bstr);
			totalCount++;
			if (totalCount >= limit)
				throw new Exception("Stopped after "+limit+" solutions found");
			return 1;
		}		
		while(ss.board(i,j) > 0) {
			j++;
			if (j == 9) {
				i++;
				j = 0;
			}
		}
		LinkedList<Integer> candidates = ss.candidates(i, j);
		int count = 0;
		for (int c : candidates) {
			b[i][j] = c;
			count += countSolutions(b,i,j);
			b[i][j] = 0;
		}
		//if (9*i+j < status_ij) {
			//System.out.println("totalCount at ("+i+","+j+") = " + totalCount);
		//	status_ij = Math.max(9*i+j, status_ij - 1);
		//}
		return count;
//		for (int r = 0; r < 9; r++) {
//			for (int c = 0; c < 9; c++) {
//				if (ss.board(r, c) > 0) continue;
//				LinkedList<Integer> candidates = ss.candidates(r, c);
//				int count = 0;
//				for (int i : candidates) {
//					SudokuBoard b2 = new SudokuBoard(b.board());
//					b2.set(r, c, i);
//					count += countSolutions(b2);
//				}
//				return count;
//			}
//		}
//		return 0;
	}
	
	public static int totalCount() {
		return totalCount;
	}
	
	//public String[] solvedBoards() {
	//	return solvedBoards.toArray(new String[0]);
	//}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter board: ");
		String str = s.nextLine();
		s.close();
		int[][] board = new int[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				board[i][j] = Integer.parseInt(str.substring(9*i+j, 9*i+j+1));
			}
		}
		try{
		long t1,t2;
		t1 = System.currentTimeMillis();
		try {
			int nSol = countSolutions(board);
			t2 = System.currentTimeMillis();
			System.out.println(nSol + " solution(s) found");
			System.out.println((t2-t1) + " milliseconds");
		} catch (Exception e) {
			t2 = System.currentTimeMillis();
			System.out.println(e.getMessage());
			System.out.println((t2-t1) + " milliseconds");
		}
		} catch (Exception e) {
		}
	}
}
