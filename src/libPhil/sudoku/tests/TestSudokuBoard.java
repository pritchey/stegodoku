package libPhil.sudoku.tests;

import static org.junit.Assert.*;
import libPhil.sudoku.SudokuBoard;

import org.junit.Test;

public class TestSudokuBoard {

	@Test
	public void testIsSudoku() {
		/*
		 * invalid board:
		 * 139257468
		 * 763412895
		 * 954826713
		 * 285649137
		 * 518793624
		 * 647931582
		 * 376184259
		 * 492568371
		 * 821375946
		 */
		assertFalse(SudokuBoard.isSudoku("139257468763412895954826713285649137518793624647931582376184259492568371821375946"));
	}

}
