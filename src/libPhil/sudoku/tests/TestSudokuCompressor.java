package libPhil.sudoku.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.BitSet;
import java.util.Random;
import java.util.Scanner;

import libPhil.factoradic.Factoradic;
import libPhil.sudoku.RandomSudoku;
import libPhil.sudoku.SudokuBoard;
import libPhil.sudoku.SudokuCompressor;
import libPhil.sudoku.SudokuPuzzleMaker;
import libPhil.sudoku.SudokuSolver;

import org.junit.Test;

public class TestSudokuCompressor {

	//@Test
	public void testPuzzleStegoDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine().trim();
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = SudokuCompressor.puzzleStegoCompress(b);
				String decompressed = SudokuCompressor.puzzleStegoDecompress(bs);
				assertEquals(p,decompressed);
			}
			s.close();
		} catch (FileNotFoundException e) {
			fail("file not found");
		} catch (Exception e) {
			fail("board creation error");
		}
	}

	//@Test
	public void testStegoDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine().trim();
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = SudokuCompressor.stegoCompress(b);
				String decompressed = SudokuCompressor.stegoDecompress(bs);
				assertEquals(p,decompressed);
			}
			s.close();
		} catch (FileNotFoundException e) {
			fail("file not found");
		}
		catch (Exception e) {
			fail("board creation error");
		}
	}

	//@Test
	public void testPgscDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine().trim();
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = SudokuCompressor.pgscCompress(b);
				String decompressed = SudokuCompressor.pgscDecompress(bs);
				assertEquals(p,decompressed);
			}
			s.close();
		} catch (FileNotFoundException e) {
			fail("file not found");
		}
		catch (Exception e) {
			fail("board creation error");
		}
	}

	//@Test
	public void testHuffmanDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine().trim();
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = SudokuCompressor.huffmanCompress(b);
				String decompressed = SudokuCompressor.huffmanDecompress(bs);
				assertEquals(p,decompressed);
			}
			s.close();
		} catch (FileNotFoundException e) {
			fail("file not found");
		}
		catch (Exception e) {
			fail("board creation error");
		}
	}

	//@Test
	public void testPatternDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine().trim();
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = SudokuCompressor.patternCompress(b);
				String decompressed = SudokuCompressor.patternDecompress(bs);
				assertEquals(p,decompressed);
			}
			s.close();
		} catch (FileNotFoundException e) {
			fail("file not found");
		}
		catch (Exception e) {
			fail("board creation error");
		}
	}

	//@Test
	public void testAhcrlDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine().trim();
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = SudokuCompressor.ahcrlCompress(b);
				String decompressed = SudokuCompressor.ahcrlDecompress(bs);
				assertEquals(p,decompressed);
			}
			s.close();
		} catch (FileNotFoundException e) {
			fail("file not found");
		}
		catch (Exception e) {
			fail("board creation error");
		}
	}

	//@Test
	public void testSymAwareHuffmanDecompress() {
		try {
			Scanner s = new Scanner(new File("244_clean_puzzles.txt"));
			while (s.hasNext()) {
				String p = s.nextLine().trim();
				int[][] b = new SudokuBoard(p).board();
				BitSet bs;
				bs = SudokuCompressor.symAwareHuffmanCompress(b);
				String decompressed = SudokuCompressor.symAwareHuffmanDecompress(bs);
				assertEquals(p,decompressed);
			}
			s.close();
		} catch (FileNotFoundException e) {
			fail("file not found");
		}
		catch (Exception e) {
			fail("board creation error");
		}
	}

	//@Test
	public void testImproperMinlex() {
		RandomSudoku rs = new RandomSudoku();
		Object[] ret = SudokuCompressor.improperMinlex(rs.board());
		SudokuBoard.printOneLineBoard((int[][])ret[0]);
		//System.out.println((String)ret[1]);
	}

	//@Test
	public void testImproperUnminlex() {
		for (int i = 0; i < 10; i++) {
			RandomSudoku rs = new RandomSudoku();
			//SudokuBoard.printOneLineBoard(rs.board());
			Object[] ret = SudokuCompressor.improperMinlex(rs.board());
			//SudokuBoard.printOneLineBoard((int[][])ret[0]);
			//System.out.println((String)ret[1]);
			int[][] b_out = SudokuCompressor.unImproperMinlex((int[][])ret[0], (String)ret[1]);
			//SudokuBoard.printOneLineBoard(b_out);
			assertEquals(SudokuBoard.getOneLineBoard(rs.board()),SudokuBoard.getOneLineBoard(b_out));
		}
	}

	//@Test
	public void testTranspose() {
		for (int i = 0; i < 10; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			b = SudokuCompressor.transpose(b);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
		}
	}

	//@Test
	public void testSwapRows() {
		for (int i = 0; i < 10; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapRows(b, 0, 1);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapRows(b, 1, 2);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapRows(b, 3, 4);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapRows(b, 4, 5);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapRows(b, 6, 7);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapRows(b, 7, 8);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
		}
	}

	//@Test
	public void testSwapColumns() {
		for (int i = 0; i < 10; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapColumns(b, 0, 1);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapColumns(b, 1, 2);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapColumns(b, 3, 4);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapColumns(b, 4, 5);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapColumns(b, 6, 7);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapColumns(b, 7, 8);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
		}
	}

	//@Test
	public void testSwapStacks() {
		for (int i = 0; i < 10; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapStacks(b, 1, 2);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapStacks(b, 2, 3);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
		}
	}

	//@Test
	public void testSwapBands() {
		for (int i = 0; i < 10; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapBands(b, 1, 2);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			SudokuCompressor.swapBands(b, 2, 3);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
		}
	}

	//@Test
	public void testMoveBoxTo1() {
		for (int i = 0; i < 10; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			for (int j = 1; j <= 9; j++) {
				SudokuCompressor.moveBoxTo1(b, j);
				assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			}
		}
	}

	@Test
	public void testMinlex() {
		for (int i = 0; i < 50; i++) {
			RandomSudoku rs = new RandomSudoku();
			//SudokuBoard.printOneLineBoard(rs.board());
			Object[] ret = SudokuCompressor.minlex(rs.board());
			//SudokuBoard.printOneLineBoard((int[][])ret2[0]);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard((int[][])ret[0])));
			//SudokuCompressor.renumber((int[][])ret[0],0);
			//SudokuBoard.printOneLineBoard((int[][])ret[0]);
			//System.out.println(((String)ret[1]).length());
			//assertTrue("\n" + SudokuBoard.getOneLineBoard((int[][])ret2[0]) +"\n" + SudokuBoard.getOneLineBoard((int[][])ret[0]),
			//		SudokuBoard.getOneLineBoard((int[][])ret2[0]).compareTo(SudokuBoard.getOneLineBoard((int[][])ret[0])) <= 0);
		}

		for (int i = 0; i < 50; i++) {
			RandomSudoku rs = new RandomSudoku();
			Object[] ret1 = SudokuCompressor.minlex(rs.board());
			Object[] ret2 = SudokuCompressor.minlex(SudokuCompressor.randomBoardPermutation(rs.board()));
			assertEquals(SudokuBoard.getOneLineBoard((int[][])ret1[0]),SudokuBoard.getOneLineBoard((int[][])ret2[0]));
		}
	}

	@Test
	public void testUnminlex() {
		for (int i = 0; i < 100; i++) {
			RandomSudoku rs = new RandomSudoku();
			//SudokuBoard.printOneLineBoard(rs.board());
			Object[] ret = SudokuCompressor.minlex(rs.board());
			int[][] b_ml = (int[][])ret[0];
			String str = (String)ret[1];
			//SudokuBoard.printOneLineBoard(b_ml);
			int[][] b = SudokuCompressor.unminlex(b_ml, str);
			//SudokuBoard.printOneLineBoard(b);
			assertTrue(SudokuBoard.isSudoku(SudokuBoard.getOneLineBoard(b)));
			assertEquals(SudokuBoard.getOneLineBoard(rs.board()),SudokuBoard.getOneLineBoard(b));
		}
	}

	//@Test
	public void testMinlexTiming() {
		for (int i = 0; i < 100; i++) {
			RandomSudoku rs = new RandomSudoku();
			long t1 = System.nanoTime();
			SudokuCompressor.minlex(rs.board());
			long t2 = System.nanoTime();
			System.out.println((t2-t1)/1000000.0);
		}
	}

	@Test
	public void testRandomBoardPermutationMinlex() {
		for (int i = 0; i < 100; i++) {
			RandomSudoku rs = new RandomSudoku();
			Object[] ret = SudokuCompressor.minlex(rs.board());
			String b1 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
			ret = SudokuCompressor.minlex(SudokuCompressor.randomBoardPermutation(rs.board()));
			String b2 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
			assertEquals(b1,b2);
		}
	}

	@Test
	public void testRenumber() {
		Random random = new Random(System.currentTimeMillis());
		for (int i = 0; i < 100; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			int[] row0 = new int[9];
			for (int j = 0; j < 9; j++)
				row0[j] = b[0][j];
			int orig_perm = Factoradic.permutationToFactoradic(row0).toNumber().intValue();
			int perm = random.nextInt(9*8*7*6*5*4*3*2);
			SudokuCompressor.renumber(b,perm);
			SudokuCompressor.renumber(b,orig_perm);
			String bstr = SudokuBoard.getOneLineBoard(b);
			String b2str = SudokuBoard.getOneLineBoard(rs.board());
			assertEquals(b2str,bstr);
		}
	}
	
	@Test
	public void testRenumberedRandomPermutationMinlex() {
		Random random = new Random(System.currentTimeMillis());
		for (int i = 0; i < 100; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = SudokuCompressor.randomBoardPermutation(rs.board());
			int[] row0 = new int[9];
			for (int j = 0; j < 9; j++)
				row0[j] = b[0][j];
			int orig_perm = Factoradic.permutationToFactoradic(row0).toNumber().intValue();
			int perm = random.nextInt(9*8*7*6*5*4*3*2);
			SudokuCompressor.renumber(b,perm);
			Object[] ret = SudokuCompressor.minlex(b);
			int[][] bb = SudokuCompressor.unminlex((int[][])ret[0],(String)ret[1]);
			SudokuCompressor.renumber(bb,orig_perm);
			ret = SudokuCompressor.minlex(bb);
			String b1 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
			ret = SudokuCompressor.minlex(rs.board());
			String b2 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
			assertEquals(b1,b2);
		}
	}
	
	@Test
	public void testRenumberedMinlex() {
		Random random = new Random(System.currentTimeMillis());
		for (int i = 0; i < 100; i++) {
			RandomSudoku rs = new RandomSudoku();
			int[][] b = rs.board();
			int[] row0 = new int[9];
			for (int j = 0; j < 9; j++)
				row0[j] = b[0][j];
			int orig_perm = Factoradic.permutationToFactoradic(row0).toNumber().intValue();
			int perm = random.nextInt(9*8*7*6*5*4*3*2);
			SudokuCompressor.renumber(b,perm);
			Object[] ret = SudokuCompressor.minlex(b);
			int[][] bb = SudokuCompressor.unminlex((int[][])ret[0],(String)ret[1]);
			SudokuCompressor.renumber(bb,orig_perm);
			String b1 = SudokuBoard.getOneLineBoard(rs.board());
			String b2 = SudokuBoard.getOneLineBoard(bb);
			assertEquals(b1,b2);
		}
	}

	// this test is failing
	@Test
	public void testEssentiallyDifferentGrid() {
		String b1,b2;

		//		RandomSudoku rs = new RandomSudoku();
		//		Object[] ret = SudokuCompressor.essentiallyDifferentGrid(rs.board());
		//		SudokuBoard.printOneLineBoard((int[][])ret[0]);
		//		System.out.println((String)ret[1]);
		//		System.out.println((int)ret[2]);
		//		System.out.println();
		//		String b1 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
		//		int[][] b = SudokuCompressor.randomBoardPermutation(rs.board());
		//		SudokuCompressor.renumber(b, 2007);
		//		ret = SudokuCompressor.essentiallyDifferentGrid(b);
		//		SudokuBoard.printOneLineBoard((int[][])ret[0]);
		//		System.out.println((String)ret[1]);
		//		System.out.println((int)ret[2]);
		//		String b2 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
		//		assertEquals(b1,b2);

		//		for (int i = 0; i < 50; i++) {
		//			RandomSudoku rs = new RandomSudoku();
		//			Object[] ret = SudokuCompressor.essentiallyDifferentGrid(rs.board());
		//			b1 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
		//			assertTrue(SudokuBoard.isSudoku(b1));
		//		}

		Random random = new Random(System.currentTimeMillis());
		for (int i = 0; i < 50; i++) {
			RandomSudoku rs = new RandomSudoku();
			Object[] ret = SudokuCompressor.essentiallyDifferentGrid(rs.board());
			b1 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
			int[][] mlb1 = (int[][])ret[0];
			String str1 = (String)ret[1];
			int perm1 = (int)ret[2];
			int[][] b = SudokuCompressor.randomBoardPermutation(rs.board());
			SudokuCompressor.renumber(b, random.nextInt(9*8*7*6*5*4*3*2));
			ret = SudokuCompressor.essentiallyDifferentGrid(b);
			b2 = SudokuBoard.getOneLineBoard((int[][])ret[0]);
			int[][] mlb2 = (int[][])ret[0];
			String str2 = (String)ret[1];
			int perm2 = (int)ret[2];
			if (!b1.equals(b2)) {
				SudokuBoard.printOneLineBoard(rs.board());
				int[][] umlb1 = SudokuCompressor.unminlex(mlb1, str1);
				SudokuCompressor.renumber(umlb1, perm1);
				SudokuBoard.printOneLineBoard(umlb1);
				int[][] umlb2 = SudokuCompressor.unminlex(mlb2, str2);
				SudokuCompressor.renumber(umlb2, perm2);
				SudokuBoard.printOneLineBoard(umlb2);
			}
			assertEquals(b1,b2);
		}
	}
}
