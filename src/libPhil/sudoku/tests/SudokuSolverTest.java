package libPhil.sudoku.tests;

import static org.junit.Assert.*;
import libPhil.sudoku.SudokuSolver;

import org.junit.Test;

public class SudokuSolverTest {
	public static final int[][] easiest = new int[][]{
        new int[]{0,0,0,1,0,5,0,0,0},
            new int[]{1,4,0,0,0,0,6,7,0},
                new int[]{0,8,0,0,0,2,4,0,0},
                    new int[]{0,6,3,0,7,0,0,1,0},
                        new int[]{9,0,0,0,0,0,0,0,3},
                            new int[]{0,1,0,0,9,0,5,2,0},
                                new int[]{0,0,7,2,0,0,0,8,0},
                                    new int[]{0,2,6,0,0,0,0,3,5},
                                        new int[]{0,0,0,4,0,9,0,0,0}};
    public static final int[][] gentle = new int[][]{
        new int[]{0,0,0,0,0,4,0,2,8},
            new int[]{4,0,6,0,0,0,0,0,5},
                new int[]{1,0,0,0,3,0,6,0,0},
                    new int[]{0,0,0,3,0,1,0,0,0},
                        new int[]{0,8,7,0,0,0,1,4,0},
                            new int[]{0,0,0,7,0,9,0,0,0},
                                new int[]{0,0,2,0,1,0,0,0,3},
                                    new int[]{9,0,0,0,0,0,5,0,7},
                                        new int[]{6,7,0,4,0,0,0,0,0}};
    public static final int[][] moderate = new int[][]{
        new int[]{4,0,0,0,1,0,0,0,0},
            new int[]{0,0,0,3,0,9,0,4,0},
                new int[]{0,7,0,0,0,5,0,0,9},
                    new int[]{0,0,0,0,6,0,0,2,1},
                        new int[]{0,0,4,0,7,0,6,0,0},
                            new int[]{1,9,0,0,5,0,0,0,0},
                                new int[]{9,0,0,4,0,0,0,7,0},
                                    new int[]{0,3,0,6,0,8,0,0,0},
                                        new int[]{0,0,0,0,3,0,0,0,6}};
	
	
    @Test(timeout=5000)
	public void testEasiest() {
		SudokuSolver s = new SudokuSolver(easiest);
		s.solveCells();
		assertTrue(s.isSolved());
	}
	
	@Test(timeout=5000)
	public void testGentle() {
		SudokuSolver s = new SudokuSolver(gentle);
		s.solveCells();
		assertTrue(s.isSolved());
	}
	
	@Test(timeout=5000)
	public void testModerate() {
		SudokuSolver s = new SudokuSolver(moderate);
		s.solveCells();
		s.printBoard();
		assertTrue(s.isSolved());
	}

}
