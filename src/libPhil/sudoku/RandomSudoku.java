package libPhil.sudoku;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;

import stegodoku.StegoDokuV4;

public class RandomSudoku {
	private SudokuSolver solver;
	private Random random;
	private boolean backtracked;

	public RandomSudoku() {
		solver = new SudokuSolver();
		random = new Random(System.currentTimeMillis());
		backtracked = false;
		if (!f(solver))
			System.out.println("ERROR: could not create board");
		else // easily solvable from here
			solver.solveCells();

	}

	/**
	 * find the next cell to set
	 * @param s - SudokuSolver object, contains board state
	 * @return {row,col} of next cell to set
	 */
	private int[] nextCell(SudokuSolver s) {
		// find the last, most constrained spot
		int cs = 10;
		int ii = -1,jj = -1;
		for (int i = 8; i >= 0; i--) {
			for (int j = 8; j >= 0; j--) {
				if (s.board(i, j) > 0) continue;
				if (s.candidates(i, j).size() < cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
		}
		return new int[]{ii,jj};
	}

	/**
	 * recursively create a random Sudoku
	 * @param s - SudokuSolver object to keep track of board state
	 * @return true if a solved Sudoku is found
	 */
	public boolean f(SudokuSolver s) {
		if (s.isSolved()) return true;

		// get next cell
		int[] cell = nextCell(s);
		int ii = cell[0];
		int jj = cell[1];
		if (ii == -1) // no open spots, but not solved
			return false;

		LinkedList<Integer> c = s.candidates(ii, jj);
		SudokuSolver s2;
		boolean b;
		do {
			if (c.isEmpty()) // no candidates, dead end
				return false;
			// pick a random candidate
			int num = c.remove(random.nextInt(c.size()));
			// on a copy of the board: set it and solve for consequences
			s2 = new SudokuSolver(s.board());
			s2.set(ii, jj, num);
			s2.solveCells();
			// recursively continue with copy
			b = f(s2);
			if (b) // found a solution, set candidate on actual board
				this.solver.set(ii, jj, num);
			else // backtrack
				backtracked = true;
		} while (!b);
		//return this.solver.solve();
		return true;
	}

	/**
	 * 
	 * @return copy of board
	 */
	public int[][] board() {
		return solver.board();
	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @return board[row][col]
	 */
	public int board(int row, int col) {
		return solver.board(row, col);
	}

	/**
	 * 
	 * @return true if generation backtracked
	 */
	public boolean backtracked() {
		return backtracked;
	}
	
	public static void runTest(int N, String path) {
		RandomSudoku rs;
		long t1,t2;
		if (path.isEmpty())
			path = "Random_results.txt";
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(path));
			outputStream.write("time (ms)\tbacktracked");
			outputStream.newLine();
			for (int i = 0; i < N; i++) {
				t1 = System.currentTimeMillis();
				rs = new RandomSudoku();
				t2 = System.currentTimeMillis();
				outputStream.write((t2-t1) + "\t" + (rs.backtracked()?"1":"0"));
				outputStream.newLine();
				if (i % 1000 == 0) {
					System.out.print(i/1000);
					outputStream.flush();
				}
			}
			outputStream.close();
			System.out.println("\ndone!\nresults written to: " + path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// create one
		RandomSudoku rs;
		long t1,t2;
		t1 = System.currentTimeMillis();
		rs = new RandomSudoku();
		t2 = System.currentTimeMillis();
		SudokuBoard.print(rs.board());
		System.out.println((t2-t1) + " ms");

		// for some reason, the first one takes 100ms, the rest take 10-20ms
		// i do not know why.  memory recency?
		
	}

}
