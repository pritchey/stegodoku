package libPhil.sudoku;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class UnavoidableSetFinder {

	/**
	 * unavoidable patterns<br>
	 * all cells of a group should be same value<br>
	 * all groups should be different values<br>
	 * cells are ij, i=row, j=col, numbered from 1<br>
	 * e.g.<br>
	 * {4, { {2,{17,98}}, {2,{18,67}}, {2,{27,68}}, {2,{28,97}} } },<br>
	 * means<br>
	 * 4 groups<br>
	 * the group of 2 {17,98} should have same value<br>
	 *   b[0][6] == b[8][7]<br>
	 * the group of 2 {18,67} should have same value<br>
	 *   b[0][7] == b[5][6]<br>
	 * the group of 2 {27,68} should have same value<br>
	 *   b[1][6] == b[5][7]<br><br>
	 * the group of 2 {28,97} should have same value<br>
	 *   b[1][7] == b[8][6]<br>
	 * the 4 groups should all have different values<br>
	 * b[0][6] != b[0][7]<br>
	 * b[0][6] != b[1][6]<br>
	 * b[0][6] != b[1][7]<br>
	 * b[0][7] != b[1][6]<br>
	 * b[0][7] != b[1][7]<br>
	 * b[1][6] != b[1][7]<br>
	 */

	/**
	 * unavoidable patterns of length 4
	 */
	public static final int[][][] unavpat4 = new int[][][]{
		new int[][]{new int[]{11,42},new int[]{12,41}}
	};

	/**
	 * unavoidable patterns of length 6
	 */
	public static final int[][][] unavpat6 = new int[][][]{
		new int[][]{new int[]{11,22},new int[]{12,41},new int[]{42,71}},
		new int[][]{new int[]{11,43,72},new int[]{12,41,73}},
		new int[][]{new int[]{11,43},new int[]{12,41},new int[]{13,42}},
		new int[][]{new int[]{11,44,52},new int[]{12,41,54}}
	};

	/**
	 * unavoidable patterns of length 8
	 */
	public static final int[][][] unavpat8 = new int[][][]{
		new int[][]{new int[]{17,98},new int[]{18,67},new int[]{27,68},new int[]{28,97}},
		new int[][]{new int[]{14,28,33,67},new int[]{17,23,34,68}},
		new int[][]{new int[]{24,76},new int[]{26,44},new int[]{45,74},new int[]{46,75}},
		new int[][]{new int[]{13,28,67},new int[]{14,23},new int[]{17,24,68}},
		new int[][]{new int[]{11,26,48,53},new int[]{16,23,41,58}},
		new int[][]{new int[]{17,55,69},new int[]{18,57,65},new int[]{19,68}},
		new int[][]{new int[]{41,65},new int[]{45,76},new int[]{46,61},new int[]{66,75}},
		new int[][]{new int[]{24,39,78,85},new int[]{28,35,74,89}},
		new int[][]{new int[]{46,67,88,94},new int[]{47,64,86,98}}
	};

	/**
	 * unavoidable patterns of length 9
	 */
	public static final int[][][] unavpat9 = new int[][][]{
		new int[][]{new int[]{13,35,84},new int[]{14,46,85},new int[]{16,33,45}},
		new int[][]{new int[]{44,75,97},new int[]{45,76},new int[]{46,94},new int[]{77,96}},
		new int[][]{new int[]{17,79,84},new int[]{18,87,94},new int[]{19,74,98}}
	};

	/**
	 * unavoidable patterns of length 10
	 */
	public static final int[][][] unavpat10 = new int[][][]{
		new int[][]{new int[]{38,75,99},new int[]{39,58},new int[]{59,78,92},new int[]{72,95}},
		new int[][]{new int[]{43,59,81},new int[]{49,62,83},new int[]{51,69},new int[]{61,82}},
		new int[][]{new int[]{14,28,47},new int[]{17,34},new int[]{24,37,98},new int[]{48,97}},
		new int[][]{new int[]{38,59,71,87},new int[]{39,57,78,84},new int[]{74,81}},
		new int[][]{new int[]{51,69},new int[]{54,61},new int[]{55,84},new int[]{59,65},new int[]{64,85}},
		new int[][]{new int[]{71,99},new int[]{72,95},new int[]{75,98},new int[]{78,91},new int[]{79,92}},
		new int[][]{new int[]{31,53,82},new int[]{32,51,93},new int[]{83,97},new int[]{87,92}},
		new int[][]{new int[]{15,86},new int[]{16,55},new int[]{34,85},new int[]{35,54},new int[]{56,84}},
		new int[][]{new int[]{11,35},new int[]{15,37,78,93},new int[]{18,31,73,97}},
		new int[][]{new int[]{42,67,78},new int[]{48,62,76,89},new int[]{69,77,86}},
		new int[][]{new int[]{24,66,75},new int[]{26,44},new int[]{35,46,74},new int[]{36,65}},
		new int[][]{new int[]{12,28,89},new int[]{14,22,76},new int[]{19,26,74,88}},
		new int[][]{new int[]{28,42,69},new int[]{29,78,86},new int[]{48,62,76,89}},
		new int[][]{new int[]{11,29,48},new int[]{18,31,77},new int[]{21,37,49,78}},
		new int[][]{new int[]{24,79,96},new int[]{26,44},new int[]{34,46},new int[]{36,74,99}},
		new int[][]{new int[]{13,24,39,72,87},new int[]{17,23,34,79,82}},
		new int[][]{new int[]{12,46,57,63,85},new int[]{13,47,52,65,86}},
		new int[][]{new int[]{45,86},new int[]{46,95},new int[]{64,96},new int[]{65,94},new int[]{66,85}},
		new int[][]{new int[]{27,68},new int[]{28,57},new int[]{58,97},new int[]{67,99},new int[]{69,98}},
		new int[][]{new int[]{14,45,51,69,96},new int[]{16,49,54,61,95}},
		new int[][]{new int[]{42,65},new int[]{43,58,64},new int[]{44,62},new int[]{45,53,68}},
		new int[][]{new int[]{14,29},new int[]{15,42,64},new int[]{19,25},new int[]{24,45,62}},
		new int[][]{new int[]{12,35},new int[]{13,62},new int[]{15,23,38},new int[]{28,32,63}},
		new int[][]{new int[]{25,38},new int[]{27,49},new int[]{28,47},new int[]{29,35},new int[]{39,48}},
		new int[][]{new int[]{13,25,42,64},new int[]{15,22,44,53},new int[]{54,63}},
		new int[][]{new int[]{24,38,85,93},new int[]{28,35,74,83},new int[]{73,94}},
		new int[][]{new int[]{15,76,91},new int[]{16,81,95},new int[]{71,88},new int[]{78,86}},
		new int[][]{new int[]{24,57,65},new int[]{25,38},new int[]{28,34},new int[]{35,54,67}},
		new int[][]{new int[]{24,65,72,96},new int[]{25,66,84,92},new int[]{74,82}},
		new int[][]{new int[]{14,85},new int[]{15,39,76,88},new int[]{19,36,78,84}},
		new int[][]{new int[]{11,62,73},new int[]{13,91},new int[]{61,72,96},new int[]{76,93}},
		new int[][]{new int[]{38,56,69,77,84},new int[]{39,57,66,74,88}},
		new int[][]{new int[]{15,37},new int[]{17,45,58},new int[]{18,35},new int[]{38,47,55}},
		new int[][]{new int[]{25,52,66},new int[]{26,55,67},new int[]{54,62},new int[]{57,64}},
		new int[][]{new int[]{12,26,74,83},new int[]{13,72,94},new int[]{24,86,93}},
		new int[][]{new int[]{25,42,86,93},new int[]{26,43,72,95},new int[]{76,82}},
		new int[][]{new int[]{15,29,47,76,88},new int[]{17,26,49,78,85}},
		new int[][]{new int[]{37,43,54,99},new int[]{39,47,53},new int[]{44,59,97}},
		new int[][]{new int[]{31,42},new int[]{32,48,63},new int[]{33,71},new int[]{41,68,73}},
		new int[][]{new int[]{51,64,73},new int[]{52,71,86},new int[]{54,63,76,82}},
		new int[][]{new int[]{24,55,62},new int[]{25,56},new int[]{26,44,52},new int[]{42,64}},
		new int[][]{new int[]{17,49,61},new int[]{19,97},new int[]{41,68,99},new int[]{67,98}},
		new int[][]{new int[]{12,26,37,75,83},new int[]{17,23,35,72,86}},
		new int[][]{new int[]{13,38,47},new int[]{17,22,83},new int[]{27,33,48,82}},
		new int[][]{new int[]{15,39,76,97},new int[]{17,35,79},new int[]{36,77,95}},
		new int[][]{new int[]{14,46},new int[]{15,44},new int[]{16,71,95},new int[]{45,76,91}},
		new int[][]{new int[]{25,37,58,66},new int[]{28,36,45,67},new int[]{48,55}}
	};

	/**
	 * unavoidable patterns of length 11
	 */
	public static final int[][][] unavpat11 = new int[][][]{
		new int[][]{new int[]{34,58,66},new int[]{36,54,75},new int[]{53,68},new int[]{55,63,76}},
		new int[][]{new int[]{34,55,96},new int[]{35,59,64},new int[]{46,69,94},new int[]{49,56}},
		new int[][]{new int[]{21,83},new int[]{23,61},new int[]{43,59,81},new int[]{49,63},new int[]{51,69}},
		new int[][]{new int[]{16,28,99},new int[]{19,36},new int[]{26,37},new int[]{27,98},new int[]{39,97}},
		new int[][]{new int[]{11,34,95},new int[]{13,24,37},new int[]{15,23,94},new int[]{17,31}},
		new int[][]{new int[]{14,65,86},new int[]{16,74},new int[]{64,75},new int[]{76,88},new int[]{78,85}},
		new int[][]{new int[]{16,37,65,98},new int[]{18,26,97},new int[]{25,66},new int[]{27,35}},
		new int[][]{new int[]{72,94},new int[]{73,85},new int[]{74,83,98},new int[]{75,92},new int[]{88,95}},
		new int[][]{new int[]{15,37},new int[]{17,79},new int[]{19,35,68},new int[]{38,67},new int[]{69,77}},
		new int[][]{new int[]{21,35,53,66},new int[]{26,32,45,61},new int[]{33,42,55}},
		new int[][]{new int[]{16,37},new int[]{17,49,52},new int[]{19,36,48},new int[]{38,42,57}},
		new int[][]{new int[]{24,55,67},new int[]{25,46,51},new int[]{26,47,64},new int[]{41,57}},
		new int[][]{new int[]{13,34},new int[]{14,65,96},new int[]{16,23,95},new int[]{25,33,64}},
		new int[][]{new int[]{65,86},new int[]{66,72,95},new int[]{76,89},new int[]{79,92},new int[]{85,99}},
		new int[][]{new int[]{34,76,89,92},new int[]{36,64,72,95},new int[]{65,84,99}},
		new int[][]{new int[]{37,78,95},new int[]{38,89},new int[]{39,87},new int[]{75,88},new int[]{85,97}},
		new int[][]{new int[]{16,39},new int[]{19,26,67,88},new int[]{28,36},new int[]{38,69,87}},
		new int[][]{new int[]{12,73},new int[]{13,28},new int[]{18,21,72,87},new int[]{23,77,81}},
		new int[][]{new int[]{14,46,75},new int[]{15,64},new int[]{44,76,97},new int[]{65,77,94}},
		new int[][]{new int[]{28,49,65,76},new int[]{29,53,68},new int[]{46,59,63,75}},
		new int[][]{new int[]{14,81,95},new int[]{15,67,78,84},new int[]{68,75,87,91}},
		new int[][]{new int[]{42,54,68},new int[]{46,52},new int[]{48,66},new int[]{56,67},new int[]{57,64}},
		new int[][]{new int[]{11,29,63},new int[]{12,81},new int[]{19,32},new int[]{23,39,61,82}},
		new int[][]{new int[]{42,64},new int[]{44,86,98},new int[]{46,52},new int[]{56,62,88,94}},
		new int[][]{new int[]{21,46,62,83},new int[]{22,53,67,81},new int[]{43,57,66}},
		new int[][]{new int[]{14,41,75,92},new int[]{15,43,71,84},new int[]{42,83,94}},
		new int[][]{new int[]{24,55,96},new int[]{25,73,94},new int[]{56,85,93},new int[]{75,83}},
		new int[][]{new int[]{27,79,94},new int[]{29,33,71,87},new int[]{31,74,83,97}},
		new int[][]{new int[]{54,96},new int[]{55,74},new int[]{56,68,75,97},new int[]{67,78,94}},
		new int[][]{new int[]{11,87,93},new int[]{13,27},new int[]{17,88},new int[]{18,23,81,97}},
		new int[][]{new int[]{11,42,69},new int[]{12,49,53,91},new int[]{59,63},new int[]{61,93}},
		new int[][]{new int[]{46,68,89,94},new int[]{48,56,62},new int[]{52,64,86,99}},
		new int[][]{new int[]{12,39,88,93},new int[]{18,22,83,99},new int[]{29,33,82}},
		new int[][]{new int[]{12,39,88,93},new int[]{15,33,82,98},new int[]{18,35,99}},
		new int[][]{new int[]{12,26,44,91},new int[]{14,31,92},new int[]{21,36},new int[]{34,46}},
		new int[][]{new int[]{55,68,76,97},new int[]{58,64,87,95},new int[]{66,77,84}},
		new int[][]{new int[]{41,68,93},new int[]{48,51},new int[]{53,91},new int[]{55,63},new int[]{58,65}},
		new int[][]{new int[]{12,28},new int[]{13,42,68},new int[]{18,21,63},new int[]{22,48,61}},
		new int[][]{new int[]{16,39},new int[]{19,24,56,67},new int[]{27,36,54},new int[]{37,69}},
		new int[][]{new int[]{42,67,76,99},new int[]{44,57,62},new int[]{49,54,77,96}},
		new int[][]{new int[]{11,63},new int[]{13,26,44},new int[]{14,31,46},new int[]{23,36,61}},
		new int[][]{new int[]{34,75,99},new int[]{35,54,69,98},new int[]{58,65,79,94}},
		new int[][]{new int[]{44,75,92},new int[]{45,58,89,94},new int[]{59,72,85,98}},
		new int[][]{new int[]{16,39,74,87},new int[]{17,34,79,85},new int[]{19,35,86}}
	};

	/**
	 * unavoidable patterns of length 12
	 */
	public static final int[][][] unavpat12 = new int[][][]{
		new int[][]{new int[]{21,43,56},new int[]{23,51,82},new int[]{32,83},new int[]{33,42},new int[]{46,52}},
		new int[][]{new int[]{24,66},new int[]{26,55,78,84},new int[]{56,75,98},new int[]{64,88,96}},
		new int[][]{new int[]{13,27,69,88,91},new int[]{18,31,67,83,99},new int[]{21,37}},
		new int[][]{new int[]{11,43,92},new int[]{13,62,91},new int[]{42,68},new int[]{46,63},new int[]{48,66}},
		new int[][]{new int[]{21,82},new int[]{22,51},new int[]{52,64},new int[]{54,68},new int[]{58,61},new int[]{62,81}},
		new int[][]{new int[]{21,82},new int[]{22,51},new int[]{31,72},new int[]{32,61},new int[]{52,71},new int[]{62,81}},
		new int[][]{new int[]{35,47,66},new int[]{36,45,84},new int[]{41,67},new int[]{44,61},new int[]{64,86}},
		new int[][]{new int[]{51,83},new int[]{52,64},new int[]{53,82},new int[]{54,68},new int[]{58,61},new int[]{62,81}},
		new int[][]{new int[]{45,51,67},new int[]{47,55,62},new int[]{52,64},new int[]{54,68},new int[]{58,61}},
		new int[][]{new int[]{17,69,98},new int[]{19,58,97},new int[]{24,56,68},new int[]{26,59,64}},
		new int[][]{new int[]{18,69},new int[]{19,98},new int[]{24,56,68},new int[]{26,59,64},new int[]{58,99}},
		new int[][]{new int[]{15,39,84},new int[]{19,25,87},new int[]{24,85},new int[]{27,34},new int[]{37,89}},
		new int[][]{new int[]{22,49,53},new int[]{23,82},new int[]{32,83},new int[]{33,42,54},new int[]{44,59}},
		new int[][]{new int[]{12,28,35,59},new int[]{15,22,37},new int[]{27,98},new int[]{39,58,97}},
		new int[][]{new int[]{14,86},new int[]{15,74},new int[]{16,95},new int[]{44,75},new int[]{45,96},new int[]{46,84}},
		new int[][]{new int[]{11,49,72,88},new int[]{12,47,79,93},new int[]{13,48,81,97}},
		new int[][]{new int[]{21,36,43,55},new int[]{26,38},new int[]{28,33,47,51},new int[]{45,57}},
		new int[][]{new int[]{11,25,39,56,67},new int[]{16,29,31,52,65},new int[]{57,62}},
		new int[][]{new int[]{12,53},new int[]{13,42,89,91},new int[]{31,52,83,99},new int[]{32,41}},
		new int[][]{new int[]{12,35,64},new int[]{14,23,82},new int[]{24,65},new int[]{25,33},new int[]{32,83}},
		new int[][]{new int[]{12,35,83},new int[]{14,23,82},new int[]{15,64},new int[]{24,65},new int[]{25,33}},
		new int[][]{new int[]{25,39,58},new int[]{28,35,77},new int[]{37,59,65},new int[]{55,67,78}},
		new int[][]{new int[]{21,45,52,83},new int[]{23,42,71},new int[]{43,55,81},new int[]{51,72}},
		new int[][]{new int[]{41,64},new int[]{43,58},new int[]{44,53,67},new int[]{48,55,61},new int[]{57,65}},
		new int[][]{new int[]{21,67,72,89},new int[]{22,41},new int[]{42,68,77,81},new int[]{69,88}},
		new int[][]{new int[]{28,35,59,66,84,97},new int[]{29,36,54,67,88,95}},
		new int[][]{new int[]{11,48,63,87},new int[]{13,49,88,91},new int[]{41,67,83,99}},
		new int[][]{new int[]{11,34,76},new int[]{16,28,45,74},new int[]{18,25,31},new int[]{35,44}},
		new int[][]{new int[]{22,63},new int[]{23,82},new int[]{31,62},new int[]{32,43},new int[]{33,61},new int[]{42,83}},
		new int[][]{new int[]{21,53,65,84,92},new int[]{23,54,62,86,91},new int[]{66,85}},
		new int[][]{new int[]{27,36,59,65},new int[]{28,35,54,67},new int[]{29,58},new int[]{34,56}},
		new int[][]{new int[]{11,67,89,93},new int[]{13,48,61,97},new int[]{43,69,81,98}},
		new int[][]{new int[]{15,27,58,86,99},new int[]{16,65},new int[]{28,59,66,87,95}},
		new int[][]{new int[]{13,36,45,62},new int[]{15,32,73,96},new int[]{43,66,75,92}},
		new int[][]{new int[]{14,65,96},new int[]{15,44,68},new int[]{45,94},new int[]{46,95},new int[]{48,66}},
		new int[][]{new int[]{34,76},new int[]{36,45,84},new int[]{43,65},new int[]{46,63,74},new int[]{64,85}},
		new int[][]{new int[]{14,55},new int[]{15,66},new int[]{16,54},new int[]{42,57,65},new int[]{47,56,62}},
		new int[][]{new int[]{15,22,38,49,54,96},new int[]{18,26,32,45,59,94}},
		new int[][]{new int[]{12,26,45,59,94},new int[]{15,28,49,54,96},new int[]{18,22}},
		new int[][]{new int[]{21,36,65,74},new int[]{25,31,54,76},new int[]{51,64},new int[]{55,61}},
		new int[][]{new int[]{17,74,98},new int[]{18,57},new int[]{58,87,93},new int[]{73,94},new int[]{77,83}},
		new int[][]{new int[]{18,89},new int[]{19,58},new int[]{41,59,64},new int[]{49,61,88},new int[]{54,68}},
		new int[][]{new int[]{21,36,74,92},new int[]{22,34,51,66},new int[]{54,62,71,96}},
		new int[][]{new int[]{42,66,79,81,95},new int[]{45,61,72,96},new int[]{71,86,99}},
		new int[][]{new int[]{11,38,44,69},new int[]{17,31},new int[]{19,67},new int[]{37,48},new int[]{47,64}},
		new int[][]{new int[]{14,58,66},new int[]{16,38},new int[]{18,24},new int[]{28,34},new int[]{36,54,68}},
		new int[][]{new int[]{31,62,73},new int[]{33,51,72},new int[]{37,59,61},new int[]{39,52,67}},
		new int[][]{new int[]{12,39},new int[]{19,22,87},new int[]{21,42},new int[]{27,31,89},new int[]{32,41}},
		new int[][]{new int[]{11,29,34,86,97},new int[]{16,21,39,87,95},new int[]{35,94}},
		new int[][]{new int[]{24,45},new int[]{25,54,62},new int[]{34,52,65},new int[]{35,46},new int[]{36,44}},
		new int[][]{new int[]{14,75},new int[]{15,84},new int[]{71,88},new int[]{74,89},new int[]{78,85},new int[]{79,81}},
		new int[][]{new int[]{14,22,39,66},new int[]{16,38},new int[]{18,26,32,64},new int[]{19,36}},
		new int[][]{new int[]{11,72},new int[]{12,26,64},new int[]{14,29,66},new int[]{19,21},new int[]{22,71}},
		new int[][]{new int[]{41,54},new int[]{43,91},new int[]{44,58},new int[]{48,63},new int[]{51,93},new int[]{53,68}},
		new int[][]{new int[]{16,29},new int[]{19,35,54,86,97},new int[]{26,39,55,87,94}},
		new int[][]{new int[]{12,39},new int[]{19,22,87},new int[]{27,68},new int[]{28,32},new int[]{38,67,89}},
		new int[][]{new int[]{12,83},new int[]{13,61,72},new int[]{21,63},new int[]{23,42,71},new int[]{43,82}},
		new int[][]{new int[]{11,29,63,78},new int[]{18,23,49,71},new int[]{48,79},new int[]{61,73}},
		new int[][]{new int[]{14,59,66,77,95},new int[]{15,56,67,74,89},new int[]{85,99}},
		new int[][]{new int[]{14,59,66,77,85,93},new int[]{15,56,67,74,83,99}},
		new int[][]{new int[]{17,41,59,93},new int[]{18,77},new int[]{19,78},new int[]{47,53,79,91}},
		new int[][]{new int[]{41,54},new int[]{44,58},new int[]{48,71,89},new int[]{49,51},new int[]{59,78,81}},
		new int[][]{new int[]{12,38,89},new int[]{19,22,77,81},new int[]{27,32,78},new int[]{71,88}},
		new int[][]{new int[]{27,79,93},new int[]{29,85,97},new int[]{73,84},new int[]{74,89},new int[]{83,95}},
		new int[][]{new int[]{14,27},new int[]{16,38},new int[]{17,36,89},new int[]{18,34},new int[]{24,39,87}},
		new int[][]{new int[]{41,73},new int[]{42,91},new int[]{43,82},new int[]{51,92},new int[]{52,83},new int[]{53,71}},
		new int[][]{new int[]{11,28},new int[]{15,37},new int[]{17,21,34},new int[]{18,22,35},new int[]{24,32}},
		new int[][]{new int[]{21,53,72},new int[]{22,51,83},new int[]{73,89,96},new int[]{76,82,99}},
		new int[][]{new int[]{11,34},new int[]{14,22,71},new int[]{24,33,72},new int[]{31,43},new int[]{41,73}},
		new int[][]{new int[]{43,55},new int[]{45,59,88},new int[]{46,53,67,89},new int[]{48,66,87}},
		new int[][]{new int[]{41,55,96},new int[]{42,91},new int[]{46,58,95},new int[]{48,52},new int[]{51,92}},
		new int[][]{new int[]{14,95},new int[]{15,44,52,68},new int[]{34,42,58,65},new int[]{35,94}},
		new int[][]{new int[]{14,37,48,65},new int[]{17,24,55,68},new int[]{27,35,44,58}},
		new int[][]{new int[]{14,37,42,55},new int[]{17,24,48,52},new int[]{27,35,44,58}},
		new int[][]{new int[]{31,72},new int[]{32,41,79,96},new int[]{42,91},new int[]{71,99},new int[]{76,92}},
		new int[][]{new int[]{15,38,46},new int[]{18,25},new int[]{28,36,55},new int[]{45,96},new int[]{56,95}},
		new int[][]{new int[]{11,36,74},new int[]{14,37,76,98},new int[]{18,31,47},new int[]{48,97}},
		new int[][]{new int[]{44,67,76,89},new int[]{47,56,79,94},new int[]{57,66},new int[]{84,99}},
		new int[][]{new int[]{15,28},new int[]{18,36,45,87},new int[]{24,37,46,88},new int[]{25,44}},
		new int[][]{new int[]{11,25,66,74,83},new int[]{14,33,65,76,81},new int[]{23,35}},
		new int[][]{new int[]{11,72},new int[]{12,38,67},new int[]{17,21},new int[]{22,71},new int[]{27,32,68}},
		new int[][]{new int[]{11,72},new int[]{12,46,51,68},new int[]{42,66,73},new int[]{58,63,71}},
		new int[][]{new int[]{12,24,53},new int[]{14,28,69,87},new int[]{17,23,52,68,89}},
		new int[][]{new int[]{21,75,82},new int[]{22,71,95},new int[]{85,98},new int[]{86,92},new int[]{88,96}},
		new int[][]{new int[]{16,28,79,95},new int[]{19,35,96},new int[]{25,37,78},new int[]{39,77}},
		new int[][]{new int[]{31,63},new int[]{33,89,91},new int[]{61,83,94},new int[]{74,99},new int[]{79,84}},
		new int[][]{new int[]{11,25,74,82},new int[]{14,33,71,96},new int[]{22,36,85,93}},
		new int[][]{new int[]{14,96},new int[]{16,64},new int[]{34,66},new int[]{36,74},new int[]{54,76},new int[]{56,94}},
		new int[][]{new int[]{13,29,81,98},new int[]{18,32,53},new int[]{21,38,52,83,99}},
		new int[][]{new int[]{37,59,66,78,84},new int[]{39,53,68,74,87},new int[]{56,63}},
		new int[][]{new int[]{12,53},new int[]{13,41,66,92},new int[]{46,51},new int[]{52,91},new int[]{56,63}},
		new int[][]{new int[]{27,49},new int[]{29,87},new int[]{47,78},new int[]{48,89},new int[]{74,88},new int[]{77,84}},
		new int[][]{new int[]{21,42},new int[]{22,61},new int[]{41,54,72},new int[]{44,62},new int[]{52,64,71}},
		new int[][]{new int[]{54,73,96},new int[]{56,84,98},new int[]{74,88},new int[]{75,93},new int[]{78,95}},
		new int[][]{new int[]{25,37,96},new int[]{27,36,49,55},new int[]{46,59,94},new int[]{54,95}},
		new int[][]{new int[]{24,46},new int[]{26,74,87},new int[]{44,59},new int[]{49,56},new int[]{54,77,86}},
		new int[][]{new int[]{21,42},new int[]{22,73},new int[]{23,34},new int[]{24,31},new int[]{33,41},new int[]{43,72}},
		new int[][]{new int[]{12,49,54,63},new int[]{13,61,92},new int[]{44,51,69},new int[]{52,91}},
		new int[][]{new int[]{11,92},new int[]{12,27,73},new int[]{17,23,71},new int[]{72,97},new int[]{77,91}},
		new int[][]{new int[]{11,72,98},new int[]{12,27,73},new int[]{17,23,75,91},new int[]{78,95}},
		new int[][]{new int[]{11,27,63},new int[]{17,23,75,91},new int[]{61,73,98},new int[]{78,95}},
		new int[][]{new int[]{37,98},new int[]{38,74,87},new int[]{73,84,97},new int[]{75,93},new int[]{78,95}},
		new int[][]{new int[]{41,55,68},new int[]{43,56,67},new int[]{47,53,65},new int[]{48,51,66}},
		new int[][]{new int[]{24,58,75,89},new int[]{25,64},new int[]{59,65,84,98},new int[]{78,95}},
		new int[][]{new int[]{18,49},new int[]{19,98},new int[]{28,99},new int[]{29,72,88},new int[]{48,79,82}},
		new int[][]{new int[]{11,29,98},new int[]{17,21},new int[]{18,49},new int[]{19,47},new int[]{27,48,99}},
		new int[][]{new int[]{23,35,64},new int[]{24,33,55},new int[]{27,58,65},new int[]{28,54,67}},
		new int[][]{new int[]{43,65,82},new int[]{45,52,83},new int[]{51,72},new int[]{55,61},new int[]{62,71}},
		new int[][]{new int[]{16,29,82,94},new int[]{18,24,86,92},new int[]{19,98},new int[]{28,99}},
		new int[][]{new int[]{13,29,47},new int[]{14,38},new int[]{17,24,49},new int[]{18,33},new int[]{23,34}},
		new int[][]{new int[]{15,37},new int[]{17,52,68},new int[]{18,35,69},new int[]{39,57},new int[]{59,62}},
		new int[][]{new int[]{65,78,96},new int[]{66,75,87},new int[]{74,88},new int[]{77,84},new int[]{86,98}},
		new int[][]{new int[]{44,51,69},new int[]{49,54,62},new int[]{52,67},new int[]{55,61},new int[]{57,65}},
		new int[][]{new int[]{22,73},new int[]{23,82,97},new int[]{72,89},new int[]{79,85,93},new int[]{87,95}},
		new int[][]{new int[]{11,28},new int[]{13,27},new int[]{17,26,31},new int[]{18,23,36},new int[]{21,38}},
		new int[][]{new int[]{34,45,62,76,83,99},new int[]{36,42,64,73,89,95}},
		new int[][]{new int[]{65,78,86,91},new int[]{66,75,87},new int[]{74,81,98},new int[]{77,84}},
		new int[][]{new int[]{14,96},new int[]{16,54,61,83},new int[]{21,53,66,94},new int[]{23,81}},
		new int[][]{new int[]{27,69,78},new int[]{28,54,67},new int[]{44,59},new int[]{49,64},new int[]{58,79}},
		new int[][]{new int[]{13,29,47},new int[]{17,32,53,98},new int[]{23,38,49,52,97}},
		new int[][]{new int[]{21,34,53},new int[]{24,32,61},new int[]{33,52},new int[]{51,72},new int[]{62,71}},
		new int[][]{new int[]{27,69,78},new int[]{28,59,85,97},new int[]{58,79},new int[]{67,89,95}},
		new int[][]{new int[]{17,79},new int[]{19,41,67},new int[]{43,91},new int[]{49,63,77},new int[]{61,93}},
		new int[][]{new int[]{21,48,62},new int[]{22,51,67},new int[]{27,68},new int[]{28,57},new int[]{41,58}},
		new int[][]{new int[]{44,86},new int[]{45,84},new int[]{46,67},new int[]{47,55},new int[]{57,65},new int[]{66,85}},
		new int[][]{new int[]{41,69},new int[]{44,86},new int[]{46,51},new int[]{49,65,84},new int[]{56,61,85}},
		new int[][]{new int[]{42,54},new int[]{44,62},new int[]{51,64},new int[]{52,67},new int[]{55,61},new int[]{57,65}},
		new int[][]{new int[]{42,84,93},new int[]{43,59,78,82},new int[]{58,74,89},new int[]{73,94}},
		new int[][]{new int[]{24,37,66},new int[]{25,64},new int[]{27,36,45},new int[]{46,75},new int[]{65,76}},
		new int[][]{new int[]{45,57,63,76},new int[]{47,52,66,75},new int[]{53,69},new int[]{59,62}},
		new int[][]{new int[]{27,33,48,61},new int[]{28,54,67},new int[]{31,43,58},new int[]{51,64}},
		new int[][]{new int[]{27,49},new int[]{29,57,88},new int[]{34,45,58,89},new int[]{35,47,54}},
		new int[][]{new int[]{21,42},new int[]{22,61},new int[]{41,58},new int[]{48,52},new int[]{51,72},new int[]{62,71}},
		new int[][]{new int[]{52,67},new int[]{57,61,79,92},new int[]{62,71},new int[]{69,77},new int[]{72,97}},
		new int[][]{new int[]{46,59,74,92},new int[]{49,54,86},new int[]{72,87,96},new int[]{77,84}},
		new int[][]{new int[]{42,83},new int[]{43,75,92},new int[]{65,76},new int[]{66,82,95},new int[]{73,86}},
		new int[][]{new int[]{24,45,86},new int[]{26,54,61,85},new int[]{34,51,65},new int[]{35,44}},
		new int[][]{new int[]{24,46},new int[]{25,44,68},new int[]{26,41,65,93},new int[]{48,63,91}},
		new int[][]{new int[]{13,26,38,44,69,85},new int[]{19,24,33,48,65,86}},
		new int[][]{new int[]{13,38,46,69,85},new int[]{19,33,48,65,84},new int[]{44,86}},
		new int[][]{new int[]{13,25,38,44,69},new int[]{19,33,48,65,84},new int[]{24,85}},
		new int[][]{new int[]{34,45,59},new int[]{35,47,54},new int[]{46,57,63},new int[]{49,53,66}},
		new int[][]{new int[]{14,39,98},new int[]{18,24,99},new int[]{22,34,93},new int[]{29,33,92}},
		new int[][]{new int[]{12,53},new int[]{13,62},new int[]{45,57,63,76},new int[]{47,52,66,75}},
		new int[][]{new int[]{11,27,52,64,73},new int[]{17,23,44,61,72},new int[]{42,54}},
		new int[][]{new int[]{13,38,45,69},new int[]{19,33,58,62},new int[]{42,65},new int[]{48,52}},
		new int[][]{new int[]{13,38,45,69},new int[]{19,33,48,64},new int[]{42,65},new int[]{44,62}},
		new int[][]{new int[]{44,58,77,95},new int[]{45,59,86,98},new int[]{46,57,74,89}},
		new int[][]{new int[]{11,26,82},new int[]{16,32,81,94},new int[]{22,34,56},new int[]{54,96}},
		new int[][]{new int[]{11,39,77},new int[]{17,22,81},new int[]{27,31,59,82},new int[]{57,79}},
		new int[][]{new int[]{11,26,85},new int[]{12,81},new int[]{15,32},new int[]{21,82},new int[]{22,35,86}},
		new int[][]{new int[]{13,37,81},new int[]{17,23,89},new int[]{29,31,63,87},new int[]{61,83}},
		new int[][]{new int[]{42,57,88},new int[]{48,63,82},new int[]{52,83},new int[]{53,67},new int[]{68,87}},
		new int[][]{new int[]{37,58,89,91},new int[]{39,57,85,98},new int[]{81,97},new int[]{87,95}},
		new int[][]{new int[]{37,58,89,91},new int[]{39,57,85,98},new int[]{71,95},new int[]{75,81}},
		new int[][]{new int[]{42,58},new int[]{48,63,72,89},new int[]{52,68,79,81},new int[]{61,83}},
		new int[][]{new int[]{13,29,36,48,62},new int[]{18,26,32,43,59},new int[]{52,69}},
		new int[][]{new int[]{42,59,86,93},new int[]{44,53,82,96},new int[]{49,56,83,94}},
		new int[][]{new int[]{15,46},new int[]{16,83,95},new int[]{45,86,99},new int[]{84,93},new int[]{89,94}},
		new int[][]{new int[]{15,21,38,67},new int[]{17,25,31,76,88},new int[]{68,77,86}},
		new int[][]{new int[]{11,38,67},new int[]{17,21},new int[]{27,31,76,88},new int[]{68,77,86}},
		new int[][]{new int[]{11,25},new int[]{14,31},new int[]{15,84,99},new int[]{21,35},new int[]{34,89,95}},
		new int[][]{new int[]{14,27,95},new int[]{17,35,94},new int[]{25,38},new int[]{28,77},new int[]{37,78}},
		new int[][]{new int[]{11,37,49,63},new int[]{19,23,41,57},new int[]{27,33},new int[]{53,67}},
		new int[][]{new int[]{13,29,36,48,52,64},new int[]{18,26,32,43,54,69}},
		new int[][]{new int[]{14,76},new int[]{16,27,55},new int[]{17,34},new int[]{24,37},new int[]{25,56,74}},
		new int[][]{new int[]{16,29,68},new int[]{18,36,79,94},new int[]{26,39},new int[]{69,74,98}},
		new int[][]{new int[]{11,36,43,68},new int[]{16,23,48,51},new int[]{26,33},new int[]{58,61}},
		new int[][]{new int[]{11,36,43,68},new int[]{16,39},new int[]{19,33,48,51},new int[]{58,61}},
		new int[][]{new int[]{13,29,71,98},new int[]{14,23,75,91},new int[]{19,25,78,94}},
		new int[][]{new int[]{14,39,47,56,62},new int[]{19,26,44,52,67},new int[]{29,36}},
		new int[][]{new int[]{41,82,95},new int[]{42,59,83},new int[]{49,53,87,91},new int[]{85,97}},
		new int[][]{new int[]{13,34,75,87},new int[]{14,22,77,83},new int[]{24,33,72,85}},
		new int[][]{new int[]{11,35,84,93},new int[]{14,23,55,91},new int[]{25,33},new int[]{54,85}},
		new int[][]{new int[]{15,46},new int[]{16,48,79,95},new int[]{45,76,92},new int[]{49,72,98}},
		new int[][]{new int[]{12,26,37,74,81,99},new int[]{17,21,36,79,84,92}},
		new int[][]{new int[]{41,75,82,97},new int[]{42,59,83},new int[]{49,53,77,85,91}},
		new int[][]{new int[]{54,68,71,85},new int[]{56,74,88},new int[]{58,65,86},new int[]{78,81}},
		new int[][]{new int[]{13,28,69,87},new int[]{19,32,67,73,88},new int[]{23,38,72}},
		new int[][]{new int[]{11,29,54,77,85},new int[]{14,21,55},new int[]{17,25,79,84}},
		new int[][]{new int[]{11,28,42},new int[]{13,91},new int[]{18,33},new int[]{22,38},new int[]{32,41,93}},
		new int[][]{new int[]{11,26,43,74,82,98},new int[]{14,23,42,78,86,91}},
		new int[][]{new int[]{42,64},new int[]{43,65},new int[]{44,69,98},new int[]{45,62},new int[]{48,63,99}},
		new int[][]{new int[]{27,86,99},new int[]{29,42,57,78,96},new int[]{48,52,76,87}},
		new int[][]{new int[]{43,65},new int[]{45,53},new int[]{55,67},new int[]{57,88},new int[]{58,63},new int[]{68,87}},
		new int[][]{new int[]{37,59},new int[]{39,67},new int[]{53,69},new int[]{57,88},new int[]{58,63},new int[]{68,87}},
		new int[][]{new int[]{11,84,93},new int[]{13,38,82,94},new int[]{18,22,81},new int[]{28,32}},
		new int[][]{new int[]{12,27,75,81,98},new int[]{13,72,88,95},new int[]{17,21,83}},
		new int[][]{new int[]{11,24,37},new int[]{12,28,35},new int[]{15,21,38},new int[]{17,22,34}},
		new int[][]{new int[]{11,24,37},new int[]{12,38},new int[]{17,22,34},new int[]{18,21},new int[]{28,32}},
		new int[][]{new int[]{46,59},new int[]{49,65,78,96},new int[]{54,69,75,98},new int[]{56,94}},
		new int[][]{new int[]{25,47,56},new int[]{26,85},new int[]{43,57},new int[]{45,63,86},new int[]{53,66}},
		new int[][]{new int[]{42,54},new int[]{44,67},new int[]{45,62},new int[]{47,55},new int[]{52,65},new int[]{57,64}},
		new int[][]{new int[]{11,28,77,82},new int[]{17,99},new int[]{19,21,88,92},new int[]{72,97}},
		new int[][]{new int[]{47,88},new int[]{48,89},new int[]{49,97},new int[]{67,98},new int[]{68,99},new int[]{69,87}},
		new int[][]{new int[]{72,89,96},new int[]{73,85},new int[]{75,92},new int[]{76,83,99},new int[]{82,95}},
		new int[][]{new int[]{14,27,58,86},new int[]{18,25,59,84},new int[]{26,85},new int[]{29,57}},
		new int[][]{new int[]{23,39,48,52,91},new int[]{29,31,43,68,92},new int[]{58,62}},
		new int[][]{new int[]{22,39,43,57},new int[]{27,33,48,51},new int[]{28,31,49,52}},
		new int[][]{new int[]{52,64,73,95},new int[]{55,96},new int[]{56,61,74,92},new int[]{63,71}},
		new int[][]{new int[]{24,66,85,98},new int[]{26,54,75},new int[]{55,88,94},new int[]{65,76}},
		new int[][]{new int[]{24,42,57,96},new int[]{25,44,52},new int[]{26,47,55},new int[]{46,94}},
		new int[][]{new int[]{12,36,73},new int[]{16,21,45,72},new int[]{25,31,46},new int[]{33,71}},
		new int[][]{new int[]{14,43,55},new int[]{15,44,68,76},new int[]{48,53},new int[]{58,66,75}},
		new int[][]{new int[]{14,43,55},new int[]{15,64,76},new int[]{44,53,68},new int[]{58,66,75}},
		new int[][]{new int[]{15,38,57},new int[]{16,29},new int[]{17,26},new int[]{19,25},new int[]{27,35,58}},
		new int[][]{new int[]{14,56},new int[]{15,74},new int[]{16,95},new int[]{45,59,96},new int[]{49,54,75}},
		new int[][]{new int[]{17,98},new int[]{18,89},new int[]{19,77},new int[]{75,97},new int[]{79,85},new int[]{88,95}},
		new int[][]{new int[]{15,27,44,69,96},new int[]{16,47,65,94},new int[]{19,26,67}},
		new int[][]{new int[]{43,56,94},new int[]{44,53,68},new int[]{55,96},new int[]{58,65},new int[]{64,95}},
		new int[][]{new int[]{12,83},new int[]{13,25,74},new int[]{14,21,75,82},new int[]{23,71,84}},
		new int[][]{new int[]{21,53},new int[]{22,51,69},new int[]{23,44,62},new int[]{43,54},new int[]{59,64}},
		new int[][]{new int[]{11,28,34,76},new int[]{13,36},new int[]{16,21,74},new int[]{18,24,33}},
		new int[][]{new int[]{24,96},new int[]{26,74},new int[]{27,78},new int[]{28,97},new int[]{76,98},new int[]{77,94}},
		new int[][]{new int[]{15,76,82},new int[]{16,95},new int[]{55,72,87,96},new int[]{56,77,85}},
		new int[][]{new int[]{42,58,87},new int[]{47,62,88},new int[]{52,69},new int[]{54,68},new int[]{59,64}},
		new int[][]{new int[]{16,37,78,94},new int[]{17,34,46,69},new int[]{49,64,76,98}},
		new int[][]{new int[]{12,37,99},new int[]{18,22},new int[]{19,88,93},new int[]{28,32,83,97}},
		new int[][]{new int[]{13,36,41,55,84,92},new int[]{15,33,46,52,81,94}},
		new int[][]{new int[]{13,25,54,62},new int[]{14,22,63},new int[]{24,53,66},new int[]{26,65}},
		new int[][]{new int[]{42,56},new int[]{45,52},new int[]{46,71,95},new int[]{54,76},new int[]{55,74,91}},
		new int[][]{new int[]{13,27},new int[]{14,23},new int[]{17,34,78},new int[]{24,38,67},new int[]{68,77}},
		new int[][]{new int[]{17,48,64,76,99},new int[]{19,45,67,78,94},new int[]{46,75}},
		new int[][]{new int[]{21,86,92},new int[]{22,43,56,81},new int[]{46,52,93},new int[]{83,96}},
		new int[][]{new int[]{22,39},new int[]{28,32},new int[]{29,47,63,88},new int[]{38,43,69,87}},
		new int[][]{new int[]{34,43,55,86,92},new int[]{35,56,83,94},new int[]{46,52,93}},
		new int[][]{new int[]{13,28,35,79},new int[]{17,33},new int[]{19,57},new int[]{25,37,59,78}},
		new int[][]{new int[]{13,29,36,72},new int[]{16,21,73},new int[]{22,39,71},new int[]{26,31}},
		new int[][]{new int[]{11,27},new int[]{15,33,61},new int[]{17,25},new int[]{21,62},new int[]{22,35,63}},
		new int[][]{new int[]{23,34,52,66,81,95},new int[]{25,33,54,62,86,91}},
		new int[][]{new int[]{42,58,76,93},new int[]{48,61,72},new int[]{51,68},new int[]{53,71,96}},
		new int[][]{new int[]{12,34,77,81},new int[]{14,38,51,72,87},new int[]{18,31,52}},
		new int[][]{new int[]{13,25,39,41,67,88},new int[]{17,21,35,48,63,89}},
		new int[][]{new int[]{42,55,68,86,91},new int[]{43,82},new int[]{46,58,61,83,95}},
		new int[][]{new int[]{42,65,86,91},new int[]{43,82},new int[]{46,51,83,95},new int[]{55,61}},
		new int[][]{new int[]{42,65},new int[]{45,76,97},new int[]{46,62},new int[]{66,77,85},new int[]{87,95}},
		new int[][]{new int[]{25,39,47,56},new int[]{27,36,59,64,75},new int[]{45,67,74}},
		new int[][]{new int[]{11,35},new int[]{14,46},new int[]{15,54},new int[]{16,31},new int[]{34,55},new int[]{36,44}},
		new int[][]{new int[]{13,35,67,89,96},new int[]{16,39,85,97},new int[]{17,33,69}},
		new int[][]{new int[]{15,39,48,54,87,96},new int[]{16,37,45,58,84,99}},
		new int[][]{new int[]{25,38,81,94},new int[]{28,34,53,75,91},new int[]{51,73,85}},
		new int[][]{new int[]{13,35,51,69},new int[]{15,31,43},new int[]{41,93},new int[]{59,63,91}},
		new int[][]{new int[]{15,27,48},new int[]{18,25,57,79},new int[]{42,59,78},new int[]{47,52}},
		new int[][]{new int[]{13,25,41,56,92},new int[]{16,33,45,51},new int[]{22,35,93}},
		new int[][]{new int[]{41,93},new int[]{43,56,69,94},new int[]{44,51,96},new int[]{59,63,91}},
		new int[][]{new int[]{24,37,59,65,96},new int[]{29,35,44,67},new int[]{49,56,94}},
		new int[][]{new int[]{12,28,36},new int[]{14,22},new int[]{16,42,64},new int[]{24,38,46,62}},
		new int[][]{new int[]{11,27,49,63},new int[]{12,41},new int[]{13,42,59},new int[]{29,53,67}},
		new int[][]{new int[]{13,25,74},new int[]{14,76},new int[]{16,29,75,88},new int[]{18,23,89}},
		new int[][]{new int[]{35,46,62,84},new int[]{36,55},new int[]{42,56,68},new int[]{58,64,85}},
		new int[][]{new int[]{12,24,63},new int[]{14,49,53,65},new int[]{15,23,44,59,62}},
		new int[][]{new int[]{28,89},new int[]{29,58,97},new int[]{57,61,88,92},new int[]{62,81,99}},
		new int[][]{new int[]{31,67,72,98},new int[]{32,86,91},new int[]{68,77,82},new int[]{88,96}},
		new int[][]{new int[]{15,46},new int[]{16,95},new int[]{45,71,86},new int[]{65,96},new int[]{66,75,81}},
		new int[][]{new int[]{31,92},new int[]{32,81},new int[]{37,89,91},new int[]{39,47},new int[]{49,82,97}},
		new int[][]{new int[]{11,28,35,42,56},new int[]{16,22,38,41},new int[]{36,45,51}},
		new int[][]{new int[]{41,65,72,86},new int[]{46,52,71},new int[]{56,62},new int[]{66,75,81}},
		new int[][]{new int[]{37,89,91},new int[]{39,47,76,81},new int[]{49,77,86},new int[]{71,97}},
		new int[][]{new int[]{37,53,81,98},new int[]{38,51,77,93},new int[]{71,97},new int[]{87,91}},
		new int[][]{new int[]{41,56,69,92},new int[]{49,63,84,91},new int[]{52,66,83,94}},
		new int[][]{new int[]{13,27,89,95},new int[]{16,23,64},new int[]{19,24,66,85,97}},
		new int[][]{new int[]{12,27},new int[]{16,32,45,74},new int[]{17,26},new int[]{22,35,44,76}},
		new int[][]{new int[]{21,38,89,96},new int[]{29,31,72,98},new int[]{79,82},new int[]{86,92}},
		new int[][]{new int[]{11,38,82},new int[]{15,22,81},new int[]{18,36,55},new int[]{25,32,56}},
		new int[][]{new int[]{12,27},new int[]{17,68},new int[]{18,22},new int[]{28,47},new int[]{43,67},new int[]{48,63}},
		new int[][]{new int[]{13,38,52},new int[]{15,37,56},new int[]{17,32,53},new int[]{18,36,55}},
		new int[][]{new int[]{35,59,66},new int[]{36,49,55,74},new int[]{44,75},new int[]{45,69,76}},
		new int[][]{new int[]{16,29,98},new int[]{18,26,47},new int[]{21,38,99},new int[]{27,31,48}},
		new int[][]{new int[]{13,24},new int[]{14,96},new int[]{16,33},new int[]{23,35,44},new int[]{36,45,94}},
		new int[][]{new int[]{28,56,62,99},new int[]{29,88},new int[]{52,69,98},new int[]{58,66,89}},
		new int[][]{new int[]{41,69,82,97},new int[]{42,67,88,93},new int[]{48,63,89,91}},
		new int[][]{new int[]{46,58,74},new int[]{48,55,76,89,91},new int[]{54,71,85,99}},
		new int[][]{new int[]{52,67,73,96},new int[]{54,63,79,92},new int[]{57,64,76,99}},
		new int[][]{new int[]{14,38,47,66},new int[]{17,26,54,68},new int[]{28,36,44,57}},
		new int[][]{new int[]{42,68},new int[]{45,57,84},new int[]{47,52},new int[]{48,54,85},new int[]{58,62}},
		new int[][]{new int[]{14,29},new int[]{16,54,67},new int[]{19,26},new int[]{24,46,57},new int[]{47,66}},
		new int[][]{new int[]{51,75,93},new int[]{53,71,85},new int[]{73,99},new int[]{79,83},new int[]{89,95}},
		new int[][]{new int[]{14,38,46,97},new int[]{15,44},new int[]{17,25,98},new int[]{28,36,45}},
		new int[][]{new int[]{21,36,43},new int[]{22,75,81},new int[]{26,33,72,85},new int[]{41,83}},
		new int[][]{new int[]{24,82,96},new int[]{25,54},new int[]{26,45},new int[]{46,85,92},new int[]{55,84}},
		new int[][]{new int[]{11,29,52,67},new int[]{17,22,35,51,68},new int[]{25,38,69}},
		new int[][]{new int[]{72,87},new int[]{73,99},new int[]{74,93},new int[]{77,84},new int[]{79,92},new int[]{82,94}},
		new int[][]{new int[]{17,49,51},new int[]{19,57,61},new int[]{41,66,95},new int[]{45,69,96}},
		new int[][]{new int[]{25,54,67,86},new int[]{26,47,65},new int[]{46,57,85},new int[]{55,84}},
		new int[][]{new int[]{22,36,93},new int[]{26,33,72,85},new int[]{62,75,83},new int[]{63,92}},
		new int[][]{new int[]{37,51,69,78},new int[]{39,77,92},new int[]{58,61,99},new int[]{72,98}},
		new int[][]{new int[]{53,66,82,95},new int[]{55,61,76,93},new int[]{62,81},new int[]{71,86}},
		new int[][]{new int[]{12,43},new int[]{13,28,34,59},new int[]{14,32},new int[]{23,39,42,58}},
		new int[][]{new int[]{12,44,53,76},new int[]{13,42,56,69},new int[]{49,64},new int[]{66,74}},
		new int[][]{new int[]{44,96},new int[]{45,78,94},new int[]{46,85,92},new int[]{72,98},new int[]{75,82}},
		new int[][]{new int[]{11,36,72,85},new int[]{15,21},new int[]{22,75,81},new int[]{25,32,76}},
		new int[][]{new int[]{15,27,59},new int[]{19,35,98},new int[]{25,38},new int[]{28,97},new int[]{57,99}},
		new int[][]{new int[]{44,69,76,97},new int[]{47,54},new int[]{56,74},new int[]{57,99},new int[]{59,66}},
		new int[][]{new int[]{41,54},new int[]{44,59,87,91},new int[]{47,51,84,99},new int[]{81,94}},
		new int[][]{new int[]{41,59,65,73,87,94},new int[]{47,54,63,75,81,99}},
		new int[][]{new int[]{13,27,34,76,82,98},new int[]{18,26,32,73,87,94}},
		new int[][]{new int[]{43,55,81},new int[]{45,63,84},new int[]{51,64,73,85},new int[]{74,83}},
		new int[][]{new int[]{13,24,56,62,75},new int[]{16,32,55,63},new int[]{22,35,74}},
		new int[][]{new int[]{42,57,73,88,95},new int[]{43,59,77,92},new int[]{58,85,99}},
		new int[][]{new int[]{12,39,45,94},new int[]{14,32,96},new int[]{19,36,44},new int[]{46,95}},
		new int[][]{new int[]{42,55,71},new int[]{43,72},new int[]{45,61,73},new int[]{51,67},new int[]{57,65}},
		new int[][]{new int[]{57,89},new int[]{58,74,87},new int[]{59,78,81},new int[]{71,94},new int[]{84,91}},
		new int[][]{new int[]{18,59},new int[]{19,33,51,68},new int[]{31,58,62},new int[]{32,53,69}},
		new int[][]{new int[]{11,34,43,56,82,95},new int[]{16,32,44,51,85,93}},
		new int[][]{new int[]{15,27},new int[]{16,65},new int[]{17,99},new int[]{19,26},new int[]{25,66},new int[]{29,97}},
		new int[][]{new int[]{11,39,57,83},new int[]{12,81},new int[]{17,23,82},new int[]{27,33,59}},
		new int[][]{new int[]{24,45,53,76,97},new int[]{25,43,66,77,94},new int[]{56,63}},
		new int[][]{new int[]{43,66,77,95},new int[]{44,53,76,97},new int[]{45,94},new int[]{56,63}},
		new int[][]{new int[]{14,75,81},new int[]{15,43,71,96},new int[]{16,84,93},new int[]{41,83}},
		new int[][]{new int[]{13,28,36,42,54,69},new int[]{16,22,38,44,59,63}},
		new int[][]{new int[]{12,24,37,88},new int[]{14,29,32,67},new int[]{28,89},new int[]{69,87}},
		new int[][]{new int[]{21,42,83},new int[]{23,71},new int[]{41,58,77,82},new int[]{57,73,88}},
		new int[][]{new int[]{24,37,59,96},new int[]{28,36,77,94},new int[]{29,58},new int[]{57,78}},
		new int[][]{new int[]{45,77,86},new int[]{46,95},new int[]{62,83,96},new int[]{63,75,87,92}},
		new int[][]{new int[]{27,35,56,69},new int[]{29,41,57},new int[]{36,45,51},new int[]{49,65}},
		new int[][]{new int[]{21,35},new int[]{23,38,82,97},new int[]{25,32,87,93},new int[]{28,31}},
		new int[][]{new int[]{12,26,75,93},new int[]{15,29,72,96},new int[]{19,22},new int[]{23,92}},
		new int[][]{new int[]{27,61,78,93},new int[]{28,76,87},new int[]{63,71,96},new int[]{86,97}},
		new int[][]{new int[]{14,38,47,76},new int[]{17,36,58,74},new int[]{43,57},new int[]{48,53}},
		new int[][]{new int[]{27,56,69,78},new int[]{29,48,62,77},new int[]{42,58},new int[]{52,66}},
		new int[][]{new int[]{24,55,86},new int[]{25,84,92},new int[]{56,75,88},new int[]{78,82,95}},
		new int[][]{new int[]{37,48,55},new int[]{38,97},new int[]{45,69,98},new int[]{57,99},new int[]{59,65}},
		new int[][]{new int[]{17,68},new int[]{18,89},new int[]{19,97},new int[]{57,99},new int[]{59,87},new int[]{67,88}},
		new int[][]{new int[]{11,48,52,83},new int[]{12,61},new int[]{21,62},new int[]{22,43,58,81}},
		new int[][]{new int[]{17,68},new int[]{18,52,83,99},new int[]{19,53,87,92},new int[]{67,88}},
		new int[][]{new int[]{14,28},new int[]{16,44},new int[]{18,25,76,83},new int[]{24,46,73,85}},
		new int[][]{new int[]{13,24,85},new int[]{15,23,44,76},new int[]{46,78,84},new int[]{75,88}},
		new int[][]{new int[]{42,56},new int[]{45,62,94},new int[]{46,57},new int[]{47,54,95},new int[]{52,64}},
		new int[][]{new int[]{11,89,93},new int[]{13,25},new int[]{15,86,99},new int[]{16,23,81,95}},
		new int[][]{new int[]{44,67,88,96},new int[]{48,66,77,84},new int[]{76,87},new int[]{86,97}},
		new int[][]{new int[]{12,24,75,83},new int[]{13,72,84,99},new int[]{15,23,79,94}},
		new int[][]{new int[]{42,55,76},new int[]{43,75,92},new int[]{46,53,95},new int[]{52,73,96}},
		new int[][]{new int[]{27,79,83},new int[]{29,54,67},new int[]{59,64,78},new int[]{68,73,87}},
		new int[][]{new int[]{21,57,62},new int[]{22,37,58},new int[]{23,51},new int[]{28,32,53,67}},
		new int[][]{new int[]{52,73,84},new int[]{53,64,85},new int[]{55,63,72},new int[]{65,74,82}},
		new int[][]{new int[]{24,76,89},new int[]{26,54,69},new int[]{55,79,84},new int[]{59,66,75}},
		new int[][]{new int[]{16,37},new int[]{17,85,98},new int[]{18,26},new int[]{28,36},new int[]{38,87,95}},
		new int[][]{new int[]{16,37,54,85,98},new int[]{18,24,56},new int[]{28,34,87,95}},
		new int[][]{new int[]{14,45,68},new int[]{15,37,48,66},new int[]{17,24},new int[]{27,36,64}},
		new int[][]{new int[]{27,79,92},new int[]{28,87},new int[]{29,88},new int[]{47,89},new int[]{49,72,97}},
		new int[][]{new int[]{42,59,71},new int[]{46,52},new int[]{49,51,66,73},new int[]{56,63,72}},
		new int[][]{new int[]{21,67,88,93},new int[]{23,57,62,81},new int[]{53,68,82,97}},
		new int[][]{new int[]{11,27,52},new int[]{16,22,51,94},new int[]{17,36},new int[]{24,37,96}},
		new int[][]{new int[]{23,37,59,61},new int[]{29,32,53,67},new int[]{31,72},new int[]{62,71}},
		new int[][]{new int[]{43,55},new int[]{44,53},new int[]{45,61,72,94},new int[]{54,62,75,91}},
		new int[][]{new int[]{11,37,59,83},new int[]{14,23,81},new int[]{19,24},new int[]{29,33,57}},
		new int[][]{new int[]{34,75},new int[]{35,84},new int[]{37,59,74,88},new int[]{39,58,77,85}},
		new int[][]{new int[]{34,75},new int[]{35,46,59,88},new int[]{36,44},new int[]{48,55,74,89}},
		new int[][]{new int[]{14,55},new int[]{15,44},new int[]{45,61,72,94},new int[]{54,62,75,91}},
		new int[][]{new int[]{21,59,62,98},new int[]{22,91},new int[]{51,68,82,99},new int[]{88,92}},
		new int[][]{new int[]{14,45,51,73},new int[]{15,46,74},new int[]{43,56,71},new int[]{54,76}},
		new int[][]{new int[]{14,55},new int[]{15,29,76,98},new int[]{19,26,78,94},new int[]{54,95}},
		new int[][]{new int[]{12,73},new int[]{13,39},new int[]{19,22,34,85},new int[]{25,33,72,84}},
		new int[][]{new int[]{11,27,88},new int[]{16,21,94},new int[]{18,36,87},new int[]{24,37,96}},
		new int[][]{new int[]{14,28,52,89,91},new int[]{19,24,78,82},new int[]{51,72,98}},
		new int[][]{new int[]{41,58,72,89},new int[]{49,61,88},new int[]{52,68,81},new int[]{78,82}},
		new int[][]{new int[]{51,82},new int[]{52,66,73,85},new int[]{55,61,76,83},new int[]{62,81}},
		new int[][]{new int[]{11,29,68},new int[]{18,22,41,69},new int[]{21,92},new int[]{48,62,91}},
		new int[][]{new int[]{21,42,66,73,85},new int[]{22,45,61,76,93},new int[]{83,95}},
		new int[][]{new int[]{12,26,43,58},new int[]{16,23,38,49,52},new int[]{29,36,48}},
		new int[][]{new int[]{56,69},new int[]{57,78,94},new int[]{58,66},new int[]{59,97},new int[]{68,74,99}},
		new int[][]{new int[]{31,43,57,72,99},new int[]{33,49,51,78,92},new int[]{58,77}},
		new int[][]{new int[]{41,82,98},new int[]{42,86,91},new int[]{71,88},new int[]{76,81},new int[]{78,96}},
		new int[][]{new int[]{13,29,34,77},new int[]{17,24,33,99},new int[]{76,97},new int[]{79,96}},
		new int[][]{new int[]{13,34},new int[]{14,27,76},new int[]{16,33},new int[]{17,36},new int[]{26,37,74}},
		new int[][]{new int[]{31,82,97},new int[]{32,73},new int[]{33,67,79,91},new int[]{69,72,87}},
		new int[][]{new int[]{11,27,79,93},new int[]{12,71,86,99},new int[]{17,23,82,96}},
		new int[][]{new int[]{21,36,63},new int[]{26,33,41,65},new int[]{35,46,51},new int[]{55,61}},
		new int[][]{new int[]{52,68,83},new int[]{57,63,78,82},new int[]{58,74,87},new int[]{73,84}},
		new int[][]{new int[]{14,37,79,95},new int[]{19,35,54,61,77},new int[]{51,65,94}},
		new int[][]{new int[]{12,37,58,63},new int[]{18,53,69},new int[]{19,22,67},new int[]{27,32}},
		new int[][]{new int[]{11,27,52,83,99},new int[]{19,22,51,87},new int[]{23,89,92}},
		new int[][]{new int[]{24,86,97},new int[]{26,89,94},new int[]{41,87,93},new int[]{43,81,99}},
		new int[][]{new int[]{11,29,36,88,93},new int[]{13,78,81},new int[]{26,38,73,99}},
		new int[][]{new int[]{24,85,93},new int[]{25,42,66},new int[]{26,44,62},new int[]{45,83,94}},
		new int[][]{new int[]{42,64,75},new int[]{45,62,96},new int[]{53,66,95},new int[]{55,63,74}},
		new int[][]{new int[]{12,29,78,81},new int[]{18,21,57,62,79},new int[]{51,67,82}},
		new int[][]{new int[]{24,37,49,66},new int[]{28,36,64,87},new int[]{29,44,67,88}},
		new int[][]{new int[]{17,49},new int[]{18,57,64},new int[]{19,54,68},new int[]{43,59},new int[]{47,53}},
		new int[][]{new int[]{14,28,65},new int[]{17,25,54,69},new int[]{18,57,64},new int[]{29,68}},
		new int[][]{new int[]{42,68,77},new int[]{47,54,62,79},new int[]{49,52,78},new int[]{58,64}},
		new int[][]{new int[]{34,57,86,98},new int[]{36,43,82,94},new int[]{42,58,87,93}},
		new int[][]{new int[]{34,96},new int[]{36,84},new int[]{37,88,94},new int[]{38,86,99},new int[]{39,97}},
		new int[][]{new int[]{21,82},new int[]{22,91},new int[]{31,83,95},new int[]{32,81},new int[]{33,85,92}},
		new int[][]{new int[]{26,38,73,87,94},new int[]{27,36,78,93},new int[]{34,86,97}},
		new int[][]{new int[]{11,83,95},new int[]{13,36},new int[]{16,32,91},new int[]{33,82},new int[]{85,92}},
		new int[][]{new int[]{17,69},new int[]{19,36,57,74},new int[]{34,59,66},new int[]{54,67,76}},
		new int[][]{new int[]{17,69},new int[]{19,57,66,75},new int[]{54,67,76},new int[]{59,65,74}},
		new int[][]{new int[]{15,29,57,62},new int[]{16,55,67},new int[]{19,26},new int[]{25,52,66}},
		new int[][]{new int[]{32,83,95},new int[]{33,41,69,75,92},new int[]{49,63,71,85}},
		new int[][]{new int[]{25,32,76,81},new int[]{26,31,85,92},new int[]{72,86},new int[]{82,96}},
		new int[][]{new int[]{43,67,88,92},new int[]{48,53},new int[]{58,82,99},new int[]{59,63,97}},
		new int[][]{new int[]{45,53,67,88,92},new int[]{48,55,82,99},new int[]{59,63,97}},
		new int[][]{new int[]{21,42},new int[]{22,71,89},new int[]{41,79,85,93},new int[]{43,82,95}},
		new int[][]{new int[]{35,86,98},new int[]{36,42,73,95},new int[]{43,88,92},new int[]{76,83}},
		new int[][]{new int[]{24,48,56},new int[]{26,41,58,74},new int[]{44,61},new int[]{51,64,76}},
		new int[][]{new int[]{41,64,72,99},new int[]{44,52,79,81},new int[]{54,62,89,91}},
		new int[][]{new int[]{41,59,87,94},new int[]{47,56,61,84,99},new int[]{51,64,96}},
		new int[][]{new int[]{14,55,69},new int[]{15,58,64},new int[]{59,71,98},new int[]{68,79,91}},
		new int[][]{new int[]{11,53},new int[]{12,44,51},new int[]{13,59,62},new int[]{42,64},new int[]{54,69}},
		new int[][]{new int[]{12,25,56,61},new int[]{13,52},new int[]{16,23,65},new int[]{21,53,66}},
		new int[][]{new int[]{16,39,45,58},new int[]{18,35,46,69},new int[]{48,56},new int[]{49,65}},
		new int[][]{new int[]{43,68,76,82},new int[]{48,52,73,86},new int[]{56,62},new int[]{58,66}},
		new int[][]{new int[]{42,58,87,96},new int[]{47,89},new int[]{49,52,76,98},new int[]{79,86}},
		new int[][]{new int[]{32,73},new int[]{33,52},new int[]{44,53,75,81},new int[]{45,51,72,84}},
		new int[][]{new int[]{13,25,41},new int[]{14,21,43},new int[]{15,27,69},new int[]{19,24,67}},
		new int[][]{new int[]{11,42,69},new int[]{12,48,61},new int[]{13,28,49},new int[]{19,23,68}},
		new int[][]{new int[]{37,89},new int[]{38,46,57},new int[]{39,56,67,88},new int[]{48,66,87}},
		new int[][]{new int[]{24,75,89},new int[]{25,38,79},new int[]{29,34,88},new int[]{35,78,84}},
		new int[][]{new int[]{22,53,64},new int[]{23,72,86},new int[]{54,61,83},new int[]{62,76,81}},
		new int[][]{new int[]{13,25,42,76},new int[]{16,32,43,75},new int[]{22,37},new int[]{27,35}},
		new int[][]{new int[]{18,49,55,84},new int[]{19,44,58,87,96},new int[]{56,85,97}},
		new int[][]{new int[]{12,35,73,88},new int[]{15,33,77,82},new int[]{75,87},new int[]{78,85}},
		new int[][]{new int[]{15,29,73,86},new int[]{17,26,89,95},new int[]{19,75,83,97}},
		new int[][]{new int[]{35,76,91},new int[]{36,83,95},new int[]{44,86,93},new int[]{46,71,94}},
		new int[][]{new int[]{42,55,86,91},new int[]{45,51,73,96},new int[]{46,53,75,82}},
		new int[][]{new int[]{41,55,86},new int[]{42,74,91},new int[]{46,52,85},new int[]{51,72,94}}
	};

	/**
	 * find all unavoidable sets on the board
	 * @param board
	 * @return
	 */
	public static LinkedList<BitSet> unavoidableSets(int[][] board) {
		// make sure board is solved
		SudokuSolver ss = new SudokuSolver(board);
		board = ss.solvedBoard();

		LinkedList<BitSet> list = new LinkedList<BitSet>();

		list.addAll(us4(board));
		//System.out.println(list.size() + " unavoidable sets found");
		list.addAll(us6(board));
		//System.out.println(list.size() + " unavoidable sets found");
		list.addAll(usGeneral(unavpat8,board));
		//System.out.println(list.size() + " unavoidable sets found");
		list.addAll(usGeneral(unavpat9,board));
		//System.out.println(list.size() + " unavoidable sets found");
		list.addAll(usGeneral(unavpat10,board));
		//System.out.println(list.size() + " unavoidable sets found");
		list.addAll(usGeneral(unavpat11,board));
		//System.out.println(list.size() + " unavoidable sets found");
		list.addAll(usGeneral(unavpat12,board));
		//System.out.println(list.size() + " unavoidable sets found");
		eliminateDuplicates(list);
		//System.out.println(list.size() + " unique unavoidable sets found");

		return list;
	}

	public static LinkedList<BitSet> usGeneral(int[][][] unavpat, int[][] board) {
		return usGeneral(unavpat,board,new boolean[81]);
	}

	/**
	 * find all unavoidable sets of size 4 on the board<br>
	 * rectangles where two numbers can go either way<br>
	 * AB<br>
	 * ---<br>
	 * BA<br>
	 * @param board
	 * @return unavoidable sets of size 4
	 */
	public static LinkedList<BitSet> us4(int[][] board) {
		LinkedList<BitSet> list = new LinkedList<BitSet>();

		for (int r1 = 0; r1 < 6; r1++) {
			for (int r2 = 3*(r1/3+1); r2 < 9; r2++) {
				for (int c1 = 0; c1 < 6; c1++) {
					for (int c2 = c1+1; c2 < 3*(c1/3+1); c2++) {
						if (board[r1][c1] == board[r2][c2] && board[r1][c2] == board[r2][c1]) {
							BitSet bs = new BitSet();
							bs.set(9*r1+c1);
							bs.set(9*r1+c2);
							bs.set(9*r2+c1);
							bs.set(9*r2+c2);
							list.add(bs);
						}
						// do transpose
						if (board[c1][r1] == board[c2][r2] && board[c2][r1] == board[c1][r2]) {
							BitSet bs = new BitSet();
							bs.set(9*c1+r1);
							bs.set(9*c2+r1);
							bs.set(9*c1+r2);
							bs.set(9*c2+r2);
							list.add(bs);
						}
					}
				}
			}
		}

		return list;
	}

	/**
	 * find all unavoidable sets of size 6 on the board<br>
	 * 4 types<br>
	 * AB<br>
	 * ---<br>
	 * BC<br>
	 * ---<br>
	 * CA<br>
	 * ====<br>
	 * AB<br>
	 * ---<br>
	 * B A<br>
	 * ---<br>
	 *  AB<br>
	 * ====<br>
	 * ABC<br>
	 * ---<br>
	 * BCA<br>
	 * ====<br>
	 * AB |<br>
	 * ---+---<br>
	 * B  |A<br>
	 *  A |B<br>
	 * @param board
	 * @return unavoidable sets of size 6
	 */
	public static LinkedList<BitSet> us6(int[][] board) {
		LinkedList<BitSet> list = new LinkedList<BitSet>();

		// Type 1
		/*
		 * AB<br>
		 * ---<br>
		 * BC<br>
		 * ---<br>
		 * CA<br>
		 */
		//System.out.print("Type 1...");
		for (int r1 = 0; r1 < 3; r1++) {
			for (int r2 = 3; r2 < 6; r2++) {
				for (int r3 = 6; r3 < 9; r3++) {
					for (int c1 = 0; c1 < 6; c1++) {
						for (int c2 = c1+1; c2 < 3*(c1/3+1); c2++) {
							if (board[r1][c1] == board[r3][c2] && board[r1][c2] == board[r2][c1] && board[r2][c2] == board[r3][c1]) {
								BitSet bs = new BitSet();
								bs.set(9*r1+c1);
								bs.set(9*r3+c2);
								bs.set(9*r1+c2);
								bs.set(9*r2+c1);
								bs.set(9*r2+c2);
								bs.set(9*r3+c1);
								list.add(bs);
							}
							// check transpose
							if (board[c1][r1] == board[c2][r3] && board[c2][r1] == board[c1][r2] && board[c2][r2] == board[c1][r3]) {
								BitSet bs = new BitSet();
								bs.set(9*c1+r1);
								bs.set(9*c2+r3);
								bs.set(9*c2+r1);
								bs.set(9*c1+r2);
								bs.set(9*c2+r2);
								bs.set(9*c1+r3);
								list.add(bs);
							}
						}
					}
				}
			}
		}
		//System.out.println("done");

		// Type 2
		/*
		 * AB<br>
		 * ---<br>
		 * B A<br>
		 * ---<br>
		 *  AB<br>
		 */
		//System.out.print("Type 2...");
		for (int r1 = 0; r1 < 3; r1++) {
			for (int r2 = 3; r2 < 6; r2++) {
				for (int r3 = 6; r3 < 9; r3++) {
					for (int c1 = 0; c1 < 9; c1++) {
						for (int c2 = 3*(c1/3); c2 < 3*(c1/3+1); c2++) {
							if (c2 == c1)
								continue;
							for (int c3 = 3*(c1/3); c3 < 3*(c1/3+1); c3++) {
								if (c3 == c2 || c3 == c1)
									continue;
								if (board[r1][c1] == board[r2][c3] && board[r1][c1] == board[r3][c2] && board[r1][c2] == board[r2][c1] && board[r1][c2] == board[r3][c3]) {
									BitSet bs = new BitSet();
									bs.set(9*r1+c1);
									bs.set(9*r2+c3);
									bs.set(9*r3+c2);
									bs.set(9*r1+c2);
									bs.set(9*r2+c1);
									bs.set(9*r3+c3);
									list.add(bs);
								}
								// check transpose
								if (board[c1][r1] == board[c3][r2] && board[c1][r1] == board[c2][r3] && board[c2][r1] == board[c1][r2] && board[c2][r1] == board[c3][r3]) {
									BitSet bs = new BitSet();
									bs.set(9*c1+r1);
									bs.set(9*c3+r2);
									bs.set(9*c2+r3);
									bs.set(9*c2+r1);
									bs.set(9*c1+r2);
									bs.set(9*c3+r3);
									list.add(bs);
								}
							}
						}
					}
				}
			}
		}
		//System.out.println("done");

		// Type 3
		/*
		 * ABC<br>
		 * ---<br>
		 * BCA<br>
		 */
		//System.out.print("Type 3...");
		for (int r1 = 0; r1 < 6; r1++) {
			for (int r2 = 3*(r1/3+1); r2 < 9; r2++) {
				for (int c = 0; c < 9; c+=3) {
					if (board[r1][c+0] == board[r2][c+2] && board[r1][c+1] == board[r2][c+0] && board[r1][c+2] == board[r2][c+1]) {
						BitSet bs = new BitSet();
						bs.set(9*r1+c+0);
						bs.set(9*r1+c+1);
						bs.set(9*r1+c+2);
						bs.set(9*r2+c+0);
						bs.set(9*r2+c+1);
						bs.set(9*r2+c+2);
						list.add(bs);

					}
					// do transpose
					if (board[c+0][r1] == board[c+2][r2] && board[c+1][r1] == board[c+0][r2] && board[c+2][r1] == board[c+1][r2]) {
						BitSet bs = new BitSet();
						bs.set(9*(c+0)+r1);
						bs.set(9*(c+1)+r1);
						bs.set(9*(c+2)+r1);
						bs.set(9*(c+0)+r2);
						bs.set(9*(c+1)+r2);
						bs.set(9*(c+2)+r2);
						list.add(bs);
					}
				}
			}
		}
		//System.out.println("done");

		// Type 4
		/*
		 * AB |<br>
		 * ---+---<br>
		 * B  |A<br>
		 *  A |B<br>
		 */
		//System.out.print("Type 4...");
		for (int r1=0; r1 < 9; r1++) {
			for (int r2=0; r2 < 9; r2++) {
				if (r2/3 == r1/3)
					continue;
				for (int r3=3*(r2/3); r3 < 3*(r2/3+1); r3++) {
					if (r3 == r2)
						continue;
					for (int c1 = 0; c1 < 9; c1++) {
						for (int c2 = 3*(c1/3); c2 < 3*(c1/3+1); c2++) {
							if (c2 == c1) 
								continue;
							for (int c3 = 0; c3 < 9; c3++) {
								if (c3/3 == c1/3)
									continue;
								if (board[r1][c1] == board[r3][c2] && 
										board[r1][c1] == board[r2][c3] && 
										board[r1][c2] == board[r2][c1] &&
										board[r1][c2] == board[r3][c3]) {
									BitSet bs = new BitSet();
									bs.set(9*r1+c1);
									bs.set(9*r3+c2);
									bs.set(9*r2+c3);
									bs.set(9*r1+c2);
									bs.set(9*r2+c1);
									bs.set(9*r3+c3);
									list.add(bs);
								}
								// check the transpose
								if (board[c1][r1] == board[c2][r3] && 
										board[c1][r1] == board[c3][r2] && 
										board[c2][r1] == board[c1][r2] &&
										board[c2][r1] == board[c3][c3]) {
									BitSet bs = new BitSet();
									bs.set(9*c1+r1);
									bs.set(9*c2+r3);
									bs.set(9*c3+r2);
									bs.set(9*c2+r1);
									bs.set(9*c1+r2);
									bs.set(9*c3+c3);
									list.add(bs);
								}
							}
						}
					}
				}
			}
		}
		//System.out.println("done");

		return list;
	}

	private static boolean matches(int[][] board, int[][] pat) {
		// check for pattern match
		boolean[] v = new boolean[9];
		for (int i = 0; i < 9; i++)
			v[i] = false;
		for (int[] g : pat) {
			// verify that each cell in the group is the same
			int n = board[g[0]/10-1][g[0]%10-1];
			for (int coord : g) {
				int n2 = board[coord/10-1][coord%10-1];
				if (n2 != n) {
					return false;
				}
			}
			// verify that all groups are different values
			if (v[n-1])
				return false;
			v[n-1] = true;
		}
		return true;
	}

	/**
	 * swap rows r1 and r2 in pattern
	 * @param pattern
	 * @param r1
	 * @param r2
	 * @return true if the pattern changed
	 */
	private static boolean swapRows(int[][] pattern, int r1, int r2) {
		boolean f = false;
		for (int[] g : pattern)
			for (int i = 0; i < g.length; i++) {
				int r = g[i]/10;
				int c = g[i]%10;
				if (r == r1) {
					g[i] = r2*10+c;
					f = true;
				}
				else if (r == r2) {
					g[i] = r1*10+c;
					f = true;
				}
			}
		return f;
	}

	/**
	 * swaps band b1 and b2 in pattern
	 * @param pattern
	 * @param b1
	 * @param b2
	 * @return true if pattern changed
	 */
	private static boolean swapBands(int[][] pattern, int b1, int b2) {
		boolean f = false;
		for (int i = 0; i < 3; i++)
			f = swapRows(pattern,3*(b1-1)+i+1,3*(b2-1)+i+1) || f;
		return f;
	}

	/**
	 * swap columns c1 and c2 in pattern
	 * @param pattern
	 * @param c1
	 * @param c2
	 * @return true if pattern changed
	 */
	private static boolean swapColumns(int[][] pattern, int c1, int c2) {
		//System.out.println("swap columns " + c1 + " and " + c2);
		boolean f = false;
		for (int[] g : pattern)
			for (int i = 0; i < g.length; i++) {
				int r = g[i]/10;
				int c = g[i]%10;
				if (c == c1) {
					//System.out.print("g["+i+"] = " + g[i] + " --> ");
					g[i] = r*10+c2;
					f = true;
					//System.out.println(g[i]);
				}
				else if (c == c2) {
					//System.out.print("g["+i+"] = " + g[i] + " --> ");
					g[i] = r*10+c1;
					f = true;
					//System.out.println(g[i]);
				}
			}
		/*
		if (f) {
			for (int[] g : pattern) {
				System.out.print("{");
				for (int n : g)
					System.out.print(n + " ");
				System.out.print("}");
			}
			System.out.println();
		}
		 */
		return f;
	}

	/**
	 * swaps stacks s1 and s2 in pattern
	 * @param pattern
	 * @param s1
	 * @param s2
	 * @return true if pattern changed
	 */
	private static boolean swapStacks(int[][] pattern, int s1, int s2) {
		boolean f = false;
		for (int i = 0; i < 3; i++)
			f = swapColumns(pattern,3*(s1-1)+i+1,3*(s2-1)+i+1) || f;
		return f;
	}

	/**
	 * @param pattern
	 * @return transposed copy of pattern
	 */
	private static int[][] transpose(int[][] pattern) {
		int[][] t = new int[pattern.length][];
		for (int i = 0; i < t.length; i++) {
			t[i] = new int[pattern[i].length];
			for (int j = 0; j < t[i].length; j++) {
				int r = pattern[i][j]/10;
				int c = pattern[i][j]%10;
				t[i][j] = c*10+r;
			}
		}
		return t;
	}

	/**
	 * 
	 * @param covered - keeps track of which cells are covered by other sets
	 * @param pattern
	 * @return true if all clues in pattern are already covered
	 */
	/*
	private static boolean covered(boolean[] covered, int[][] pattern) {
		for (int[] g : pattern)
			for (int coord : g) 
				if (!covered[9*(coord/10-1) + (coord%10-1)])
					return false;
		return true;
	}
	 */

	/**
	 * find unavoidable sets of size 8+
	 * @param board
	 * @param covered - array keeping track of clues that are already covered by other sets
	 * @return unavoidable sets
	 */
	public static LinkedList<BitSet> usGeneral(int[][][] unavpat, int[][] board, boolean[] covered) {
		LinkedList<BitSet> list = new LinkedList<BitSet>();

		boolean f;
		for (int[][] pat : unavpat) {
			//int cnt = 0;
			f = true;
			// do for all permutations of rows
			for (int r123perm = 0; r123perm < 6; r123perm++) {
				//System.out.println("r123perm = " + r123perm);
				for (int r456perm = 0; r456perm < 6; r456perm++) {
					//System.out.println("r456perm = " + r456perm);
					for (int r789perm = 0; r789perm < 6; r789perm++) {
						//System.out.println("r789perm = " + r789perm);
						for (int bperm = 0; bperm < 6; bperm++) {
							//System.out.println("bperm = " + bperm);

							// do for all permutations of columns
							for (int c123perm = 0; c123perm < 6; c123perm++) {
								//System.out.println("c123perm = " + c123perm);
								for (int c456perm = 0; c456perm < 6; c456perm++) {
									//System.out.println("c456perm = " + c456perm);
									for (int c789perm = 0; c789perm < 6; c789perm++) {
										//System.out.println("c789perm = " + c789perm);
										for (int sperm = 0; sperm < 6; sperm++) {
											//System.out.println("sperm = " + sperm);

											//for (int[] g : pat) {
											//	System.out.print("{");
											//	for (int n : g)
											//		System.out.print(n + " ");
											//	System.out.print("}");
											//}
											//System.out.println();

											if (f) { // if pattern changed, check
												//cnt++;
												f = false;
												if (matches(board,pat)) {
													// pattern matches
													BitSet bs = new BitSet();
													for (int[] g : pat) {
														for (int coord : g) {
															bs.set(9*(coord/10-1) + (coord%10-1));
															covered[9*(coord/10-1) + (coord%10-1)] = true;
														}
													}
													list.add(bs);
												}

												//cnt++;
												//check transpose
												int[][] Tpat = transpose(pat);
												if (matches(board,Tpat)) {
													// pattern matches
													BitSet bs = new BitSet();
													for (int[] g : Tpat) {
														for (int coord : g) {
															bs.set(9*(coord/10-1) + (coord%10-1));
															covered[9*(coord/10-1) + (coord%10-1)] = true;
														}
													}
													list.add(bs);
												}
											} // else pattern did not change, so don't bother rechecking
											// swap stacks
											if (sperm % 2 == 0)
												f = swapStacks(pat,1,2) || f;
											else
												f = swapStacks(pat,1,3) || f;
										}
										//swapStacks(pat,2,3); // put it back right
										f = false;
										// swap columns in right stack
										if (c789perm % 2 == 0)
											f = swapColumns(pat,7,8) || f;
										else
											f = swapColumns(pat,7,9) || f;
									}
									//swapColumns(pat,8,9); // put it back right
									f = false;
									// swap columns in middle stack
									if (c456perm % 2 == 0)
										f = swapColumns(pat,4,5) || f;
									else
										f = swapColumns(pat,4,6) || f;
								}
								//swapColumns(pat,5,6); // put it back right
								f = false;
								// swap columns in left stack
								if (c123perm % 2 == 0)
									f = swapColumns(pat,1,2) || f;
								else
									f = swapColumns(pat,1,3) || f;
							}
							//swapColumns(pat,2,3); // put it back right
							f = false;
							// swap bands
							if (bperm % 2 == 0)
								f = swapBands(pat,1,2) || f;
							else
								f = swapBands(pat,1,3) || f;
						}
						//swapBands(pat,2,3); // put it back right
						f = false;
						// swap rows in bottom band
						if (r789perm % 2 == 0)
							f = swapRows(pat,7,8) || f;
						else
							f = swapRows(pat,7,9) || f;
					}
					//swapRows(pat,8,9); // put it back right
					//f = false;
					// swap rows in middle band
					if (r456perm % 2 == 0)
						f = swapRows(pat,4,5) || f;
					else
						f = swapRows(pat,4,6) || f;
				}
				//swapRows(pat,5,6); // put it back right
				f = false;
				// swap rows in top band
				if (r123perm % 2 == 0)
					f = swapRows(pat,1,2) || f;
				else
					f = swapRows(pat,1,3) || f;
			}
			//swapRows(pat,2,3); // put it back right
			f = false;
			//System.out.println(cnt + " checks");
			break;
		}
		return list;
	}

	/**
	 * brute force all unavoidable sets of size 12 or less
	 * @param board
	 */
	public static LinkedList<BitSet> us12ol(int[][] board) {
		LinkedList<BitSet> list = new LinkedList<BitSet>();

		for (int n = 4; n <= 12; n++) {
			System.out.println(n);
			int[] idx = new int[n];
			for (int i = 0; i < idx.length; i++)
				idx[i] = i;
			int[] cp = new int[n];
			while(idx[0] <= 81-n) {

				// take 'em out
				for (int i = 0; i < idx.length; i++) {
					cp[i] = board[idx[i]/9][idx[i]%9];
					board[idx[i]/9][idx[i]%9] = 0;
				}
				try{
					SudokuSolutionCounter.setLimit(2);
				// put 'em back
				for (int i = 0; i < idx.length; i++)
					board[idx[i]/9][idx[i]%9] = cp[i];
				boolean f = false;
				try {
					int nc = SudokuSolutionCounter.countSolutions(board);
					if (nc > 1) {
						// unavoidable set
						f = true;
					} // else avoidable
				} catch (Exception e) {
					// more than 1 solution found
					// unavoidable set
					f = true;
				}
				if (f) {
					BitSet bs = new BitSet();
					for (int i = 0; i < idx.length; i++)
						bs.set(idx[i]);
					list.add(bs);
					System.out.println(bs);
				}
				} catch (Exception e) {
				}

				int j = n-1;
				idx[j]++;
				while (j > 0 && idx[j] == 81-n+j+1) {
					idx[j-1]++;
					for (int k = j; k < n; k++)
						idx[k]=idx[k-1]+1;
					j--;
				}
			}

		}

		return list;
	}

	/**
	 * remove duplicate unavoidable sets
	 * @param list- list to filter 
	 * @return unique sets
	 */
	public static void eliminateDuplicates(LinkedList<BitSet> list) {
		LinkedList<BitSet> list2 = new LinkedList<BitSet>();
		int i1 = 0;
		Iterator<BitSet> iter1 = list.iterator();
		while(iter1.hasNext()) {
			BitSet bs1 = iter1.next();
			int i2 = i1+1;	
			Iterator<BitSet> iter2 = list.iterator();
			while (i2 > 0) {
				iter2.next();
				i2--;
			}
			boolean f = true;
			while (iter2.hasNext()) {
				BitSet bs2 = iter2.next();
				if (bs1.equals(bs2)) {
					//System.out.println(bs1 + " = " + bs2);
					f = false;
					break;
				}
			}
			if (f)
				list2.add(bs1);
			i1++;
		}

		list.clear();
		list.addAll(list2);
	}

	public static BitSet unavoidableClues(LinkedList<BitSet> unavoidableSets) {
		Random random = new Random(System.currentTimeMillis());
		BitSet clues = new BitSet();
		LinkedList<BitSet> us = new LinkedList<BitSet>(unavoidableSets);
		while (!us.isEmpty()) {
			//System.out.println(us.size());
			// find max
			int[] f = new int[81];
			for (int i = 0; i < 81; i++)
				f[i] = 0;
			LinkedList<Integer> i_max = new LinkedList<Integer>();
			for (int i = 0; i < 81; i++) {
				for (BitSet bs : us) {
					//System.out.println(bs);
					f[i] += (bs.get(i) ? 1 : 0);
				}
				if (i_max.isEmpty() || f[i] == f[i_max.peek()])
					i_max.add(i);
				if (f[i] > f[i_max.peek()]) {
					i_max.clear();
					i_max.add(i);
				}
			}

			//int clue = i_max.peek();
			int clue = i_max.get(random.nextInt(i_max.size()));
			clues.set(clue);
			//System.out.println(clue);

			// remove all sets including the clue
			LinkedList<BitSet> list = new LinkedList<BitSet>();
			for (BitSet bs : us)
				if (!bs.get(clue))
					list.add(bs);
			us.clear();
			us.addAll(list);
		}

		return clues;
	}

	public static BitSet unavoidableClues(int[][] board) {
		LinkedList<BitSet> us = unavoidableSets(board);
		//		System.out.println(us.size() + " unavoidable sets found");
		//for (BitSet bs : us)
		//	System.out.println(bs);
		//		double m = 0.0;
		//		for (BitSet bs : us)
		//			m += bs.cardinality();
		//		m /= us.size();
		//		System.out.println("mean size = " + m);	
		return unavoidableClues(us);
	}

	public static void main(String[] args) {
		SudokuBoard b;
		try {
			b = new SudokuBoard("639241785284765193517983624123857946796432851458619237342178569861594372975326418");
		
		//RandomSudoku b = new RandomSudoku();
		//SudokuBoard.print(b.board());

		//LinkedList<BitSet> us = unavoidableSets(b.board());

		// just the 4s
		//LinkedList<BitSet> us = us4(b.board());

		// just the 6s
		//LinkedList<BitSet> us = us6(b.board());

		// just the 9+s
		//LinkedList<BitSet> us = us9plus(b.board());

		// so god-damned slow
		//LinkedList<BitSet> us = us12ol(rs.board());


		//System.out.println(us.size() + " unavoidable sets found");

		//RandomSudoku b = new RandomSudoku();
		long t1,t2;
//		int[] f = new int[81];
//		for (int i = 0; i < 81; i++)
//			f[i] = 0;
		for (int i = 0; i < 10; i++) {
			t1 = System.nanoTime();
			BitSet clues = unavoidableClues(b.board());
//			for (int j = 0; j < 81; j++)
//				if (clues.get(j))
//					f[j]++;
			t2 = System.nanoTime();
			System.out.printf("%.4f\n", (t2-t1)/1000000.0);
			System.out.println(clues.cardinality() + " unavoidable clues: " + clues);
		}

//		int k = 0;
//		int[] c = new int[81];
//		for (int i = 0; i < 81; i++) {
//			int max_j = 0;
//			for (int j = 0; j < 81; j++)
//				if (f[max_j] < f[j])
//					max_j = j;
//			//System.out.println(max_j + " " + f[max_j]);
//			c[k++] = max_j;
//			f[max_j] = -1;
//		}
//
//		int[][] puzzle = new int[9][9];
//		for (k = 0; k < 17; k++) {
//			int row = c[k]/9;
//			int col = c[k]%9;
//			puzzle[row][col] = b.board(row, col);
//		}
//		SudokuSolutionCounter ssc;
//		while (true){
//			ssc = new SudokuSolutionCounter(puzzle,2);
//			try {
//				int ns = ssc.countSolutions();
//				if (ns == 1)
//					break;
//			} catch (Exception e) {
//				int row = c[k]/9;
//				int col = c[k]%9;
//				puzzle[row][col] = b.board(row, col);
//				k++;
//				continue;
//			}
//		}
//		
//		System.out.println(k + " clues");
//		SudokuBoard.print(puzzle);
		} catch (Exception e) {
		}
	}
}
