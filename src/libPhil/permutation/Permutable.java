package libPhil.permutation;

import libPhil.factoradic.Factoradic;

public interface Permutable<T> {

	/**
	 * generate the permutation specified by the given Factoradic
	 * @param f - Factoradic object specifying a permutation
	 * @return permuted array
	 */
	public T[] permute(Factoradic f);
}
