package libPhil.permutation;

import java.util.Arrays;
import java.util.LinkedList;

import libPhil.factoradic.Factoradic;

public class PermutableArray<T> implements Permutable<T> {
	private T[] array; 	// original array
	private T[] pArray;	// permuted array
	
	/**
	 * make a new Permutation object with the given array 
	 * @param a - array of objects to permute
	 */
	public PermutableArray(T[] a) {
		this.array = Arrays.copyOf(a, a.length);
		pArray = Arrays.copyOf(array, array.length);
	}
	
	/**
	 * generate the permutation specified by the given Factoradic
	 * @param f - Factoradic object specifying a permutation
	 * @return permuted array
	 */
	public T[] permute(Factoradic f) {
		//if (f.length() != array.length) {
		//	System.err.println("(Warning) Permutation.permute(Factoradic): lengths differ.");
		//}
		LinkedList<T> list = new LinkedList<T>();
		for (T t : array)
			list.add(t);
		for (int i = 0; i < array.length; i++)
			pArray[i] = list.remove(f.getDigit(array.length-i-1));
		return pArray;
	}
	
	/**
	 * 
	 * @return copy of the original array
	 */
	public T[] array() {
		return Arrays.copyOf(array, array.length);
	}
	
	/**
	 * 
	 * @return copy of the permuted array
	 */
	public T[] pArray() {
		return Arrays.copyOf(pArray, pArray.length);
	}
	
	
	/**
	 * set the value of the array
	 * @param newArray - new array to make permutable
	 */
	/*
	public void array(T[] newArray) {
		this.array = Arrays.copyOf(array, array.length);
		this.pArray = Arrays.copyOf(array, array.length);
	}
	*/
}
