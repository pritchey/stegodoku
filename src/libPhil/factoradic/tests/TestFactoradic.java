package libPhil.factoradic.tests;

import static org.junit.Assert.*;
import libPhil.factoradic.Factoradic;
import libPhil.factoradic.FactoradicException;

import org.junit.Test;

public class TestFactoradic {

	@Test
	public void testFactoradic() {
		Factoradic f = new Factoradic();
		assertArrayEquals(new int[]{0},f.fNum());
		
		boolean b = false;
		try {
			f = new Factoradic(new int[]{0,1,2,3,4,5});
		} catch (FactoradicException e) {
			b = true;
		}
		assertTrue(b);
		
		try {
			f = new Factoradic(new int[]{5,4,3,2,1,0});
		} catch (FactoradicException e) {
			fail();
		}
	}
	
	@Test
	public void testGetDigit() {
		Factoradic f = new Factoradic();
		try {
			f = new Factoradic(new int[]{5,4,3,2,1,0});
		} catch (FactoradicException e) {
			fail("constructor failed.");
		}
		
		for (int i = 0 ; i < 6; i++)
			assertEquals(i,f.getDigit(i));
		assertEquals(0,f.getDigit(6));
	}
	
	@Test
	public void testExtend() {
		Factoradic f = new Factoradic();
		try {
			f = new Factoradic(new int[]{5,4,3,2,1,0});
		} catch (FactoradicException e) {
			fail("constructor failed.");
		}
		
		f.extend(10);
		for (int i = 0 ; i < 6; i++)
			assertEquals(i,f.getDigit(i));
		for (int i = 6 ; i < 10; i++)
		assertEquals(0,f.getDigit(i));
	}

	@Test
	public void testLength() {
		Factoradic f = new Factoradic();
		try {
			f = new Factoradic(new int[]{5,4,3,2,1,0});
		} catch (FactoradicException e) {
			fail("constructor failed.");
		}
		
		assertEquals(6,f.length());
		f.extend(10);
		assertEquals(10,f.length());
	}
	
	@Test
	public void testAdd() {
		Factoradic f = new Factoradic();
		Factoradic f1 = Factoradic.valueOf(1);
		
		for (int i = 1; i <= 100; i++) {
			f = Factoradic.add(f, f1);
			assertArrayEquals(Factoradic.valueOf(i).fNum(),f.fNum());
		}
		
		f = new Factoradic();
		f1 = Factoradic.valueOf(2);
		for (int i = 1; i <= 100; i++) {
			f = Factoradic.add(f, f1);
			assertArrayEquals(Factoradic.valueOf(2*i).fNum(),f.fNum());
		}
		
		f = new Factoradic();
		f1 = Factoradic.valueOf(3);
		for (int i = 1; i <= 100; i++) {
			f = Factoradic.add(f, f1);
			assertArrayEquals(Factoradic.valueOf(3*i).fNum(),f.fNum());
		}
	}
	
	@Test
	public void testEquals() {
		Factoradic f = new Factoradic();
		Factoradic f1 = Factoradic.valueOf(1);
		
		for (int i = 1; i <= 100; i++) {
			f = Factoradic.add(f, f1);
			Factoradic ref = Factoradic.valueOf(i);
			assertTrue(ref.equals(f));
			assertTrue(f.equals(ref));
		}
	}
	
	@Test
	public void testToNumber() {
		for (int i = 1; i <= 100; i++) {
			Factoradic f = Factoradic.valueOf(i);
			assertEquals(i,f.toNumber());
		}
	}
}
