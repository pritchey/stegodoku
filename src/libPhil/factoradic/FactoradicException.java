package libPhil.factoradic;

public class FactoradicException extends Exception {
	private static final long serialVersionUID = 1L;

	public FactoradicException(String mssg) {
		super(mssg);
	}
}
