package libPhil.factoradic;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.Stack;

public class Factoradic implements Comparable<Factoradic> {
	private int[] fNum;
	
	public static final Factoradic ONE = Factoradic.valueOf(1);
	
	// -----BEGIN CONSTRUCTORS ----- //

	/**
	 * default constructor creates the factoradic 0
	 */
	public Factoradic() {
		this.fNum = new int[1];
		this.fNum[0] = 0;
	}
	
	/**
	 * create a new Factoradic object from given factoradic number
	 * @param s - factoradic number
	 * @throws FactoradicException if given number is not factoradic
	 */
	public Factoradic(int[] f) throws FactoradicException {
		if (!isFactoradic(f))
			throw new FactoradicException("Not a valid factoradic number");
		this.fNum = new int[f.length];
		System.arraycopy(f, 0, this.fNum, 0, f.length);
	}

	/**
	 * copy constructor for Factoradic objects
	 * @param f
	 */
	public Factoradic(Factoradic f) {
		this.fNum = f.fNum();
	}
	
	// -----END CONSTRUCTORS ----- //

	// -----BEGIN NON-STATIC FUNCTIONS ----- //
	
	/**
	 * 
	 * @return a copy of factoradic number fNum
	 */
	public int[] fNum() {
		int[] f = new int[fNum.length];
		System.arraycopy(this.fNum, 0, f, 0, this.fNum.length);
		return f;
	}

	/**
	 * return the k-th most significant digit 
	 * @param k
	 * @return k!-th place of the number
	 * @throws FactoradicException 
	 */
	public int getDigit(int k) {
		if (k < 0) {
			System.err.println("(Error) Factoradic.getDigit(int): k must be >= 0");
			k = 0;
		}
		if (k >= fNum.length)
			return 0;
		return fNum[fNum.length-1-k];
	}
	
	/**
	 * pad the RHS with zeros up to length
	 * @param len - desired length of number
	 * @return this factoradic with length len
	 */
	public void extend(int len) {
		int[] f = new int[len];
		for (int i = 0; i < len; i++)
			f[len-1-i] = this.getDigit(i);
		this.fNum = f;
	}
	
	/**
	 * trim leading zeros
	 */
	public void trim() {
		int len = fNum.length;
		for (int i = 0 ; i < fNum.length; i++)
			if (fNum[i] == 0)
				len--;
			else
				break;
		int[] f = new int[len];
		for (int i = 0; i < len; i++)
			f[len-i-1] = fNum[fNum.length-i-1];
		this.fNum = f;
	}
	
	/**
	 * 
	 * @return length of factoradic number in digits
	 */
	public int length() {
		return fNum.length;
	}
	
	/**
	 * add 1 to this factoradic
	 */
	public void increment() {
		this.fNum = add(this,ONE).fNum();
	}
	
	/**
	 * @return decimal representation of this factoradic number
	 */
	public BigInteger toNumber() {
		int len = this.length();
		BigInteger num = BigInteger.ZERO;
		while (len > 0) {
			num = num.multiply(BigInteger.valueOf(len));
			num = num.add(BigInteger.valueOf(this.getDigit(len-1)));
			len--;
		}
		return num;
	}
		
	
	@Override
	public String toString() {
		String s = "[";
		s += fNum[0];
		for (int i = 1; i < fNum.length; i++)
			s += "," + fNum[i];
		s += "]";
		return s;
	}

	@Override
	/**
	 * compare this factoradic to f
	 * @param f - factoradic with to which to compare
	 * @return -1 if this < f, 0 if this.equals(f), 1 this > f
	 */
	public int compareTo(Factoradic f) {
		int diff = 0;
		long v = 1;
		for (int k = 0; k < Math.max(this.length(), f.length()); k++) {
			diff += (this.getDigit(k) - f.getDigit(k))*v;
			v *= k+1;
		}
		return (int)Math.signum(diff);
	}
	
	public boolean equals(Factoradic f) {
		for (int k = 0; k < Math.max(this.length(), f.length()); k++)
			if (this.getDigit(k) != f.getDigit(k)) {
				System.out.println("mismatch at k = " + k);
				return false;
			}
		return true;
	}
	
	// ----- END NON-STATIC FUNCTIONS ----- //
	
	// ----- BEGIN STATIC FUNCTIONS ----- //

	/**
	 * determine if a number is a valid factoradic number
	 * @param f - number to test
	 * @return true if number is valid factoradic, false otherwise
	 */
	public static boolean isFactoradic(int[] f) {
		for (int i = f.length-1; i >= 0; i--) {
			if (f[i] > f.length-i-1)
				return false;
		}
		return true;
	}
	
	/**
	 * convert given BigInteger number to factoradic base
	 * @param b BigInteger number to convert
	 * @return a new Factoradic object representing the converted number
	 */
	public static Factoradic valueOf(BigInteger b) {
		Stack<Integer> s = new Stack<Integer>();
		int k = 1;
		while (b.compareTo(BigInteger.ZERO) > 0) {
			BigInteger[] ret = b.divideAndRemainder(BigInteger.valueOf(k));
			b = ret[0]; 				// b = b/k;
			s.push(ret[1].intValue());	// s.push(b%k);
			k++;
		}
		int[] f = new int[s.size()];
		int i = 0;
		while (!s.isEmpty())
			f[i++] = s.pop();
		try {
			return new Factoradic(f);
		} catch (FactoradicException e) {
			return null;
		}
	}
	
	/**
	 * convert given base-10 number to factoradic base
	 * @param l base-10 number to convert
	 * @return a new Factoradic object representing the converted number
	 */
	public static Factoradic valueOf(long l) {
		return valueOf(BigInteger.valueOf(l));
	}
	
	/**
	 * convert given base-10 number to factoradic base
	 * @param s base-10 number to convert
	 * @return a new Factoradic object representing the converted number
	 */
	public static Factoradic valueOf(String s) {
		return valueOf(Long.parseLong(s));
	}
	
	/**
	 * convert given base-10 number to factoradic base
	 * @param i base-10 number to convert
	 * @return a new Factoradic object representing the converted number
	 */
	public static Factoradic valueOf(int i) {
		return valueOf((long)i);
	}
	
	/**
	 * add two factoradic numbers together
	 * @param f1
	 * @param f2
	 * @return f1 + f2
	 */
	public static Factoradic add(Factoradic f1, Factoradic f2) {
		int[] f = new int[Math.max(f1.length(),f2.length())+1];
		int carry = 0;
		for (int k = 0; k < f.length; k++) {
			int d1 = f1.getDigit(k);
			int d2 = f2.getDigit(k);
			f[f.length-k-1] = ((d1+d2+carry) % (k+1));
			carry = (d1+d2+carry)/(k+1);
		}
		try {
			Factoradic F = new Factoradic(f);
			F.trim();
			return F;
		} catch (FactoradicException e) {
			return null;
		}
	}
	/**
	 * convert permutation to factoradic
	 * @param a - array of object IDs (1-n)
	 * @return factoradic number representing given permutation
	 */
	public static Factoradic permutationToFactoradic(int[] a) {
		LinkedList<Integer> l = new LinkedList<Integer>();
		for (int i=1; i <= a.length; i++)
			l.add(i);
		int[] f = new int[a.length];
		int idx = -1;
		for (int i : a) {
			f[++idx] = l.indexOf(i);
			l.remove(f[idx]);
		}
		Factoradic fact;
		try {
			fact =  new Factoradic(f);
		} catch (FactoradicException e) {
			fact = null;
		}
		return fact;
	}
	
	// ----- END STATIC FUNCTIONS ----- //
}
