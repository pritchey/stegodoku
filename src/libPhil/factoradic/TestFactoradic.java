package libPhil.factoradic;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Random;

import org.junit.Test;

public class TestFactoradic {

	@Test
	public void testPermutationToFactoradic() {
		int[] a = new int[]{1,2,3,4,5,6,7,8,9};
		Factoradic f = Factoradic.permutationToFactoradic(a);
		assertArrayEquals(new int[]{0,0,0,0,0,0,0,0,0},f.fNum());

		a = new int[]{8,6,7,5,3,1,9,2,4};
		f = Factoradic.permutationToFactoradic(a);
		assertArrayEquals(new int[]{7,5,5,4,2,0,2,0,0},f.fNum());
	}

	@Test
	public void testValueOfBigInteger() {
		Random random = new Random(System.currentTimeMillis());
		for (int j = 0; j < 10; j++) {
			double len = 0;
			BigInteger b = BigInteger.ONE;
			for (int i = 1; i <= 52; i++) {
				if (random.nextBoolean()) {
					b = b.multiply(BigInteger.valueOf(i));
					len += Math.log(i)/Math.log(2);
				}
			}
			Factoradic f = Factoradic.valueOf(b);
			BigInteger b2 = f.toNumber();
			assertEquals(b,b2);
			assertEquals((int)(len+1),b2.toString(2).length());
		}
	}

}
