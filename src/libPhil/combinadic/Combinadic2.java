package libPhil.combinadic;

import java.math.BigInteger;

public class Combinadic2 {
	private static final long[][] lut;
	
	static {
		lut = new long[67][67];
		for (int i = 0; i <= 66; i++)
			for (int j = 0; j <= 66; j++)
				if (i==j)
					lut[i][j] = 1;
				else if(j > i)
					lut[i][j] = 0;
				else
					lut[i][j] = -1;
	}

	/**
	 * convert i to a k-combination. recursive.
	 * @param i
	 * @return i-th k-combination
	 */
	public static int[] toCombination2(long i, int k) {
		if (i == 0) {
			int[] ret = new int[k];
			for (int j = 0; j < ret.length; j++)
				ret[j] = --k;
			return ret;
		}
		
		int n = k;
		while (combin(n,k) <= i)
			n++;
		n--;
		int[] c = toCombination2(i - combin(n,k),k-1);
		int[] ret = new int[1+c.length];
		ret[0] = n;
		for (int j = 1; j < ret.length; j++)
			ret[j] = c[j-1];
		return ret;
	}
	
	/**
	 * convert i to a k-combination. iterative.
	 * @param i
	 * @return i-th k-combination
	 */
	public static int[] toCombination(long n, int k) {
		int[] c = new int[k];
		int i = 0;
		while (k >= 1) {
			c[i] = 0;
			while (combin(c[i],k) <= n)
				c[i]++;
			c[i]--;
			n -= combin(c[i],k);
			k--;
			i++;
		}
		return c;
	}
	
	// TODO convert back to a number from a combination
	/**
	 * convert given combination to a number
	 * @param combin
	 * @return num - index of given combination
	 */
	public static BigInteger toNumber(int[] combin) {
		BigInteger num = BigInteger.ZERO;
		int k = 1;
		for (int i = combin.length-1; i >= 0; i--)
			num = num.add(BigInteger.valueOf(combin(combin[i],k++)));
		return num;
	}
	
	/**
	 * convert combination to readable string
	 * @param combin
	 */
	public static String toString(int[] combin) {
		String str = "";
		
		str += "{" + combin[0];
		for (int i = 1; i < combin.length; i++)
			str += "," + combin[i];
		str += "}";
		
		return str;
	}
	
	/**
	 * compute n choose k
	 */
	public static long combin(int n, int k) {
		if (k > n)
			return 0;
		if (k == 0 || k == n)
			return 1;
		if (n <= 66 && k <= 66) {
			if (lut[n][k] < 0)
				lut[n][k] = combin(n-1,k-1) + combin(n-1,k);
			return lut[n][k];
		}
		return combin(n-1,k-1) + combin(n-1,k);
	}
}
