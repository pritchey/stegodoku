package libPhil.combinadic;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCombinadic {
	
	@Test
	public void testCombin() {
		assertEquals(1,Combinadic.combin(0, 0).intValue());
		assertEquals(0,Combinadic.combin(0, 1).intValue());
		
		assertEquals(1,Combinadic.combin(1, 0).intValue());
		assertEquals(1,Combinadic.combin(1, 1).intValue());
		assertEquals(0,Combinadic.combin(1, 2).intValue());
		
		assertEquals(1,Combinadic.combin(2, 0).intValue());
		assertEquals(2,Combinadic.combin(2, 1).intValue());
		assertEquals(1,Combinadic.combin(2, 2).intValue());
		assertEquals(0,Combinadic.combin(2, 3).intValue());
		
		assertEquals(1,Combinadic.combin(3, 0).intValue());
		assertEquals(3,Combinadic.combin(3, 1).intValue());
		assertEquals(3,Combinadic.combin(3, 2).intValue());
		assertEquals(1,Combinadic.combin(3, 3).intValue());
		assertEquals(0,Combinadic.combin(3, 4).intValue());
	}
	
	@Test
	public void testToCombination() {
		assertArrayEquals(new int[]{5,4,3,0},Combinadic.toCombination(12, 4));
		assertArrayEquals(new int[]{8,6,3,1,0},Combinadic.toCombination(72, 5));
		assertArrayEquals(new int[]{5,4,3,2,1,0},Combinadic.toCombination(0, 6));
	}
	
	@Test
	public void testToNumber() {
		assertEquals(12,Combinadic.toNumber(new int[]{5,4,3,0}).intValue());
		assertEquals(72,Combinadic.toNumber(new int[]{8,6,3,1,0}).intValue());
	}
}
