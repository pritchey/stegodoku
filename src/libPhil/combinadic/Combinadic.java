package libPhil.combinadic;

import java.math.BigInteger;
import java.util.Hashtable;

public class Combinadic {
	private static final Hashtable<Key,BigInteger> lut = new Hashtable<Key,BigInteger>();
	
	static class Key {
		public int n;
		public int k;
		
		public Key(int n, int k) {
			this.n = n;
			this.k = k;
		}
		
		public boolean equals(Object o) {
			return (o instanceof Key && ((Key)o).k == this.k && ((Key)o).n == this.n);
		}
		
		public int hashCode() {
			return (n-2)*(n-1)/2 + k - 1;
		}
	}
	
	/**
	 * convert i to a k-combination. iterative.
	 * @param i
	 * @return i-th k-combination
	 */
	public static int[] toCombination(BigInteger n, int k) {
		int[] c = new int[k];
		int i = 0;
		while (k >= 1) {
			c[i] = 0;
			while (combin(c[i],k).compareTo(n) <= 0)
				c[i]++;
			c[i]--;
			n = n.subtract(combin(c[i],k));
			k--;
			i++;
		}
		return c;
	}
	
	public static int[] toCombination(long n, int k) {
		return toCombination(BigInteger.valueOf(n),k);
	}
	
	/**
	 * convert given combination to a number
	 * @param combin
	 * @return index of given combination
	 */
	public static BigInteger toNumber(int[] combin) {
		BigInteger num = BigInteger.ZERO;
		int k = 1;
		for (int i = combin.length-1; i >= 0; i--)
			num = num.add(combin(combin[i],k++));
		return num;
	}
	
	/**
	 * convert combination to readable string
	 * @param combin
	 */
	public static String toString(int[] combin) {
		String str = "";
		
		str += "{" + combin[0];
		for (int i = 1; i < combin.length; i++)
			str += "," + combin[i];
		str += "}";
		
		return str;
	}
	
	/**
	 * compute n choose k
	 */
	public static BigInteger combin(int n, int k) {
		if (k > n || k < 0)
			return BigInteger.ZERO;
		if (k == 0 || k == n)
			return BigInteger.ONE;
		if (k > n-k)
			k = n-k;
		Key key = new Key(n,k);
		if (!lut.containsKey(key)) {
			lut.put(key, combin(n-1,k-1).add(combin(n-1,k)));
		}
		return lut.get(key);
	}
}
