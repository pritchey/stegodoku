package stegodoku;

import java.util.LinkedList;
import java.util.Random;

import libPhil.sudoku.RandomSudoku;
import libPhil.sudoku.SudokuBoard;

public class CompressionTest {

	public static void main(String[] args) {
		RandomSudoku rs;
		Random random = new Random();
		LinkedList<Integer> cells;
		int[][] board;
		int[][] lengths = new int[82][2];
		for (int i = 0; i < 82; i++) {
			lengths[i][0] = 0; // count
			lengths[i][1] = 0; // sum
		}

		for (int nb2 = 0; nb2 < 33; nb2++) {
			for (int i = 0; i < 150; i++) {
				rs = new RandomSudoku();
				cells = new LinkedList<Integer>();
				for (int j = 0; j < 41; j++) {
					cells.add(j);
				}
				board = rs.board();
				int nb = 0;
				for (int j = 0; j < nb2; j++) {
					int k = cells.remove(random.nextInt(cells.size()));
					int row = k/9;
					int col = k%9;
					board[row][col] = 0;
					board[8-row][8-col] = 0;
					nb += k == 40 ? 1 : 2;
				}
				String c = SudokuBoard.compress(board);
				lengths[81-nb][0]++;
				lengths[81-nb][1] += c.length();
			}
		}
		for (int i = 17; i < 82; i++) {
			System.out.println(i + "\t" + (double)lengths[i][1]/lengths[i][0]);
		}
	}

}
