package stegodoku;

import java.util.Random;

import libPhil.sudoku.RandomSudoku;
import libPhil.sudoku.SudokuBoard;
import libPhil.sudoku.SudokuPuzzleMaker;
import libPhil.sudoku.SudokuSolver;


/**
 * Provides top-level interface for accessing the StegoDoku system
 * @author Philip C. Ritchey
 *
 */
public class StegoDokuMain {
	public static final byte versionMajor = 0;
	public static final byte versionMinor = 5;
	public static final int versionDate = 20160628;

	private static boolean useHuffman = false;
	private static boolean usePermutations = false;
	private static boolean keySet = false;
	private static long key = 0;
	private static byte paddingMode = 0;
	private static boolean runTest = false;
	private static int N = 0;
	private static boolean random = false;
	private static String secret = "";
	private static String path = "";
	private static boolean encode = false;
	private static boolean decode = false;
	private static String board = "";
	private static boolean makePuzzle = false;
	private static boolean solvePuzzle = false;
	private static boolean debug = false;

	public static void printVersion() {
		System.out.println("StegoDoku, by Philip C. Ritchey");
		System.out.println("version " + versionMajor + "." + versionMinor + "." + versionDate);
	}

	public static void printUsage() {
		System.out.println("StegDoku [options]");
		System.out.println("Option\t\tMeaning");
		System.out.println("--help\t\tprint information about usage.");
		System.out.println("--version\tprint version number.");
		
		System.out.println("-e <bitstring>\tgenerate StegoDoku");
		System.out.println("-d <board>\tdecode board.");
		
		System.out.println("-h\t\tuse Huffman coding.");
		
		System.out.println("-p\t\tuse permutations. if -k is not set, a key will be randomly chosen.");
		System.out.println("-k <key>\tkey value to use.");
		
		System.out.println("-P <mode>\tset the padding mode.\n"
				+ "\t\t  0 - pad with 0.\n"
				+ "\t\t  1 - pad with 1.\n"
				+ "\t\t  2 - pad with random bits (does not use -k value).");
				
		System.out.println("-r\t\tgenerate a random authentic sudoku board.");
		System.out.println("-s <board>\tsolve puzzle.");
		System.out.println("-D\t\tsolver debug mode (verbose output).");
		System.out.println("-z <board>\tmakes a sudoku puzzle from the given board.");
		
		System.out.println("-t <N>\t\trun test, generate N boards. used for data collection.");
		System.out.println("-o <path>\tspecify output path for testing.");
		
	}

	public static void parseOptions(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("--help")) {
				printUsage();
				System.exit(0);
			}
			else if (args[i].equals("--version")) {
				printVersion();
				System.exit(0);
			}
			else if (args[i].equals("-e")) {
				secret = args[++i];
				encode = true;
			}
			else if (args[i].equals("-d")) {
				board = args[++i];
				decode = true;
			}
			else if (args[i].equals("-h"))
				useHuffman = true;
			else if (args[i].equals("-p"))
				usePermutations = true;
			else if (args[i].equals("-o"))
				path = args[++i];
			else if (args[i].equals("-k")) {
				try {
					key = Long.parseLong(args[i+1]);
					keySet = true;
					i++;
				} catch (NumberFormatException e) {
					System.err.println("Error: key value ("+args[i+1]+") invalid.  key must be in [-2^63 , 2^63 - 1]");
					key = 0;
				}
			}
			else if (args[i].equals("-P")) {
				try {
					paddingMode = Byte.parseByte(args[i+1]);
					i++;
					if (paddingMode < 0 || paddingMode > 2) {
						paddingMode = 0;
						throw new NumberFormatException("padding mode out of bounds");
					}
				} catch (NumberFormatException e) {
					System.err.println("Error: padding mode ("+args[i+1]+") invalid.  mode must be 0, 1, or 2.");
					paddingMode = 0;
				}
			}
			else if (args[i].equals("-r"))
				random = true;
			else if (args[i].equals("-t")) {
				runTest = true;
				try {
					N = Integer.parseInt(args[i+1]);
					i++;
					if (N <= 0) {
						N = 0;
						throw new NumberFormatException("N out of bounds");
					}
				} catch (NumberFormatException e) {
					System.err.println("Error: number of test samples ("+args[i+1]+") invalid.  N must be > 0.");
					runTest = false;
				}
			} else if (args[i].equals("-z")) {
				board = args[++i];
				makePuzzle = true;
			} else if (args[i].equals("-s")) {
				board = args[++i];
				solvePuzzle = true;
			} else if (args[i].equals("-D")) {
				debug = true;
			}
		}
		if (usePermutations && !keySet) {
			key = new Random().nextLong();
			keySet = true;
			System.out.println("random key chosen: " + key);
		}
	}
	
	public static void runTest() {
		if (random)
			RandomSudoku.runTest(N, path);
		else
			if (useHuffman)
				if (usePermutations)
					StegoDokuV5.runTest(N, path);
				else
					StegoDokuV3.runTest(N, path);
			else
				if (usePermutations)
					StegoDokuV4.runTest(N, path);
				else
					StegoDokuV2.runTest(N, path);
	}
	
	public static void decode() {
		int[][] b;
		try {
			b = new SudokuBoard(board).board();
		String d = "";
		if (useHuffman)
			if (usePermutations)
				d = StegoDokuV5.decode(b, key);
			else
				d = StegoDokuV3.decode(b);
		else
			if (usePermutations)
				d = StegoDokuV4.decode(b, key);
			else
				d = StegoDokuV2.decode(b);
		System.out.println("Decoded Message ("+d.length()+" bits):\n" + d);
		} catch (Exception e) {
		}
	}
	
	public static void makePuzzle() {
		SudokuPuzzleMaker spm;
		int[][] b;
		try {
			b = new SudokuBoard(board).board();
		spm = new SudokuPuzzleMaker(b);
		spm.makePuzzle(100); // find fewest clues out of 100 attempts
		SudokuBoard.print(spm.puzzle(), path);
		} catch (Exception e) {
		}
	}
	
	public static void solvePuzzle() {
		SudokuSolver ss = new SudokuSolver(board);
		ss.printBoard();
		ss.solveCells(debug);
		ss.printBoard();
	}
	
	public static void generate() {
		if (random)
			SudokuBoard.print(new RandomSudoku().board(),path);
		else {
			StegoDoku sd;
			if (useHuffman)
				if (usePermutations)
					sd = new StegoDokuV5(secret,key);
				else
					sd = new StegoDokuV3(secret);
			else
				if (usePermutations)
					sd = new StegoDokuV4(secret,key);
				else
					sd = new StegoDokuV2(secret);
			SudokuBoard.print(sd.board(),path);
		}
	}

	public static void main(String[] args) {
		if (args.length > 0)
			parseOptions(args);
		else {
			printUsage();
			System.exit(0);
		}
		
		StegoDoku.setPaddingMode(paddingMode);

		long t1 = System.currentTimeMillis();
		if (runTest)
			runTest();
		else if (decode)
			decode();
		else if (makePuzzle)
			makePuzzle();
		else if (solvePuzzle)
			solvePuzzle();
		else if (encode || random)
			generate();
		long t2 = System.currentTimeMillis();
		System.out.println();
		System.out.println("elapsed time: " + (t2-t1)/1000 + " seconds");
		System.out.println();
	}
}
