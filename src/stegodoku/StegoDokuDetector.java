package stegodoku;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;

import libPhil.huffman.HuffmanTree;
import libPhil.sudoku.RandomSudoku;
import libPhil.sudoku.SudokuPuzzleMaker;
import libPhil.sudoku.SudokuSolver;
import static libPhil.sudoku.SudokuPuzzleMaker.isSymmetric;

public class StegoDokuDetector {


	/**
	 * Are StegoDoku puzzles which hide plaintext ASCII distinguishable
	 * from clean sudoku puzzles?
	 * 
	 * Method:
	 * Generate 500 clean sudoku puzzles
	 * Generate 500 StegoDoku puzzles using random ASCII messages
	 *   use 32-126
	 * For each board
	 *   decode it to obtain bits
	 *   test if bits are ASCII
	 *   if ASCII, classify board as StegoDoku
	 *   else, classify board as clean
	 * Report detection accuracy
	 * Repeat for other StegoDoku versions
	 * 
	 * Expected Results:
	 * 100% accuracy against StegoDoku V2 and V3
	 * 50% accuracy against StegoDoku V4 and V5
	 * because V4 and V5 require a key in order to correctly decode the board
	 * 
	 * Actual Results:
	 * As expected.
	 * 100% on V2 and V3
	 * 50% on V4 and V5
	 */
	public static void Experiment1() {
		System.out.println("Running Experiment 1");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C2 = new int[2][2];
		int[][] C3 = new int[2][2];
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];
		String d;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				C2[i][j] = 0;
				C3[i][j] = 0;
				C4[i][j] = 0;
				C5[i][j] = 0;
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			d = StegoDokuV2.decode(rs.board());
			if (isASCII(d)) { // classify as dirty
				// which is wrong
				C2[0][1]++;
			} else { // classify as clean
				// which is correct
				C2[0][0]++;
			}
			d = StegoDokuV3.decode(rs.board());
			if (isASCII(d)) { // classify as dirty
				// which is wrong
				C3[0][1]++;
			} else { // classify as clean
				// which is correct
				C3[0][0]++;
			}
			d = StegoDokuV4.decode(rs.board(),random.nextLong());
			if (isASCII(d)) { // classify as dirty
				// which is wrong
				C4[0][1]++;
			} else { // classify as clean
				// which is correct
				C4[0][0]++;
			}
			d = StegoDokuV5.decode(rs.board(),random.nextLong());
			if (isASCII(d)) { // classify as dirty
				// which is wrong
				C5[0][1]++;
			} else { // classify as clean
				// which is correct
				C5[0][0]++;
			}

			mssg = "";
			for (int j = 0; j < 10; j++) 
				mssg += "0" + Integer.toBinaryString(random.nextInt(126-32)+32+128).substring(1);
			sd2 = new StegoDokuV2(mssg);
			d = StegoDokuV2.decode(sd2.board());
			if (isASCII(d)) { // classify as dirty
				// which is correct
				C2[1][1]++;
			} else { // classify as clean
				// which is wrong
				C2[1][0]++;
			}
			sd3 = new StegoDokuV3(mssg);
			d = StegoDokuV3.decode(sd3.board());
			if (isASCII(d)) { // classify as dirty
				// which is correct
				C3[1][1]++;
			} else { // classify as clean
				// which is wrong
				C3[1][0]++;
			}
			sd4 = new StegoDokuV4(mssg,random.nextLong());
			d = StegoDokuV4.decode(sd4.board(),random.nextLong());
			if (isASCII(d)) { // classify as dirty
				// which is correct
				C4[1][1]++;
			} else { // classify as clean
				// which is wrong
				C4[1][0]++;
			}
			sd5 = new StegoDokuV5(mssg,random.nextLong());
			d = StegoDokuV5.decode(sd5.board(),random.nextLong());
			if (isASCII(d)) { // classify as dirty
				// which is correct
				C5[1][1]++;
			} else { // classify as clean
				// which is wrong
				C5[1][0]++;
			}
		}

		/*
			System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
			System.out.println("acc = " + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
			System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
			System.out.println("acc = " + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
			System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
			System.out.println("acc = " + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
			System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
			System.out.println("acc = " + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
		 */
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr1_results.txt"));
			outputStream.write("StegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			double acc2 =  ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]);
			double acc3 =  ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]);
			double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
			double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
			outputStream.write(acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
			outputStream.newLine();
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * determine if bitstring is ASCII 
	 * @param bits
	 * @return true if all bytes are between 32 and 126, inclusive
	 */
	public static boolean isASCII(String bits) {
		for (int i = 0; i+8 <= bits.length(); i+=8) {
			int n = Integer.parseInt(bits.substring(i, i+8), 2);
			if (n < 32 || n > 126) 
				return false;
		}
		return true;
	}

	/**
	 * Are StegoDoku puzzles which hide random bits (ciphertext analogue) distinguishable
	 * from clean sudoku puzzles?
	 * 
	 * Can't use ASCII test, so use statistics of board generation
	 * StegoDokuV2 won't put a 9 in the first cell, ever
	 * StegoDokuV3 will put a 9 there 1/8 of the time
	 * Clean will put a 9 there 1/9 of the time
	 * p = 1/8 and p = 1/9 are distinguishable given enough data
	 * but, it takes a LOT of data, like 10000s of boards
	 * 
	 * StegoDokuV3 will put a 1 in the first cell 1/16 of the time
	 * same for putting a 2 there.
	 * This is because the Huffman code gives 1 and 2 4-bit codewords
	 * 
	 * Method:
	 * Generate 500*n clean boards
	 * Generate 500*n StegoDoku boards using random bits
	 * For each set of n boards
	 *   count number of time 9 and 1 appears in the first cell
	 *   for StegoDokuV2 and StegoDokuV4,
	 *     if 9 appears 0 times, classify as StegoDoku
	 *     else, classify as clean
	 *   for StegoDokuV3 and StegoDokuV5,
	 *     if 9 appears >= n*17/144 > 1 times, classify board as StegoDoku
	 *     or if 1 appears <= n*25/288, classify board as StegoDoku
	 *     else, classify board as clean
	 * Report detection accuracy
	 * Repeat for other StegoDoku versions
	 * 
	 * 17/144 comes from:
	 * Prob(Basic puts a 9 in cell 0) = 0
	 * Prob(Huffman puts a 9 in cell 0) = 1/8
	 * Prob(Clean puts a 9 in cell 0) = 1/9
	 * (1/8 + 1/9)/2 = 17/144
	 * midpoint is best threshold for Huffman and Clean
	 * 
	 * 25/288 comes from:
	 * Prob(Huffman puts a 1 in cell 0) = 1/16
	 * Prob(Clean puts a 1 in cell 0) = 1/9
	 * 25/288 = (1/9+1/16)/2
	 * again, midpoint 
	 * 
	 * Expected Results:
	 * 100% accuracy against V2
	 * barely more than 50% accuracy against V3
	 * 50% accuracy against V4, and V5
	 * because V2 will never put 9 in the first cell
	 * but V3 will put 9 there more often, and 1 there less often than clean
	 * V4 and V5 will put both there with equal likelihood to clean due to permutations
	 * 
	 * Actual Results:
	 * 
	 */
	public static void Experiment2() {
		System.out.println("Running Experiment 2");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C2 = new int[2][2];
		int[][] C3 = new int[2][2];
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];
		double eps9;
		double eps1;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr2_results.txt"));
			outputStream.write("n\tStegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int n = 1; n < 30; n++) {
				eps9 = n * 17.0/144.0;
				eps1 = n * 25.0/288.0;
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 2; j++) {
						C2[i][j] = 0;
						C3[i][j] = 0;
						C4[i][j] = 0;
						C5[i][j] = 0;
					}
				}
				for (int i = 0; i < 500; i++) {
					int n9 = 0;
					int n1 = 0;
					for (int j = 0; j < n; j++) {
						rs = new RandomSudoku();
						if (rs.board(0,0) == 9) 
							n9++;
						if (rs.board(0,0) == 1)
							n1++;
					}
					if (n9 == 0) { // classify as dirty against V2 and V4
						// which is wrong
						C2[0][1]++;
						C4[0][1]++;
					} else { // classify as clean
						// which is correct
						C2[0][0]++;
						C4[0][0]++;
					}
					if (n9 >= eps9 || n1 <= eps1) { // classify as dirty against V3 and V5
						// which is wrong
						C3[0][1]++;
						C5[0][1]++;
					} else { // classify as clean
						// which is correct
						C3[0][0]++;
						C5[0][0]++;
					}

					int n9_2 = 0;
					int n9_3 = 0;
					int n9_4 = 0;
					int n9_5 = 0;
					int n1_3 = 0;
					int n1_5 = 0;
					for (int j = 0; j < n; j++) {
						mssg = "";
						for (int k = 0; k < 82; k++) 
							mssg += random.nextBoolean() ? "0" : "1";
						sd2 = new StegoDokuV2(mssg);
						sd3 = new StegoDokuV3(mssg);
						sd4 = new StegoDokuV4(mssg,random.nextLong());
						sd5 = new StegoDokuV5(mssg,random.nextLong());
						if (sd2.board(0,0) == 9) 
							n9_2++;
						if (sd3.board(0,0) == 9) 
							n9_3++;
						if (sd4.board(0,0) == 9) 
							n9_4++;
						if (sd5.board(0,0) == 9) 
							n9_5++;
						if (sd3.board(0,0) == 1) 
							n1_3++;
						if (sd5.board(0,0) == 1) 
							n1_5++;
					}

					if (n9_2 == 0) { // classify as dirty
						// which is correct
						C2[1][1]++;
					} else { // classify as clean
						// which is wrong
						C2[1][0]++;
					}
					if (n9_4 == 0) { // classify as dirty
						// which is correct
						C4[1][1]++;
					} else { // classify as clean
						// which is wrong
						C4[1][0]++;
					}
					if (n9_3 >= eps9 || n1_3 <= eps1) { // classify as dirty
						// which is correct
						C3[1][1]++;
					} else { // classify as clean
						// which is wrong
						C3[1][0]++;
					}
					if (n9_5 >= eps9 || n1_5 <= eps1) { // classify as dirty
						// which is correct
						C5[1][1]++;
					} else { // classify as clean
						// which is wrong
						C5[1][0]++;
					}
				}
				/*
				//System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
				System.out.print("\t" + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
				//System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
				System.out.print("\t" + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
				//System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
				System.out.print("\t" + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
				//System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
				System.out.print("\t" + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
				System.out.println();
				 */
				double acc2 =  ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]);
				double acc3 =  ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]);
				double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
				double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
				outputStream.write(n + "\t" + acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();
				outputStream.flush();
			}
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Experiment 2 requires that many boards from the same source be available
	 * for analysis.  And, it only uses the first cell on each board.  In this experiment, 
	 * we attempt to reduce the number of boards required for analysis to 1
	 * by using more than just the first cell.
	 * 
	 * In basic StegoDoku, the first cell, will never be the last candidate.
	 *   the 3rd cell will never be either of the last 3 candidates.
	 *   the 4th cell will never be either of the last 2,
	 *   the 5th and 7th will never be the last.
	 * This is because those last candidates are not used by the basic StegoDoku
	 * algorithm.  To use this feature for detection against StegoDokuV2, 
	 * we simply go through the board as if we were decoding it count the number 
	 * of times an 'out-of-bounds' candidate is chosen.  If the count is 0, 
	 * the board is StegoDoku, otherwise it is clean.
	 * 
	 * In StegoDoku-H, in all but the power-of-two-sized candidates lists, 
	 * some candidates will be chosen more often than others (equivalently, 
	 * some will be chosen less often than others).
	 * |c| = 3: p(i=0) = p(i=1) = 1/4, p(i=2) = 1/2
	 * |c| = 5: p(i=0) = p(i=1) = 1/8, p(i=2) = p(i=3) = p(i=4) = 1/4
	 * |c| = 6: p(i=0) = p(i=1) = p(i=2) = p(i=3) = 1/8, p(i=4) = p(i=5) = 1/4
	 * |c| = 7: p(i=0) = p(i=1) = p(i=2) = p(i=3) = p(i=4) = p(i=5) = 1/8, p(i=6) = 1/4
	 * |c| = 9: p(i=0) = p(i=1) = 1/16, p(i=2) = p(i=3) = p(i=4) = p(i=5) = p(i=6) = p(i=7) = p(i=8) = 1/4
	 * This is an effect of using Huffman coding.  To use this feature for detection against
	 * StegoDokuV3 we go through the board as if we are decoding it and keep track
	 * of the ratio sums of the log of the probability if the chosen candidates under both
	 * the StegoDoku model (Huffman distribution) and the clean model (uniform distribution).
	 * Then, take the ratio of the sum for the Huffman model hypothesis to the sum for the
	 * clean model hypothesis.  If the ratio is > 1, classify as StegoDoku,
	 * else classify as clean.
	 * 
	 * Method:
	 * Generate 500 clean boards
	 * Generate 500 StegoDoku boards using random bits
	 * For each board
	 *   for V2 and V4, count out-of-bounds candidates, if 0 --> StegoDoku, else --> clean
	 *   for V3 and V5, compute ratio of sum of log probabilities, if > 1 --> StegoDoku, else --> clean
	 * Report detection accuracy
	 * 
	 * Expected Results:
	 * 100% against V2 and V3
	 * 50% against V4 and V5
	 * Permutations are a pain for detectors
	 * 
	 * Actual Results:
	 * 99.9% against V2, as expected
	 * 76.5% against V3, lower than expected
	 * 49.9% against V4, as expected
	 * 48.2% against V5, as expected
	 */
	public static void Experiment3() {
		System.out.println("Running Experiment 3");
		System.err.println("WARNING: Experiments 4 and 5, together, are the complete version of Experiment 3");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C2 = new int[2][2];
		int[][] C3 = new int[2][2];
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				C2[i][j] = 0;
				C3[i][j] = 0;
				C4[i][j] = 0;
				C5[i][j] = 0;
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			int n = expr3_helper_v2(rs.board());
			if (n == 0) { // classify as StegoDoku
				// which is wrong
				C2[0][1]++;
				C4[0][1]++;
			} else { //classify as clean
				// which is correct
				C2[0][0]++;
				C4[0][0]++;
			}
			double r = expr3_helper_v3(rs.board());
			if (r >= 1) { // classify as StegoDoku
				// which is wrong
				C3[0][1]++;
				C5[0][1]++;
			} else { // classify as clean
				// which is correct
				C3[0][0]++;
				C5[0][0]++;
			}

			mssg = "";
			for (int k = 0; k < 82; k++) 
				mssg += random.nextBoolean() ? "0" : "1";
			sd2 = new StegoDokuV2(mssg);
			sd3 = new StegoDokuV3(mssg);
			sd4 = new StegoDokuV4(mssg,random.nextLong());
			sd5 = new StegoDokuV5(mssg,random.nextLong());
			n = expr3_helper_v2(sd2.board());
			if (n == 0) { // classify as StegoDoku
				// which is correct
				C2[1][1]++;
			} else { //classify as clean
				// which is wrong
				C2[1][0]++;
			}
			r = expr3_helper_v3(sd3.board());
			if (r >= 1) { // classify as StegoDoku
				// which is correct
				C3[1][1]++;
			} else { // classify as clean
				// which is wrong
				C3[1][0]++;
			}
			n = expr3_helper_v2(sd4.board());
			if (n == 0) { // classify as StegoDoku
				// which is correct
				C4[1][1]++;
			} else { //classify as clean
				// which is wrong
				C4[1][0]++;
			}
			r = expr3_helper_v3(sd5.board());
			if (r >= 1) { // classify as StegoDoku
				// which is correct
				C5[1][1]++;
			} else { // classify as clean
				// which is wrong
				C5[1][0]++;
			}
		}
		/*
		System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
		System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
		System.out.print(((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
		System.out.println("\t" + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
		//System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
		//System.out.print("\t" + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
		//System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
		//System.out.println("\t" + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
		 */
		BufferedWriter outputStream;
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr3_results.txt"));
			outputStream.write("StegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			double acc2 =  ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]);
			double acc3 =  ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]);
			double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
			double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
			outputStream.write(acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
			outputStream.newLine();
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * count out of bounds choices during generation for V2
	 * @param board
	 * @return count of out of bounds choices
	 */
	public static int expr3_helper_v2(int[][] board) {
		SudokuSolver s = new SudokuSolver();
		int oob_cnt = 0;
		while (!s.isSolved()) {
			int[] cell = StegoDoku.nextCell(s);
			int ii = cell[0];
			int jj = cell[1];
			if (ii == -1) {
				System.err.println("(Error) expr3_helper_v2: Board complete but not solved.");
				return oob_cnt;
			}
			LinkedList<Integer> c = s.candidates(ii, jj);
			int cap = (int)(Math.log(c.size())/Math.log(2));
			if (cap > 0) {
				int n = c.indexOf(new Integer(board[ii][jj]));
				// if index is out of StegoDoku's bounds
				if (n >= Math.pow(2, cap))
					oob_cnt++;
			}
			if (!s.set(ii, jj, board[ii][jj])) {
				System.err.println("expr3_helper_v2: did not set cell ("+ii+","+jj+")");
				break;
			}
			s.solveCells();
		}
		return oob_cnt;
	}

	/**
	 * compute ratio of log likelihoods for V3
	 * @param board
	 * @return ratio of log likelihoods: p(D|Huffman)/p(D|Clean)
	 */
	public static double expr3_helper_v3(int[][] board) {
		SudokuSolver s = new SudokuSolver();
		double llH = 0;
		double llC = 0;
		while (!s.isSolved()) {
			int[] cell = StegoDoku.nextCell(s);
			int ii = cell[0];
			int jj = cell[1];
			if (ii == -1) {
				System.err.println("(Error) expr3_helper_v3(): Board complete but not solved.");
				//return llH/llC;
				return llC/llH; // engineering
			}
			LinkedList<Integer> c = s.candidates(ii, jj);

			// accumulate log likelihoods
			//if (c.size() == 3 || c.size() == 5 || c.size() == 6 || c.size() == 7 || c.size() == 9) {
			if (c.size() > 1) {
				HuffmanTree tree = new HuffmanTree(c.toArray());
				String m = tree.encode(board[ii][jj]);
				llH -= m.length();
				llC -= Math.log(c.size())/Math.log(2);
			}
			if (!s.set(ii, jj, board[ii][jj])) {
				System.err.println("expr3_helper_v3(): did not set cell ("+ii+","+jj+")");
				break;
			}
			s.solveCells();
		}
		//return llH/llC;
		return llC/llH; // engineering
	}

	/** Apply the expr3_helper_v2() function to V2,3,4,5 and see how it performs.
	 * 
	 * Method:
	 *   generate 500 random sudoku boards
	 *    and 500 StegoDoku boards
	 *   for each board,
	 *     count out of bounds choices
	 *     if count == 0, classify as StegoDoku
	 *     else, classify as clean
	 * 
	 * Expected Results:
	 * 100% accuracy on V2
	 * 50% accuracy on V3,4,5 because Huffman coding and permutations 
	 * allows all candidates to be usable
	 * 
	 * Actual Results:
	 * as expected.
	 * 99.9% on V2
	 * 49.9% on V3
	 * 50% on V4
	 * 49.9% on V5
	 */
	public static void Experiment4() {
		System.out.println("Running Experiment 4");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C2 = new int[2][2];
		int[][] C3 = new int[2][2];
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				C2[i][j] = 0;
				C3[i][j] = 0;
				C4[i][j] = 0;
				C5[i][j] = 0;
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			int n = expr3_helper_v2(rs.board());
			if (n == 0) { // classify as StegoDoku
				// which is wrong
				C2[0][1]++;
				C3[0][1]++;
				C4[0][1]++;
				C5[0][1]++;
			} else { //classify as clean
				// which is correct
				C2[0][0]++;
				C3[0][0]++;
				C4[0][0]++;
				C5[0][0]++;
			}

			mssg = "";
			for (int k = 0; k < 82; k++) 
				mssg += random.nextBoolean() ? "0" : "1";
			sd2 = new StegoDokuV2(mssg);
			sd3 = new StegoDokuV3(mssg);
			sd4 = new StegoDokuV4(mssg,random.nextLong());
			sd5 = new StegoDokuV5(mssg,random.nextLong());
			n = expr3_helper_v2(sd2.board());
			if (n == 0) { // classify as StegoDoku
				// which is correct
				C2[1][1]++;
			} else { //classify as clean
				// which is wrong
				C2[1][0]++;
			}
			n = expr3_helper_v2(sd3.board());
			if (n == 0) { // classify as StegoDoku
				// which is correct
				C3[1][1]++;
			} else { //classify as clean
				// which is wrong
				C3[1][0]++;
			}
			n = expr3_helper_v2(sd4.board());
			if (n == 0) { // classify as StegoDoku
				// which is correct
				C4[1][1]++;
			} else { //classify as clean
				// which is wrong
				C4[1][0]++;
			}
			n = expr3_helper_v2(sd5.board());
			if (n == 0) { // classify as StegoDoku
				// which is correct
				C5[1][1]++;
			} else { //classify as clean
				// which is wrong
				C5[1][0]++;
			}
		}
		/*
		//System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
		System.out.print("\t" + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
		//System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
		System.out.print("\t" + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
		//System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
		System.out.print("\t" + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
		//System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
		System.out.println("\t" + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
		 */
		BufferedWriter outputStream;
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr4_results.txt"));
			outputStream.write("StegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			double acc2 =  ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]);
			double acc3 =  ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]);
			double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
			double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
			outputStream.write(acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
			outputStream.newLine();
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Apply expr3_helper_v3() to V2,3,4,5 and see how it does
	 * 
	 * Method:
	 *   generate 500 random sudoku boards
	 *    and 500 StegoDoku boards
	 *   for each board,
	 *     compute ratio of log likelihood P(Board|Huffman)/P(Board|Random)
	 *     if ratio >= 1, classify as StegoDoku
	 *     else, classify as clean
	 * 
	 * Expected results:
	 * 50% on V2 because V2 does not use Huffman codes
	 * 100% on V3 because V3 does
	 * 50% on V4,5 because permutations ruin everything
	 * 
	 * Actual results:
	 * 39.3% on V2 (60.7% flipped), higher than expected
	 * 78% on V3, lower than expected
	 * 51.5% on V4, as expected
	 * 52.5% on V5, as expected
	 */
	public static void Experiment5() {
		System.out.println("Running Experiment 5");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C2 = new int[2][2];
		int[][] C3 = new int[2][2];
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				C2[i][j] = 0;
				C3[i][j] = 0;
				C4[i][j] = 0;
				C5[i][j] = 0;
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			double r = expr3_helper_v3(rs.board());
			if (r > 1) { // classify as StegoDoku
				// which is wrong
				C2[0][1]++;
				C3[0][1]++;
				C4[0][1]++;
				C5[0][1]++;
			} else { //classify as clean
				// which is correct
				C2[0][0]++;
				C3[0][0]++;
				C4[0][0]++;
				C5[0][0]++;
			}

			mssg = "";
			for (int k = 0; k < 82; k++) 
				mssg += random.nextBoolean() ? "0" : "1";
			sd2 = new StegoDokuV2(mssg);
			sd3 = new StegoDokuV3(mssg);
			sd4 = new StegoDokuV4(mssg,random.nextLong());
			sd5 = new StegoDokuV5(mssg,random.nextLong());
			r = expr3_helper_v3(sd2.board());
			if (r >= 1) { // classify as StegoDoku
				// which is correct
				C2[1][1]++;
			} else { //classify as clean
				// which is wrong
				C2[1][0]++;
			}
			r = expr3_helper_v3(sd3.board());
			if (r >= 1) { // classify as StegoDoku
				// which is correct
				C3[1][1]++;
			} else { //classify as clean
				// which is wrong
				C3[1][0]++;
			}
			r = expr3_helper_v3(sd4.board());
			if (r >= 1) { // classify as StegoDoku
				// which is correct
				C4[1][1]++;
			} else { //classify as clean
				// which is wrong
				C4[1][0]++;
			}
			r = expr3_helper_v3(sd5.board());
			if (r >= 1) { // classify as StegoDoku
				// which is correct
				C5[1][1]++;
			} else { //classify as clean
				// which is wrong
				C5[1][0]++;
			}
		}
		/*
		//System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
		System.out.print("\t" + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
		//System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
		System.out.print("\t" + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
		//System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
		System.out.print("\t" + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
		//System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
		System.out.println("\t" + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
		 */
		BufferedWriter outputStream;
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr5_results.txt"));
			outputStream.write("StegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			double acc2 =  ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]);
			double acc3 =  ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]);
			double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
			double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
			outputStream.write(acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
			outputStream.newLine();
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Do experiments 4 and 5 for n=1..10 boards
	 */
	public static void Experiment6() {
		System.out.println("Running Experiment 6");
		System.err.println("Warning: Experiment 6 takes 55 times as long as Experiments 4 and 5 (back-to-back)");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C2 = new int[2][2];
		int[][] C3 = new int[2][2];
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		// do Experiment 4 for n=1..10
		System.out.println("Experiment 6.4");
		BufferedWriter outputStream;
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr6.4_results.txt"));
			outputStream.write("n\tStegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 1; k <= 10; k++) {
				System.out.println("\tn = " + k);
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 2; j++) {
						C2[i][j] = 0;
						C3[i][j] = 0;
						C4[i][j] = 0;
						C5[i][j] = 0;
					}
				}
				for (int i = 0; i < 500; i++) {
					int n = 0;
					for (int j = 0; j < k; j++) {
						rs = new RandomSudoku();
						n += expr3_helper_v2(rs.board());
					}
					if (n == 0) { // classify as StegoDoku
						// which is wrong
						C2[0][1]++;
						C3[0][1]++;
						C4[0][1]++;
						C5[0][1]++;
					} else { //classify as clean
						// which is correct
						C2[0][0]++;
						C3[0][0]++;
						C4[0][0]++;
						C5[0][0]++;
					}

					int n2 = 0;
					int n3 = 0;
					int n4 = 0;
					int n5 = 0;
					for (int j = 0; j < k; j++) {
						mssg = "";
						for (int l = 0; l < 82; l++) 
							mssg += random.nextBoolean() ? "0" : "1";
						sd2 = new StegoDokuV2(mssg);
						sd3 = new StegoDokuV3(mssg);
						sd4 = new StegoDokuV4(mssg,random.nextLong());
						sd5 = new StegoDokuV5(mssg,random.nextLong());
						n2 += expr3_helper_v2(sd2.board());
						n3 += expr3_helper_v2(sd3.board());
						n4 += expr3_helper_v2(sd4.board());
						n5 += expr3_helper_v2(sd5.board());
					}
					if (n2 == 0) { // classify as StegoDoku
						// which is correct
						C2[1][1]++;
					} else { //classify as clean
						// which is wrong
						C2[1][0]++;
					}
					if (n3 == 0) { // classify as StegoDoku
						// which is correct
						C3[1][1]++;
					} else { //classify as clean
						// which is wrong
						C3[1][0]++;
					}
					if (n4 == 0) { // classify as StegoDoku
						// which is correct
						C4[1][1]++;
					} else { //classify as clean
						// which is wrong
						C4[1][0]++;
					}
					if (n5 == 0) { // classify as StegoDoku
						// which is correct
						C5[1][1]++;
					} else { //classify as clean
						// which is wrong
						C5[1][0]++;
					}
				}
				/*
				//System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
				System.out.print("\t" + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
				//System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
				System.out.print("\t" + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
				//System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
				System.out.print("\t" + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
				//System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
				System.out.println("\t" + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
				 */

				double acc2 =  ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]);
				double acc3 =  ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]);
				double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
				double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
				outputStream.write(k + "\t" + acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();	
				outputStream.flush();
			}
			outputStream.close();
			System.out.println("\n1/2-done!");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// do Experiment 5 for n=1..10
		System.out.println("Experiment 6.5");
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr6.5_results.txt"));
			outputStream.write("n\tStegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 1; k <= 10; k++) {
				System.out.println("\tn = " + k);
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 2; j++) {
						C2[i][j] = 0;
						C3[i][j] = 0;
						C4[i][j] = 0;
						C5[i][j] = 0;
					}
				}
				for (int i = 0; i < 500; i++) {
					double r = 0.0;
					for (int j = 0; j < k; j++) {
						rs = new RandomSudoku();
						r += Math.log(expr3_helper_v3(rs.board()))/Math.log(2);
					}
					if (r >= 0) { // classify as StegoDoku
						// which is wrong
						C2[0][1]++;
						C3[0][1]++;
						C4[0][1]++;
						C5[0][1]++;
					} else { //classify as clean
						// which is correct
						C2[0][0]++;
						C3[0][0]++;
						C4[0][0]++;
						C5[0][0]++;
					}

					double r2 = 0;
					double r3 = 0;
					double r4 = 0;
					double r5 = 0;
					for (int j = 0; j < k; j++) {
						mssg = "";
						for (int l = 0; l < 82; l++) 
							mssg += random.nextBoolean() ? "0" : "1";
						sd2 = new StegoDokuV2(mssg);
						sd3 = new StegoDokuV3(mssg);
						sd4 = new StegoDokuV4(mssg,random.nextLong());
						sd5 = new StegoDokuV5(mssg,random.nextLong());
						r2 += Math.log(expr3_helper_v3(sd2.board()))/Math.log(2);
						r3 += Math.log(expr3_helper_v3(sd3.board()))/Math.log(2);
						r4 += Math.log(expr3_helper_v3(sd4.board()))/Math.log(2);
						r5 += Math.log(expr3_helper_v3(sd5.board()))/Math.log(2);
					}
					if (r2 >= 0) { // classify as StegoDoku
						// which is correct
						C2[1][1]++;
					} else { //classify as clean
						// which is wrong
						C2[1][0]++;
					}
					if (r3 >= 0) { // classify as StegoDoku
						// which is correct
						C3[1][1]++;
					} else { //classify as clean
						// which is wrong
						C3[1][0]++;
					}
					if (r4 >= 0) { // classify as StegoDoku
						// which is correct
						C4[1][1]++;
					} else { //classify as clean
						// which is wrong
						C4[1][0]++;
					}
					if (r5 >= 0) { // classify as StegoDoku
						// which is correct
						C5[1][1]++;
					} else { //classify as clean
						// which is wrong
						C5[1][0]++;
					}
				}
				/*
				//System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
				System.out.print("\t" + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
				//System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
				System.out.print("\t" + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
				//System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
				System.out.print("\t" + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
				//System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
				System.out.println("\t" + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
				 */

				double acc2 =  ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]);
				double acc3 =  ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]);
				double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
				double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
				outputStream.write(k + "\t" + acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();
				outputStream.flush();
			}
			outputStream.close();
			System.out.println("\n1/2-done!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * What is the probability that RandomSudoku looks like StegoDoku?
	 * 
	 * Method
	 * do 100 times:
	 *   generate a random sudoku board
	 *   count the number of out-of-bounds choices
	 *   repeat (generate and count) until count is 0
	 *   record number of trials required
	 * 
	 * This will have a geometric distribution
	 * therefore, the probability that a random sudoku looks like StegoDoku
	 * will be approximately 1/n, where n is the average number of trials
	 * required to find a board which is authentic but looks like StegoDoku
	 * 
	 * Expected results:
	 * p = very small
	 * 
	 * Actual results:
	 * p = 1/(1095 +/- 73) with 50% confidence
	 * it takes a long time to do it this way
	 * with 100 samples, the confidence level is low for even a wide interval
	 * time for 100 samples: 2000 seconds
	 * estimated time for 10000 samples: 56 hours, too long!
	 */
	public static void Experiment7() {
		System.out.println("Running Experiment 7");
		RandomSudoku rs;
		int n,c;

		BufferedWriter outputStream; 
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr7_results.txt"));
			outputStream.write("num trials");
			outputStream.newLine();
			for (int i = 0; i < 100; i++) {
				n = 0;
				do {
					n++;
					rs = new RandomSudoku();
					c = expr3_helper_v2(rs.board());
				} while (c > 0);
				//System.out.println(n);
				outputStream.write(Integer.toString(n));
				outputStream.newLine();
			}
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * compute likelihood that a StegoDoku was generated at random
	 * 
	 * Method:
	 * do 10000 times:
	 *   generate a random StegoDoku board
	 *   compute log of ratio of probabilities: log(Prob(authentic)/Prob(StegoDoku))
	 *   record ratio
	 *   
	 * Expected results:
	 * p = very small
	 * 
	 * Actual results:
	 * p = 1/(1131 +/- 20) with 99.999% confidence 
	 */
	public static void Experiment8() {
		System.out.println("Running Experiment 8");
		Random random = new Random();
		String mssg;
		StegoDokuV2 sd;
		double r;

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr8_results.txt"));
			outputStream.write("log(Prob(Authentic)/Prob(Stego))");
			outputStream.newLine();
			for (int i = 0; i < 10000; i++) {
				mssg = "";
				for (int k = 0; k < 82; k++) 
					mssg += random.nextBoolean() ? "0" : "1";
				sd = new StegoDokuV2(mssg);
				r = expr8_helper(sd.board());
				//System.out.println(r);
				outputStream.write(Double.toString(r));
				outputStream.newLine();
			}
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}



	}

	/**
	 * compute log ratio of probability of authentic to probability of StegoDoku
	 * @param board
	 * @return log(Prob(authentic)/Prob(StegoDoku)) 
	 */
	public static double expr8_helper(int[][] board) {
		SudokuSolver s = new SudokuSolver();
		double ratio = 0.0;
		while (!s.isSolved()) {
			int[] cell = StegoDoku.nextCell(s);
			int ii = cell[0];
			int jj = cell[1];
			if (ii == -1) {
				System.err.println("(Error) expr8_helper: Board complete but not solved.");
				return ratio;
			}
			LinkedList<Integer> c = s.candidates(ii, jj);
			int cap = (int)(Math.log(c.size())/Math.log(2));
			ratio += cap-Math.log(c.size())/Math.log(2);
			if (!s.set(ii, jj, board[ii][jj])) {
				System.err.println("expr8_helper: did not set cell ("+ii+","+jj+")");
				break;
			}
			s.solveCells();
		}
		return ratio;
	}

	/**
	 * Can padding be used to detect StegoDoku with short messages?
	 * 
	 * Method:
	 *   Generate 500 clean and 500 dirty boards (use short messages)
	 *   for each board,
	 *     decode
	 *     if last n bits are all the same, classify as dirty
	 *     else classify as clean
	 * 
	 * Expected results:
	 * 100% on V2 and V3
	 * 50% on V4 and V5
	 * 
	 * Actual results:
	 * as expected
	 * for k=10 (last 10 bits
	 * 99.6% on V2
	 * 100%	on V3
	 * 50.1% on V4
	 * 50.2% on V5
	 */
	public static void Experiment9() {
		System.out.println("Running Experiment 9");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][][] C2 = new int[10][2][2];
		int[][][] C3 = new int[10][2][2];
		int[][][] C4 = new int[10][2][2];
		int[][][] C5 = new int[10][2][2];
		String d;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int k = 0; k < 10; k++) {
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					C2[k][i][j] = 0;
					C3[k][i][j] = 0;
					C4[k][i][j] = 0;
					C5[k][i][j] = 0;
				}
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			d = StegoDokuV2.decode(rs.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C2[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C2[k][0][0]++;
				}
			}
			d = StegoDokuV3.decode(rs.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C3[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C3[k][0][0]++;
				}
			}
			d = StegoDokuV4.decode(rs.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C4[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C4[k][0][0]++;
				}
			}
			d = StegoDokuV5.decode(rs.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C5[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C5[k][0][0]++;
				}
			}

			mssg = "";
			// make short messages so there will be padding
			for (int k = 0; k < 10; k++) 
				mssg += random.nextBoolean() ? "0" : "1";
			sd2 = new StegoDokuV2(mssg);
			d = StegoDokuV2.decode(sd2.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C2[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C2[k][1][0]++;
				}
			}
			sd3 = new StegoDokuV3(mssg);
			d = StegoDokuV3.decode(sd3.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C3[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C3[k][1][0]++;
				}
			}
			sd4 = new StegoDokuV4(mssg,random.nextLong());
			d = StegoDokuV4.decode(sd4.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C4[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C4[k][1][0]++;
				}
			}
			sd5 = new StegoDokuV5(mssg,random.nextLong());
			d = StegoDokuV5.decode(sd5.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C5[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C5[k][1][0]++;
				}
			}
		}

		/*
			System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
			System.out.println("acc = " + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
			System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
			System.out.println("acc = " + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
			System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
			System.out.println("acc = " + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
			System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
			System.out.println("acc = " + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
		 */
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr9_results.txt"));
			outputStream.write("k\tStegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 0; k < 10; k++) {
				double acc2 =  ((double)(C2[k][0][0]+C2[k][1][1]))/(C2[k][0][0]+C2[k][0][1]+C2[k][1][0]+C2[k][1][1]);
				double acc3 =  ((double)(C3[k][0][0]+C3[k][1][1]))/(C3[k][0][0]+C3[k][0][1]+C3[k][1][0]+C3[k][1][1]);
				double acc4 =  ((double)(C4[k][0][0]+C4[k][1][1]))/(C4[k][0][0]+C4[k][0][1]+C4[k][1][0]+C4[k][1][1]);
				double acc5 =  ((double)(C5[k][0][0]+C5[k][1][1]))/(C5[k][0][0]+C5[k][0][1]+C5[k][1][0]+C5[k][1][1]);
				outputStream.write((k+1) + "\t" + acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();
			}
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * repeat experiment 9 for full length messages
	 * 
	 * Expected:
	 * 50% against all
	 * because without padding it is only guessing
	 * 
	 * Actual:
	 * for k=1..10
	 * 50% against all, as expected
	 */
	public static void Experiment10() {
		System.out.println("Running Experiment 10");
		RandomSudoku rs;
		StegoDokuV2 sd2;
		StegoDokuV3 sd3;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][][] C2 = new int[10][2][2];
		int[][][] C3 = new int[10][2][2];
		int[][][] C4 = new int[10][2][2];
		int[][][] C5 = new int[10][2][2];
		String d;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int k = 0; k < 10; k++) {
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					C2[k][i][j] = 0;
					C3[k][i][j] = 0;
					C4[k][i][j] = 0;
					C5[k][i][j] = 0;
				}
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			d = StegoDokuV2.decode(rs.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C2[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C2[k][0][0]++;
				}
			}
			d = StegoDokuV3.decode(rs.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C3[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C3[k][0][0]++;
				}
			}
			d = StegoDokuV4.decode(rs.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C4[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C4[k][0][0]++;
				}
			}
			d = StegoDokuV5.decode(rs.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C5[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C5[k][0][0]++;
				}
			}

			mssg = "";
			// make LONG messages so there will NOT be padding
			for (int k = 0; k < 90; k++) 
				mssg += random.nextBoolean() ? "0" : "1";
			sd2 = new StegoDokuV2(mssg);
			d = StegoDokuV2.decode(sd2.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C2[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C2[k][1][0]++;
				}
			}
			sd3 = new StegoDokuV3(mssg);
			d = StegoDokuV3.decode(sd3.board());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C3[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C3[k][1][0]++;
				}
			}
			sd4 = new StegoDokuV4(mssg,random.nextLong());
			d = StegoDokuV4.decode(sd4.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C4[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C4[k][1][0]++;
				}
			}
			sd5 = new StegoDokuV5(mssg,random.nextLong());
			d = StegoDokuV5.decode(sd5.board(),random.nextLong());
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C5[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C5[k][1][0]++;
				}
			}
		}

		/*
			System.out.println(C2[0][0]+"\t"+C2[0][1]+"\n"+C2[1][0]+"\t"+C2[1][1]);
			System.out.println("acc = " + ((double)(C2[0][0]+C2[1][1]))/(C2[0][0]+C2[0][1]+C2[1][0]+C2[1][1]));
			System.out.println(C3[0][0]+"\t"+C3[0][1]+"\n"+C3[1][0]+"\t"+C3[1][1]);
			System.out.println("acc = " + ((double)(C3[0][0]+C3[1][1]))/(C3[0][0]+C3[0][1]+C3[1][0]+C3[1][1]));
			System.out.println(C4[0][0]+"\t"+C4[0][1]+"\n"+C4[1][0]+"\t"+C4[1][1]);
			System.out.println("acc = " + ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]));
			System.out.println(C5[0][0]+"\t"+C5[0][1]+"\n"+C5[1][0]+"\t"+C5[1][1]);
			System.out.println("acc = " + ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]));
		 */
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr10_results.txt"));
			outputStream.write("k\tStegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 0; k < 10; k++) {
				double acc2 =  ((double)(C2[k][0][0]+C2[k][1][1]))/(C2[k][0][0]+C2[k][0][1]+C2[k][1][0]+C2[k][1][1]);
				double acc3 =  ((double)(C3[k][0][0]+C3[k][1][1]))/(C3[k][0][0]+C3[k][0][1]+C3[k][1][0]+C3[k][1][1]);
				double acc4 =  ((double)(C4[k][0][0]+C4[k][1][1]))/(C4[k][0][0]+C4[k][0][1]+C4[k][1][0]+C4[k][1][1]);
				double acc5 =  ((double)(C5[k][0][0]+C5[k][1][1]))/(C5[k][0][0]+C5[k][0][1]+C5[k][1][0]+C5[k][1][1]);
				outputStream.write((k+1) + "\t" + acc2 + "\t" + acc3 + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();
			}
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** 
	 * Repeat experiment 1
	 * for V4 and V5 when the key is known to the adversary
	 * 
	 */
	public static void Experiment11_1() {
		System.out.println("Running Experiment 11_1");
		RandomSudoku rs;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];
		String d;
		long key;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				C4[i][j] = 0;
				C5[i][j] = 0;
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			key = random.nextLong();
			d = StegoDokuV4.decode(rs.board(),key);
			if (isASCII(d)) { // classify as dirty
				// which is wrong
				C4[0][1]++;
			} else { // classify as clean
				// which is correct
				C4[0][0]++;
			}
			d = StegoDokuV5.decode(rs.board(),key);
			if (isASCII(d)) { // classify as dirty
				// which is wrong
				C5[0][1]++;
			} else { // classify as clean
				// which is correct
				C5[0][0]++;
			}

			mssg = "";
			for (int j = 0; j < 10; j++) 
				mssg += "0" + Integer.toBinaryString(random.nextInt(126-32)+32+128).substring(1);
			key = random.nextLong();
			sd4 = new StegoDokuV4(mssg,key);
			d = StegoDokuV4.decode(sd4.board(),key);
			if (isASCII(d)) { // classify as dirty
				// which is correct
				C4[1][1]++;
			} else { // classify as clean
				// which is wrong
				C4[1][0]++;
			}
			sd5 = new StegoDokuV5(mssg,key);
			d = StegoDokuV5.decode(sd5.board(),key);
			if (isASCII(d)) { // classify as dirty
				// which is correct
				C5[1][1]++;
			} else { // classify as clean
				// which is wrong
				C5[1][0]++;
			}
		}
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr11_1_results.txt"));
			outputStream.write("StegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
			double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
			outputStream.write(acc4  + "\t" + acc5);
			outputStream.newLine();
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** 
	 * Repeat experiment 6
	 * for V4 and V5 when the key is known to the adversary
	 * 
	 */
	public static void Experiment11_6() {
		System.out.println("Running Experiment 11_6");
		RandomSudoku rs;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][] C4 = new int[2][2];
		int[][] C5 = new int[2][2];
		long key;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		// do Experiment 4 for n=1..10
		System.out.println("Experiment 11_6.4");
		BufferedWriter outputStream;
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr11_6.4_results.txt"));
			outputStream.write("n\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 1; k <= 10; k++) {
				System.out.println("\tn = " + k);
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 2; j++) {
						C4[i][j] = 0;
						C5[i][j] = 0;
					}
				}
				for (int i = 0; i < 500; i++) {
					int n = 0;
					for (int j = 0; j < k; j++) {
						rs = new RandomSudoku();
						key = random.nextLong();
						n += expr11_6_helper_v2(rs.board(),key);
					}
					if (n == 0) { // classify as StegoDoku
						// which is wrong
						C4[0][1]++;
						C5[0][1]++;
					} else { //classify as clean
						// which is correct
						C4[0][0]++;
						C5[0][0]++;
					}

					int n4 = 0;
					int n5 = 0;
					for (int j = 0; j < k; j++) {
						mssg = "";
						for (int l = 0; l < 82; l++) 
							mssg += random.nextBoolean() ? "0" : "1";
						key = random.nextLong();
						sd4 = new StegoDokuV4(mssg,key);
						sd5 = new StegoDokuV5(mssg,key);
						n4 += expr11_6_helper_v2(sd4.board(),key);
						n5 += expr11_6_helper_v2(sd5.board(),key);
					}
					if (n4 == 0) { // classify as StegoDoku
						// which is correct
						C4[1][1]++;
					} else { //classify as clean
						// which is wrong
						C4[1][0]++;
					}
					if (n5 == 0) { // classify as StegoDoku
						// which is correct
						C5[1][1]++;
					} else { //classify as clean
						// which is wrong
						C5[1][0]++;
					}
				}

				double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
				double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
				outputStream.write(k + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();	
				outputStream.flush();
			}
			outputStream.close();
			System.out.println("\n1/2-done!");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// do Experiment 5 for n=1..10
		System.out.println("Experiment 11_6.5");
		try {
			outputStream = new BufferedWriter(new FileWriter("Expr11_6.5_results.txt"));
			outputStream.write("n\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 1; k <= 10; k++) {
				System.out.println("\tn = " + k);
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 2; j++) {
						C4[i][j] = 0;
						C5[i][j] = 0;
					}
				}
				for (int i = 0; i < 500; i++) {
					double r = 0.0;
					for (int j = 0; j < k; j++) {
						rs = new RandomSudoku();
						key = random.nextLong();
						r += Math.log(expr11_6_helper_v3(rs.board(),key))/Math.log(2);
					}
					if (r >= 0) { // classify as StegoDoku
						// which is wrong
						C4[0][1]++;
						C5[0][1]++;
					} else { //classify as clean
						// which is correct
						C4[0][0]++;
						C5[0][0]++;
					}

					double r4 = 0;
					double r5 = 0;
					for (int j = 0; j < k; j++) {
						mssg = "";
						for (int l = 0; l < 82; l++) 
							mssg += random.nextBoolean() ? "0" : "1";
						key = random.nextLong();
						sd4 = new StegoDokuV4(mssg,key);
						sd5 = new StegoDokuV5(mssg,key);
						r4 += Math.log(expr11_6_helper_v3(sd4.board(),key))/Math.log(2);
						r5 += Math.log(expr11_6_helper_v3(sd5.board(),key))/Math.log(2);
					}
					if (r4 >= 0) { // classify as StegoDoku
						// which is correct
						C4[1][1]++;
					} else { //classify as clean
						// which is wrong
						C4[1][0]++;
					}
					if (r5 >= 0) { // classify as StegoDoku
						// which is correct
						C5[1][1]++;
					} else { //classify as clean
						// which is wrong
						C5[1][0]++;
					}
				}

				double acc4 =  ((double)(C4[0][0]+C4[1][1]))/(C4[0][0]+C4[0][1]+C4[1][0]+C4[1][1]);
				double acc5 =  ((double)(C5[0][0]+C5[1][1]))/(C5[0][0]+C5[0][1]+C5[1][0]+C5[1][1]);
				outputStream.write(k + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();
				outputStream.flush();
			}
			outputStream.close();
			System.out.println("\n1/2-done!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * count out of bounds choices during generation for V2, with permutation seed
	 * @param board
	 * @return count of out of bounds choices
	 */
	public static int expr11_6_helper_v2(int[][] board, long seed) {
		SudokuSolver s = new SudokuSolver();
		Random rng = new Random(seed);
		int oob_cnt = 0;
		while (!s.isSolved()) {
			int[] cell = StegoDoku.nextCell(s);
			int ii = cell[0];
			int jj = cell[1];
			if (ii == -1) {
				System.err.println("(Error) expr11_6_helper_v2: Board complete but not solved.");
				return oob_cnt;
			}
			LinkedList<Integer> c = s.candidates(ii, jj);
			int cap = (int)(Math.log(c.size())/Math.log(2));
			if (cap > 0) {
				for (int i = c.size(); i > 0; i--)
					c.add(c.remove(rng.nextInt(i)));
				int n = c.indexOf(new Integer(board[ii][jj]));
				// if index is out of StegoDoku's bounds
				if (n >= Math.pow(2, cap))
					oob_cnt++;
			}
			if (!s.set(ii, jj, board[ii][jj])) {
				System.err.println("expr11_6_helper_v2: did not set cell ("+ii+","+jj+")");
				break;
			}
			s.solveCells();
		}
		return oob_cnt;
	}

	/**
	 * compute ratio of log likelihoods for V3, with permuation seed
	 * @param board
	 * @return ratio of log likelihoods: p(D|Huffman)/p(D|Clean)
	 */
	public static double expr11_6_helper_v3(int[][] board,long seed) {
		SudokuSolver s = new SudokuSolver();
		Random rng = new Random(seed);
		double llH = 0;
		double llC = 0;
		while (!s.isSolved()) {
			int[] cell = StegoDoku.nextCell(s);
			int ii = cell[0];
			int jj = cell[1];
			if (ii == -1) {
				System.err.println("(Error) expr11_6_helper_v3(): Board complete but not solved.");
				//return llH/llC;
				return llC/llH; // engineering
			}
			LinkedList<Integer> c = s.candidates(ii, jj);

			// accumulate log likelihoods
			//if (c.size() == 3 || c.size() == 5 || c.size() == 6 || c.size() == 7 || c.size() == 9) {
			if (c.size() > 1) {
				for (int i = c.size(); i > 0; i--)
					c.add(c.remove(rng.nextInt(i)));
				HuffmanTree tree = new HuffmanTree(c.toArray());
				String m = tree.encode(board[ii][jj]);
				llH -= m.length();
				llC -= Math.log(c.size())/Math.log(2);
			}
			if (!s.set(ii, jj, board[ii][jj])) {
				System.err.println("expr11_6_helper_v3(): did not set cell ("+ii+","+jj+")");
				break;
			}
			s.solveCells();
		}
		//return llH/llC;
		return llC/llH; // engineering
	}

	/** 
	 * Repeat experiment 9
	 * for V4 and V5 when the key is known to the adversary
	 * 
	 */
	public static void Experiment11_9() {
		System.out.println("Running Experiment 11_9");
		RandomSudoku rs;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][][] C4 = new int[10][2][2];
		int[][][] C5 = new int[10][2][2];
		String d;
		long key;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int k = 0; k < 10; k++) {
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					C4[k][i][j] = 0;
					C5[k][i][j] = 0;
				}
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			d = StegoDokuV2.decode(rs.board());
			key = random.nextLong();
			d = StegoDokuV4.decode(rs.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C4[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C4[k][0][0]++;
				}
			}
			d = StegoDokuV5.decode(rs.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C5[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C5[k][0][0]++;
				}
			}

			mssg = "";
			// make SHORT messages so there WILL be padding
			for (int k = 0; k < 10; k++) 
				mssg += random.nextBoolean() ? "0" : "1";
			key = random.nextLong();
			sd4 = new StegoDokuV4(mssg,key);
			d = StegoDokuV4.decode(sd4.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C4[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C4[k][1][0]++;
				}
			}
			sd5 = new StegoDokuV5(mssg,key);
			d = StegoDokuV5.decode(sd5.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C5[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C5[k][1][0]++;
				}
			}
		}

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr11_9_results.txt"));
			outputStream.write("k\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 0; k < 10; k++) {
				double acc4 =  ((double)(C4[k][0][0]+C4[k][1][1]))/(C4[k][0][0]+C4[k][0][1]+C4[k][1][0]+C4[k][1][1]);
				double acc5 =  ((double)(C5[k][0][0]+C5[k][1][1]))/(C5[k][0][0]+C5[k][0][1]+C5[k][1][0]+C5[k][1][1]);
				outputStream.write((k+1) + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();
			}
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** 
	 * Repeat experiment 10
	 * for V4 and V5 when the key is known to the adversary
	 * 
	 */
	public static void Experiment11_10() {
		System.out.println("Running Experiment 11_10");
		RandomSudoku rs;
		StegoDokuV4 sd4;
		StegoDokuV5 sd5;
		String mssg;
		Random random = new Random();
		int[][][] C4 = new int[10][2][2];
		int[][][] C5 = new int[10][2][2];
		String d;
		long key;

		/*
		 * C[0][0] - true negatives
		 * C[0][1] - false positives
		 * C[1][0] - false negatives
		 * C[1][1] - true positives
		 */

		for (int k = 0; k < 10; k++) {
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					C4[k][i][j] = 0;
					C5[k][i][j] = 0;
				}
			}
		}
		for (int i = 0; i < 500; i++) {
			rs = new RandomSudoku();
			//d = StegoDokuV2.decode(rs.board());
			key = random.nextLong();
			d = StegoDokuV4.decode(rs.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C4[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C4[k][0][0]++;
				}
			}
			d = StegoDokuV5.decode(rs.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is wrong
					C5[k][0][1]++;
				} else { // classify as clean
					// which is correct
					C5[k][0][0]++;
				}
			}

			mssg = "";
			// make LONG messages so there will NOT be padding
			for (int k = 0; k < 90; k++) 
				mssg += random.nextBoolean() ? "0" : "1";
			key = random.nextLong();
			sd4 = new StegoDokuV4(mssg,key);
			d = StegoDokuV4.decode(sd4.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C4[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C4[k][1][0]++;
				}
			}
			sd5 = new StegoDokuV5(mssg,key);
			d = StegoDokuV5.decode(sd5.board(),key);
			for (int k=0; k < 10; k++) {
				int b = Integer.parseInt(d.substring(d.length()-(k+1)),2);
				if (b == 0 || b+1 == 2<<k) { // classify as dirty
					// which is correct
					C5[k][1][1]++;
				} else { // classify as clean
					// which is wrong
					C5[k][1][0]++;
				}
			}
		}

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter("Expr11_10_results.txt"));
			outputStream.write("k\tStegoDoku-P\tStegoDoku-HP");
			outputStream.newLine();
			for (int k = 0; k < 10; k++) {
				double acc4 =  ((double)(C4[k][0][0]+C4[k][1][1]))/(C4[k][0][0]+C4[k][0][1]+C4[k][1][0]+C4[k][1][1]);
				double acc5 =  ((double)(C5[k][0][0]+C5[k][1][1]))/(C5[k][0][0]+C5[k][0][1]+C5[k][1][0]+C5[k][1][1]);
				outputStream.write((k+1) + "\t" + acc4  + "\t" + acc5);
				outputStream.newLine();
			}
			outputStream.close();
			System.out.println("\ndone!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Check for clue symettry, classify as dirty if clues not symmetric
	 */
	public static void Experiment12() {
		int[][] C = new int[2][2];
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 2; j++)
				C[i][j] = 0;
		
		int N = 100;

		Random random = new Random(System.currentTimeMillis());
		/* waste of time, these are always symetric
		// make a clean puzzle
		RandomSudoku rs = new RandomSudoku();
		int[][] p_clean = SudokuPuzzleMaker.findSymMinClueSet(rs.board(), random.nextLong());

		// classify clean puzzle
		if (isSymmetric(p_clean)) {
			C[0][0]++; // true negeative
		} else {
			C[1][0]++; // false positive
		}
		 */
		C[0][0] = N;

		for (int n = 0; n < N; n++) {
			// make a dirty puzzle
			String mssg_in = "";
			for (int i = 0; i < 150; i++) {
				mssg_in += random.nextBoolean() ? "0" : "1";
			}
			Object[] ret = StegoPuzzleMaker.encode(mssg_in);
			int[][] p_dirty = (int[][])ret[0];

			// classify dirty puzzle
			if (isSymmetric(p_dirty)) {
				C[0][1]++; // false negative
			} else {
				C[1][1]++; // true positive
			}
		}
		
		System.out.println((C[0][0]+C[1][1])/(2.0*N));

	}

	public static void main(String[] args) {
		long t1,t2;
		t1 = System.currentTimeMillis();
		//Experiment1();
		//Experiment2();
		//Experiment3();
		//Experiment4();
		//Experiment5();
		//Experiment6();
		//Experiment7();
		//Experiment8();
		//Experiment9();
		//Experiment10();
		//Experiment11_1();
		//Experiment11_6();
		//Experiment11_9();
		//Experiment11_10();
		t2 = System.currentTimeMillis();
		System.out.println((t2-t1) + " ms");
	}

}
