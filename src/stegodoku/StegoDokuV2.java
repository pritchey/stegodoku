package stegodoku;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

import libPhil.sudoku.SudokuSolver;

/**
 * StegoDoku
 * 
 * @author pritchey
 *
 */

public class StegoDokuV2 extends StegoDoku{
	private boolean backtracked;
	
	private final boolean DECODE = false;

	/**
	 * construct a new StegoDokuV2 object using given binary message
	 * @param mssg - binary String representing secret message
	 */
	public StegoDokuV2(String mssg) {
		super();
		//System.out.println("Making StegoDoku...");
		if (f(s,mssg)) {
			s.solveCells();
			//s.printBoard();
			//System.out.println("Success! " + numBits + " bits hidden");
			if (DECODE) {
				String decoded = decode();
				//System.out.println("Decoded as ("+decoded.length()+" bits): " + decoded);
				int err = 0;
				for (int i = 0; i < Math.min(mssg.length(), decoded.length()); i++)
					if (mssg.charAt(i) != decoded.charAt(i))
						err++;
				if (err > 0)
					System.out.println("Error rate = " + (double)err/Math.min(mssg.length(), decoded.length()));
			}
		} else {
			//System.out.println("Failure");
		}
	}

	//private int depth = 0;
	/**
	 * recursively create a Sudoku which encodes the given message
	 * @param s - SudokuSolver object to keep track of board state
	 * @param mssg - binary String secret message
	 * @return true if a solved Sudoku is found
	 */
	public boolean f(SudokuSolver s, String mssg) {
		if (s.isSolved()) return true;

		// get next cell
		int[] cell = nextCell(s);
		int ii = cell[0];
		int jj = cell[1];
		if (ii == -1) {
			//System.err.println("(Error) StegoDokuV2.f(SudokuSolver,String): Board complete but not solved.");
			return false;
		}

		// encode some bits there
		LinkedList<Integer> c = s.candidates(ii, jj);
		SudokuSolver s2;
		boolean b;
		do {
			if (c.isEmpty()) {
				return false;
			}
			int cap = (int)(Math.log(c.size())/Math.log(2));
			//System.out.println("depth = " + depth);
			//System.out.println("cap("+ii+","+jj+") = " + cap);
			//System.out.println(c.size());
			int n;
			if (cap == 0)
				n = 0;
			else {
				if (cap > mssg.length()) {
					String pad = "";
					switch (paddingMode) {
					default:
					case 0: pad="0000"; break;
					case 1: pad="1111"; break;
					case 2: pad=Integer.toBinaryString(randomPad.nextInt(16)+32).substring(1);
					}
					mssg += pad;
				}
				n = Integer.parseInt(mssg.substring(0,cap), 2);
			}
			int num = c.remove(n);
			s2 = new SudokuSolver(s.board());
			//System.out.println("setting cell ("+ii+","+jj+") => "+num+".");
			s2.set(ii, jj, num);
			s2.solveCells();
			//depth++;
			b = f(s2,mssg.substring(cap));
			//depth--;
			if (b) {
				//System.out.println("setting cell ("+ii+","+jj+") => "+num+".");
				this.s.set(ii, jj, num);
				numBits += cap;
			} else
				backtracked = true;
		} while (!b);
		return b;
	}

	/**
	 * extract hidden bits from given board
	 * @param board
	 * @return extracted bits
	 */
	public static String decode(int[][] board) {
		// make sure board is solved
		board = new SudokuSolver(board).solvedBoard();
		SudokuSolver s = new SudokuSolver();
		StringBuilder sb = new StringBuilder();
		while (!s.isSolved()) {
			// get next cell
			int[] cell = nextCell(s);
			int ii = cell[0];
			int jj = cell[1];
			if (ii == -1) {
				System.err.println("(Error) StegoDokuV2.decode(): Board complete but not solved.");
				return sb.toString();
			}

			LinkedList<Integer> c = s.candidates(ii, jj);
			int cap = (int)(Math.log(c.size())/Math.log(2));
			if (cap > 0) {
				int n = c.indexOf(new Integer(board[ii][jj]));
				String m = Integer.toBinaryString(n);
				while (m.length() < cap)
					m = "0" + m;
				sb.append(m);
				//System.out.println("extracted " + m + " from " + ii + "," + jj);
			}
			//System.out.println("setting " + ii + "," + jj + " to " + this.s.board(ii, jj));
			if (!s.set(ii, jj, board[ii][jj])) {
				//				for (int[] r : s.board()) {
				//					for (int n : r) {
				//						System.out.print(n + " ");
				//					}
				//					System.out.println();
				//				}
				System.err.println("StegoDokuV2.decode(): did not set cell ("+ii+","+jj+")");
				break;
			}
			s.solveCells();
		}
		return sb.toString();
	}

	/**
	 * extract the hidden bits from the sudoku board
	 * @return binary String secret message
	 */
	public String decode() {
		return decode(this.s.board());
	}

	/**
	 * 
	 * @return true if generation backtracked
	 */
	public boolean backtracked() {
		return backtracked;
	}
	
	public static void runTest(int N, String path) {
		String str;
		Random random = new Random();
		StegoDokuV2 sd;
		long t1,t2;
		if (path == null || path.isEmpty())
			path = "V2_results.txt";
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(path));
			outputStream.write("time (ms)\tcapacity (bits)\terr (%)\tbacktracked");
			outputStream.newLine();
			for (int i = 0; i < N; i++) {
				str = "";
				for (int j = 0 ; j < 72; j++)
					if (random.nextBoolean())
						str += "0";
					else
						str += "1";
				t1 = System.currentTimeMillis();
				sd = new StegoDokuV2(str);
				t2 = System.currentTimeMillis();
				String decoded = sd.decode();
				int err = 0;
				for (int j = 0; j < Math.min(str.length(), decoded.length()); j++)
					if (str.charAt(j) != decoded.charAt(j))
						err++;
				outputStream.write((t2-t1) + "\t" + sd.numBits() + "\t" + (double)err*100.0/Math.min(str.length(), decoded.length()) + (sd.backtracked()?"1":"0"));
				outputStream.newLine();
				if (i % 1000 == 0) {
					System.out.print(i/1000);
					outputStream.flush();
				}
			}
			outputStream.close();
			System.out.println("\ndone!\nresults written to: " + path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void main(String[] args) {
		System.out.println("You must use the StegoDokuMain class.");
	}

}
