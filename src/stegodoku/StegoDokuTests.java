package stegodoku;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Random;

import libPhil.sudoku.SudokuBoard;
import libPhil.sudoku.SudokuPuzzleMaker;

public class StegoDokuTests {

	@Test
	public void testStegoDokuDecode() {
		String mssg = "";
		Random r = new Random();
		for (int i = 0; i < 7; i++) {
			String b = Integer.toBinaryString(r.nextInt(26)+65);
			while (b.length() < 8)
				b = '0' + b;
			mssg += b;
		}
		StegoDokuV2 sd = new StegoDokuV2(mssg);
		String mssg_out = sd.decode();
		assertEquals("decoded mssg does not match original.",mssg.substring(0, Math.min(mssg.length(), mssg_out.length())),mssg_out.substring(0, Math.min(mssg.length(), mssg_out.length())));
	}
	
	@Test
	public void testBoardCompression() {
		String mssg = "";
		Random r = new Random();
		for (int i = 0; i < 7; i++) {
			String b = Integer.toBinaryString(r.nextInt(26)+65);
			while (b.length() < 8)
				b = '0' + b;
			mssg += b;
		}
		StegoDokuV2 sd = new StegoDokuV2(mssg);
		SudokuPuzzleMaker spm  = new SudokuPuzzleMaker(sd.board());
		spm.makePuzzle(100);
		int[][] puzzle = spm.puzzle();
		//SudokuBoard.print(puzzle);
		String c = SudokuBoard.compress(puzzle);
		//System.out.println(c + " (" + c.length() +" bits)");
		int[][] d = SudokuBoard.decompress(c);
		//SudokuBoard.print(d);
		for (int i = 0; i < 9; i++)
			assertArrayEquals("row " + i + " does not match.",puzzle[i],d[i]);
	}

}
