package stegodoku;

import java.util.Scanner;

import libPhil.huffman.HuffmanTree;
import libPhil.sudoku.SudokuBoard;
import libPhil.sudoku.SudokuPuzzleMaker;

public class StegoDokuDemo {
	public static final Object[] sym;
	public static final double[] prob;
	public static final HuffmanTree T;
	public static final String END;

	static {
		sym = new Object[40];
		for (int i = 0; i < 26; i++)
			sym[i] = (char)('A'+i);
		for (int i=26; i < 36; i++)
			sym[i] = (char)('0'+(i-26));
		sym[36] = ' ';
		sym[37] = ',';
		sym[38] = '.';
		sym[39] = ';'; // END symbol
		prob = new double[]{
				0.0704815225, // A
				0.0129331858, // B
				0.0235227741, // C
				0.0374975588, // ...
				0.1043334852,
				0.019963978,
				0.0176203806,
				0.0513855435,
				0.0634507302,
				0.000867999,
				0.0059891934,
				0.034546362,
				0.0226547751,
				0.0603259336,
				0.0666623267,
				0.0157975826,
				0.0009547989,
				0.0522535425,
				0.05451034,
				0.0789879131,
				0.0249983725,
				0.0096347894,
				0.01814118,
				0.0014755984, // X
				0.0183147799, // Y
				0.0006075993,  // Z
				0.0023056225,  // 0
				0.0023056225,  // 1
				0.0023056225,  // ...
				0.0023056225,
				0.0023056225,
				0.0023056225,
				0.0023056225,
				0.0023056225,
				0.0023056225, // 8
				0.0023056225, // 9
				0.1044202851, // space
				0.0023056225, //,
				0.0023056225, //.
				0.0           // END
		};
		T = new HuffmanTree(sym,prob);
		END = T.encode(';');
	}

	/**
	 * encode specified message as a sudoku puzzle
	 * @param mssg
	 * @return sudoku puzzle
	 */
	public static int[][] encode(String mssg) {
		// clean mssg
		System.out.print("cleaning message...");
		mssg = mssg.toUpperCase();
		mssg = mssg.replaceAll("[^A-Z0-9 ,.]", "");
		System.out.print("done\n");
		

		// convert mssg to bits
		System.out.print("converting message to bits...");
		String bits = "";
		for (char c : mssg.toCharArray())
			bits += T.encode(c);
		bits += END;
		System.out.print("done ("+bits.length()+" bits)\n");
		
		// make stegodoku puzzle
		System.out.print("building sudoku puzzle...");
		StegoDoku.setPaddingMode(0);
		Object[] ret = StegoPuzzleMaker.encode(bits);
		int[][] puzzle = (int[][])ret[0];
		int num_bits = (int)ret[1];
		System.out.print("done\n");
		
		System.out.printf("%.2f%% of bits hidden\n\n",Math.min(100.0,(100.0*num_bits/(bits.length()-END.length()))));
		
		//// make puzzle
		//System.out.print("making sudoku puzzle...");
		//int[][] puzzle = new SudokuPuzzleMaker(sd.board()).makePuzzle(8);
		//System.out.print("done\n");
		
		return puzzle;
	}

	/**
	 * decode hidden message from specified puzzle
	 * @param puzzle
	 * @return message decoded from puzzle
	 */
	public static String decode(int[][] puzzle) {
		// extract bits from puzzle
		System.out.print("extracting bits from puzzle...");
		String bits = StegoPuzzleMaker.decode(puzzle);
		//String bits = StegoDokuV3.decode(puzzle);
		System.out.print("done\n");
		
		// decode bits
		System.out.print("converting bits to message...");
		String mssg = "";
		Object[] ret = T.decode_nice(bits);
		while (ret != null && (char)ret[0] != ';') {
			mssg += (char)ret[0];
			bits = (String)ret[1];
			ret = T.decode_nice(bits);
		}
		System.out.print("done\n");
		
		return mssg;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Welcome to StegoDoku Demo.\n");

		System.out.println("What would you like to do?");
		System.out.println("(1) Encode a message");
		System.out.println("(2) Decode a puzzle");

		System.out.print("\n? ");
		String in = scanner.nextLine();

		boolean f = false;
		while (!f) {
			switch(in) {
			case "1":
				f = true;
				System.out.println("\nMessages should only contain letters [A-Z], numbers [0-9], spaces [ ], periods [.], and commas [,].");
				System.out.println("About 20 characters will fit.");
				System.out.println("Enter the message to encode: ");
				System.out.print("? ");
				in = scanner.nextLine();
				System.out.println();
				int[][] puzzle = encode(in);
				SudokuBoard.print(puzzle);
				break;
			case "2":
				f = true;
				System.out.println("\nEnter the solved puzzle to decode:");
				System.out.print("? ");
				in = scanner.nextLine();
				System.out.println();
				try {
					String in2 = in;
					while (!(new SudokuBoard(in2).isSolved())){
						System.out.println("The puzzle is not solved.  Try again.");
						System.out.print("? ");
						in2 = scanner.nextLine();
						if (in2.equalsIgnoreCase("solve it for me, please")) {
							break;
						}
					}
				System.out.println("\n" + decode(new SudokuBoard(in).board()));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			default:
				System.out.println("You must enter a \"1\" or a \"2\"");
				System.out.print("\n? ");
				in = scanner.nextLine();
				break;
			}
		}
		scanner.close();

	}

}
