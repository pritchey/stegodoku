package stegodoku;

import java.util.Random;

import libPhil.sudoku.SudokuSolver;

public abstract class StegoDoku {
	protected SudokuSolver s;
	protected int numBits;
	protected static int paddingMode;
	protected static Random randomPad;
	
	public StegoDoku() {
		s = new SudokuSolver();
		numBits = 0;
	}
	
	/**
	 * find the next cell to set
	 * @param s - SudokuSolver object, contains board state
	 * @return {row,col} of next cell to set
	 */
	public static int[] nextCell(SudokuSolver s) {
		// find the last, most constrained spot
		int cs = 10;
		int ii = -1,jj = -1;
		for (int i = 8; i >= 0; i--) {
			for (int j = 8; j >= 0; j--) {
				if (s.board(i, j) > 0) continue;
				if (s.candidates(i, j).size() < cs) {
					cs = s.candidates(i, j).size();
					ii = i;
					jj = j;
				}
			}
		}
		return new int[]{ii,jj};
	}
	
	/**
	 * 
	 * @return copy of the board
	 */
	public int[][] board() {
		return s.board();
	}
	
	/**
	 * 
	 * @param row
	 * @param col
	 * @return board[row][col]
	 */
	public int board(int row, int col) {
		return s.board(row, col);
	}
	
	/**
	 * 
	 * @return number of bits hidden
	 */
	public int numBits() {
		return numBits;
	}
	
	/**
	 * set padding mode<br>
	 * 0 - pad with 0s<br>
	 * 1 - pad with 1s<br>
	 * 2 - pad randomly<br>
	 * @param mode
	 */
	protected static void setPaddingMode(int mode) {
		if (mode < 0 || mode > 2) {
			System.err.println("ERROR: padding mode must be 0, 1, or 2.");
			return;
		}
		paddingMode = mode;
		if (paddingMode == 2)
			randomPad = new Random(System.currentTimeMillis());
	}
	
	
//	/**
//	 * recursively create a Sudoku which encodes the given message
//	 * @param s - SudokuSolver object to keep track of board state
//	 * @param mssg - binary String secret message
//	 * @return true if a solved Sudoku is found
//	 */
//	protected abstract boolean f(SudokuSolver s, String mssg);
}
