package stegodoku;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.Random;

import libPhil.combinadic.Combinadic;
import libPhil.sudoku.RandomSudoku;
import libPhil.sudoku.SudokuBoard;
import libPhil.sudoku.SudokuPuzzleMaker;
import libPhil.sudoku.SudokuSolutionCounter;

/**
 * 
 * @author pritchey
 * @version 2014-08-01
 *
 */

public class StegoPuzzleMaker extends StegoDoku {

	/**
	 * maximum number of clues allowed in initial puzzle, 
	 * larger values mean more bits but slower puzzle making.
	 */
	private static final int MAX_CLUES = 32;

	/**
	 * if set to true, PM will try to remove the initial clues.  
	 * this is slow and does not affect capacity.  
	 * it does affect the final number of clues.
	 */
	private static boolean REMOVE_MCS = false;

	/**
	 * @param board - solved board
	 * @param n - number of clues
	 * @return [0]: puzzle, [1]: number of bits hidden
	 */
	public static Object[] embedInPuzzle(int[][] board, String bits) {
		//System.out.println("input board");
		//SudokuBoard.printOneLineBoard(board);
		int[][] mcs = SudokuPuzzleMaker.findMinClueSet(board,0);

		// count clues
		int nc = 0;
		for (int[] r : mcs)
			for (int c : r)
				if (c > 0)
					nc++;
		//System.out.println(nc + " clues in mcs");

		BigInteger sum = BigInteger.ZERO;
		for (int i = 0; i <= MAX_CLUES-nc; i++) {
			sum = sum.add(Combinadic.combin(81-nc, i));
			//System.out.println(Combinadic.combin(81-nc, i));
		}
		int cap = sum.bitLength();
		//System.out.println(cap + " bits in puzzle config");

		// convert next bits to a number
		BigInteger l = BigInteger.ZERO;
		for (int i = 0; i < cap; i++) {
			l = l.shiftLeft(1);
			if (i < bits.length() && bits.charAt(i) == '1')
				l = l.add(BigInteger.ONE);
		}
		//System.out.println("mssg = " + l);

		int[][] puzzle = new int[9][9];
		int k = 0;
		while (l.compareTo(BigInteger.ZERO) >= 0)
			l = l.subtract(Combinadic.combin(81-nc, ++k));
		l = l.add(Combinadic.combin(81-nc, k));
		//System.out.println("construct the " + l + "-th " + k + "-combination");
		int[] comb = Combinadic.toCombination(l, k);
		//System.out.println(Combinadic.toString(comb));
		k = 0;
		int idx = comb.length-1;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (mcs[i][j] > 0) {
					puzzle[i][j] = mcs[i][j];
				} else {
					if (idx >= 0 && comb[idx] == k) {
						puzzle[i][j] = board[i][j];
						idx--;
					}
					k++;
				}
			}
		}

		// remove clues in mcs until board not solvable, 
		// then add last clue removed back.
		// should work since only extra clues are used for hiding.
		int[] mc = new int[nc];
		k = 0;
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				if (mcs[i][j] > 0)
					mc[k++] = 9*i + j;

		//SudokuBoard.printOneLineBoard(mcs);
		//SudokuBoard.printOneLineBoard(puzzle);

		// this is slow, and does not affect capacity, only for style points
		if (REMOVE_MCS)
			removeMCS(mc,puzzle);

		//		SudokuBoard.printOneLineBoard(puzzle);
		//
		//		SudokuSolutionCounter ssc = new SudokuSolutionCounter(puzzle,2);
		//		try {
		//			int ns = ssc.countSolutions();
		//			if (ns == 1) {
		//				System.out.println("1 solution");
		//				System.out.println(ssc.solvedBoards()[0]);
		//			} else {
		//				System.out.println("2 solutions?");
		//				System.out.println(ssc.solvedBoards()[0]);
		//				System.out.println(ssc.solvedBoards()[1]);
		//			}
		//		} catch (Exception e) {
		//			System.out.println("more than 1 solution");
		//			System.out.println(ssc.solvedBoards()[0]);
		//			System.out.println(ssc.solvedBoards()[1]);
		//		}

		//SudokuBoard.printOneLineBoard(puzzle);

		return new Object[]{puzzle,cap};
	}

	/**
	 * randomly remove mcs clues until puzzle no more can be removed without invalidating the puzzle
	 * @param mc - location of min clues
	 * @param puzzle
	 * @param cnt
	 */
	private static void removeMCS(int[] mc, int[][] puzzle) {
		Random random = new Random(System.currentTimeMillis());
		int l,c;
		LinkedList<Integer> list = new LinkedList<Integer>();
		for (int i = 0; i < mc.length; i++) {
			if (mc[i] == 0)
				continue;
			l = mc[i];
			c = puzzle[l/9][l%9];	// backup clue
			puzzle[l/9][l%9] = 0;	// remove clue
			SudokuSolutionCounter.setLimit(2);
			try {
				int ns = SudokuSolutionCounter.countSolutions(puzzle);
				if (ns == 1) {
					list.add(l);
				}
			} catch (Exception e) {

			}
			puzzle[l/9][l%9] = c;	// restore clue
		}
		//System.out.println(list.size() + " clues potentially removable");
		// heuristic: start search at size/2+2
		LinkedList<int[]> list2 = new LinkedList<int[]>();
		for (int k = list.size()/2+2; k >= 1 && list2.isEmpty(); k--) {
			BigInteger combin = Combinadic.combin(list.size(), k);
			long loop_max;
			if (combin.compareTo(BigInteger.ONE.shiftLeft(64)) >= 0)
				loop_max = Long.MAX_VALUE;
			else 
				loop_max = combin.longValue();
			//System.out.println(combin + " "+k+"-combinations to check");
			for (long i = 0; i < loop_max; i++) {
				int[] comb = Combinadic.toCombination(i, k);
				int[] ca = new int[comb.length];
				for (int j = 0; j < comb.length; j++) {
					l = list.get(comb[j]);
					ca[j] = puzzle[l/9][l%9];	// backup
					puzzle[l/9][l%9] = 0;		// remove
				}
				SudokuSolutionCounter.setLimit(2);
				try {
					int ns = SudokuSolutionCounter.countSolutions(puzzle);
					if (ns == 1) {
						list2.add(comb);
					}
				} catch (Exception e) {

				}
				for (int j = 0; j < comb.length; j++) {
					l = list.get(comb[j]);
					puzzle[l/9][l%9] = ca[j];	// restore
				}
			}
		}
		if (!list2.isEmpty()) {
			int[] comb = list2.get(random.nextInt(list2.size()));
			for (int j = 0; j < comb.length; j++) {
				l = list.get(comb[j]);
				puzzle[l/9][l%9] = 0;		// remove
			}
			//System.out.println("removed " + comb.length + " clues");
		}
	}

	/**
	 * extract bits from a puzzle (clue config)
	 * @param puzzle
	 * @return bits hidden in puzzle (clue config)
	 */
	public static String decodePuzzle(int[][] puzzle) {
		// find min clue set
		int[][] mcs = SudokuPuzzleMaker.findMinClueSet(puzzle,0);
		//System.out.println("mcs extract");
		//SudokuBoard.printOneLineBoard(mcs);

		// count clues in mcs
		int nc = 0;
		for (int[] r : mcs)
			for (int c : r)
				if (c > 0)
					nc++;
		//System.out.println(nc + " clues in mcs");

		// compute number of extra clues
		int m = 0;
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				if (puzzle[i][j] > 0 && mcs[i][j] == 0)
					m++;
		//System.out.println(m + " extra clues");


		// get combination of extra clues
		int[] comb = new int[m];
		int idx = m-1;
		int k = 0;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (mcs[i][j] == 0) {
					if (puzzle[i][j] > 0)
						comb[idx--] = k;
					k++;
				}
			}
		}
		//System.out.println(Combinadic.toString(comb));

		// compute index of stego-combination
		BigInteger b = BigInteger.ZERO;
		for (int i = 1; i <= m-1; i++)
			b = b.add(Combinadic.combin(81-nc, i));
		b = b.add(Combinadic.toNumber(comb));
		//System.out.println("deconstruct the " + Combinadic.toNumber(comb) + "-th " + m + "-combination");
		//System.out.println("message = " + b);

		// compute capacity
		BigInteger sum = BigInteger.ZERO;
		for (int i = 0; i <= MAX_CLUES-nc; i++)
			sum = sum.add(Combinadic.combin(81-nc, i));
		int cap = sum.bitLength();
		//System.out.println(cap + " bits in puzzle config");

		String bits = "";
		for (int i = b.bitLength()-1; i >= 0; i--)
			bits += b.testBit(i) ? "1" : "0";
		while (bits.length() < cap)
			bits = "0" + bits;

		return bits;
	}

	/**
	 * encode some bits as a sudoku board
	 * @param bits - bits to hide
	 * @return [0]: board, [1]: number of bits hidden
	 */
	public static Object[] embedInBoard(String bits) {
		//System.out.println("making board");
		StegoDokuV3 sd = new StegoDokuV3(bits);
		//System.out.println("done");
		return new Object[]{sd.board(),sd.numBits()};
	}

	/**
	 * encode some bits as a sudoku board
	 * @param bits - bits to hide
	 * @param seed - secret RNG seed/key
	 * @return [0]: board, [1]: number of bits hidden
	 */
	public static Object[] embedInBoard(String bits, long seed) {
		//System.out.println("making board");
		StegoDokuV5 sd = new StegoDokuV5(bits,seed);
		//System.out.println("done");
		return new Object[]{sd.board(),sd.numBits()};
	}

	/**
	 * encode bits as a StegoDoku puzzle
	 * @param bits - secret bits
	 * @return [0]: stegodoku puzzle, [1]:number of bits hidden
	 */
	public static Object[] encode(String bits) {

		// encoding implies hiding, so try to do a good job.
		REMOVE_MCS = true;
		
		// embed some bits in the board
		//System.out.print("embed in board...");
		Object[] retB = embedInBoard(bits);
		//System.out.println("done");

		// embed some bits in the puzzle
		//System.out.print("embed in puzzle...");
		Object[] retP = embedInPuzzle((int[][])(retB[0]),bits.substring((int)(retB[1])));
		//System.out.println("done");

		return new Object[]{retP[0],(int)(retB[1]) + (int)(retP[1])};
	}

	/**
	 * encode bits as a StegoDoku puzzle
	 * @param bits - secret bits
	 * @param seed - secret RNG seed
	 * @return [0]: stegodoku puzzle, [1]:number of bits hidden
	 */
	public static Object[] encode(String bits, long seed) {
		
		// encoding implies hiding, so try to do a good job.
		REMOVE_MCS = true;
		
		// embed some bits in the board
		//System.out.print("embed in board...");
		Object[] retB = embedInBoard(bits,seed);
		//System.out.println("done");

		// embed some bits in the puzzle
		//System.out.print("embed in puzzle...");
		Object[] retP = embedInPuzzle((int[][])(retB[0]),bits.substring((int)(retB[1])));
		//System.out.println("done");

		return new Object[]{retP[0],(int)(retB[1]) + (int)(retP[1])};
	}

	/**
	 * decode a stegodoku puzzle
	 * @param puzzle - stegodoku
	 * @return bits extracted from puzzle and board
	 */
	public static String decode(int[][] puzzle) {

		// decode board
		//System.out.println("decoding board");
		String bits0 = StegoDokuV3.decode(puzzle);
		//System.out.println("done");

		// decode puzzle
		//System.out.println("decoding puzzle");
		String bits1 = decodePuzzle(puzzle);
		//System.out.println("done");

		return bits0+bits1;
	}

	/**
	 * decode a stegodoku puzzle
	 * @param puzzle
	 * @param seed - secret RNG seed
	 * @return bits extracted from puzzle and board
	 */
	public static String decode(int[][] puzzle, long seed) {

		// decode board
		//System.out.println("decoding board");
		String bits0 = StegoDokuV5.decode(puzzle,seed);
		//System.out.println("done");

		// decode puzzle
		//System.out.println("decoding puzzle");
		String bits1 = decodePuzzle(puzzle);
		//System.out.println("done");

		return bits0+bits1;
	}

	/**
	 * run a test of the stegodoku puzzle maker
	 * @param N - number of puzzles to make
	 * @param path - path to store results, default = PM_results.txt
	 * @param rmcs - if true, will attempt to remove initial clue set (must be set to true for timing analysis, may be set to false for capacity analysis)
	 */
	public static void runTest(int N, String path, boolean rmcs) {
		String mssg_in;
		Random random = new Random();
		RandomSudoku rs;
		long t1,t2;
		REMOVE_MCS = rmcs;
		if (path == null || path.isEmpty())
			path = "PM_results.txt";
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(path));
			if (!REMOVE_MCS) {
				outputStream.write("!!!!! THIS DATA SHOULD NOT BE USED FOR TIMING ANALYSIS !!!!!");
				outputStream.newLine();
			}
			outputStream.write("encode time (ms),decode time (ms),capacity (bits),num clues,err");
			outputStream.newLine();
			for (int i = 0; i < N; i++) {
				// make a random bitstring to hide
				mssg_in = "";
				for (int j = 0 ; j < 50; j++)
					if (random.nextBoolean())
						mssg_in += "0";
					else
						mssg_in += "1";

				// make a random sudoku board
				rs = new RandomSudoku();

				// get board
				int[][] b = rs.board();

				// make a stegopuzzle
				t1 = System.currentTimeMillis();
				Object[] ret = embedInPuzzle(b,mssg_in);
				t2 = System.currentTimeMillis();
				long encodeTime = t2-t1;
				int[][] puzzle = (int[][])(ret[0]);
				int num_bits = (int)(ret[1]);
				int num_clues = SudokuPuzzleMaker.numClues(puzzle);

				// decode puzzle
				t1 = System.currentTimeMillis();
				String mssg_out = decodePuzzle((int[][])ret[0]);
				t2 = System.currentTimeMillis();
				long decodeTime = t2-t1;

				// compute error
				double err = 0;
				for (int j = 0; j < mssg_out.length(); j++)
					if (mssg_in.charAt(j) != mssg_out.charAt(j))
						err++;
				err /= mssg_out.length();
				if (err > 0) {
					System.out.println(mssg_in);
					SudokuBoard.printOneLineBoard((int[][])ret[0]);
				}

				// write results
				//System.out.println(encodeTime + "," + decodeTime + "," + num_bits + "," + num_clues + "," + err);
				outputStream.write(encodeTime + "," + decodeTime + "," + num_bits + "," + num_clues + "," + err);
				outputStream.newLine();
				if ((i+1) % 100 == 0) {
					System.out.println((100.0*(i+1))/N);
					outputStream.flush();
				}
			}
			outputStream.close();
			System.out.println("\ndone!\nresults written to: " + path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * make stego board and stego puzzle and remove initial clues
	 * @param N - number of puzzles to generate
	 * @param path - path to store results, default = SD+PM_results.txt
	 */
	public static void runTestSDPM(int N, String path) {
		Random random = new Random(System.currentTimeMillis());
		String mssg_in, mssg_out;
		long t1,t2,encodeTime,decodeTime;
		int num_clues, num_bits;
		int[][] puzzle;
		double err;
		REMOVE_MCS = true;
		
		if (path == null || path.isEmpty())
			path = "SD+PM_results.txt";
		
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(path));
			outputStream.write("encode time (ms),decode time (ms),capacity (bits),num clues,err");
			outputStream.newLine();
			for (int r = 0; r < N+1; r++) {
				// generate random secret bits
				mssg_in = "";
				for (int i = 0; i < 150; i++)
					mssg_in += random.nextBoolean() ? "1" : "0";
				
				// make board and puzzle
				t1 = System.currentTimeMillis();
				Object[] ret = encode(mssg_in);
				t2 = System.currentTimeMillis();
				encodeTime = t2-t1;
				puzzle = (int[][])(ret[0]);
				num_bits = (int)(ret[1]);
				num_clues = SudokuPuzzleMaker.numClues(puzzle);
				//System.out.println(num_bits + " bits hidden");
				//SudokuBoard.print(puzzle);
				
				// decode board and puzzle
				t1 = System.currentTimeMillis();
				mssg_out = decode(puzzle);
				t2 = System.currentTimeMillis();
				decodeTime = t2-t1;
				//System.out.println("mssg_in : " + mssg_in);
				//System.out.println("mssg_out: " + mssg_out);
				
				// compute error
				err = 0;
				for (int i = 0; i < mssg_out.length(); i++)
					if (mssg_in.charAt(i) != mssg_out.charAt(i))
						err++;
				err /= (double)mssg_out.length();
				if (err > 0) {
					System.out.println(mssg_in);
					SudokuBoard.printOneLineBoard((int[][])ret[0]);
				}
				//System.out.println("err = " + (err*100) + "%");
				
				// write results
				outputStream.write(encodeTime + "," + decodeTime + "," + num_bits + "," + num_clues + "," + err);
				outputStream.newLine();
				if ((r+1) % 100 == 0) {
					System.out.println((100.0*(r+1))/N);
					outputStream.flush();
				}
			}
			outputStream.close();
			System.out.println("\ndone!\nresults written to: " + path);
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	public static void main(String[] args) {
		// timing data for PM
		// this will be slow
		//runTest(10000,null,true);
		
		// capacity data for PM
		//runTest(10000,null,false);
		
		// timing and capacity data for full StegoDoku (board + puzzle)
		// REMOVE_MCS is set to true, so this will be slow.
		runTestSDPM(10000,null);
	}
}
