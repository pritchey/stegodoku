#! /usr/bin/bash

read -p "Make sure you have generated results for Rules from 1 to $1.\n(ENTER to continue)\n"
for column in {1..4}
do
	case "$column" in
	1)	printf "time (ms)\n" ;;
	2)	printf "capacity (bits)\n" ;;
	3)	printf "decoding error (%%)\n" ;;
	4)	printf "backtracks (n)\n" ;;
	esac
	printf "Rules\tStegoDoku\tStegoDoku-H\tStegoDoku-P\tStegoDoku-HP\n"
	for i in {1..$1}
	do
		printf "$i\t"
		for version in {2..5}
		do
			awk 'NR > 1 {s+=$'${column}'} END {printf "%0.4f\t",s/(NR-1)}' V${version}_results_${i}.txt
		done
		printf "\n"
	done
done
