# README #

This README documents the steps necessary to get StegoDoku up and running.

### What is this repository for? ###

* StegoDoku: Hiding Data in Sudoku Puzzles

### How do I get set up? ###

0. Install [Apache Ant](https://ant.apache.org/bindownload.cgi)
1. Clone the repository
2. Download JUnit
3. Build

```
#!bash

# install ant (on Debian, Ubuntu, Mint)
sudo apt install ant

# clone the repository
git clone https://pritchey@bitbucket.org/pritchey/stegodoku.git

# get JUnit
cd stegodoku
wget -O junit-4.12.jar https://search.maven.org/remotecontent?filepath=junit/junit/4.12/junit-4.12.jar
mkdir lib
mv junit-4.12.jar lib

# build
ant
```


### Who do I talk to? ###

* Philip Ritchey <pcr@tamu.edu>